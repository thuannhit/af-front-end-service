const webpack = require('webpack')
const path = require('path')
const WebpackMd5Hash = require('webpack-md5-hash')
const TerserPlugin = require('terser-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'production',
  entry: './src/main.ts',
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@': path.resolve(__dirname, 'src/app/'),
      $: path.resolve(__dirname, '')
    }
  },
  target: 'web',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/static/',
    filename: 'resources/js/[name].[contenthash:8].js',
    chunkFilename: 'resources/js/chunk-[name].[contenthash:8].chunk.js',
    globalObject: 'this'
  },
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: 'all'
    },
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          parse: {
            ecma: 8
          },
          compress: {
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2
          },
          mangle: {
            safari10: true
          },
          keep_classnames: false,
          keep_fnames: false,
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true
          }
        },
        sourceMap: false
      })
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'assets',
        to: 'assets'
      },
      {
        from: './node_modules/bootstrap/dist/css/bootstrap.min.css',
        to: 'assets/css/bootstrap.min.css'
      },
      {
        from: './node_modules/jquery/dist/jquery.slim.min.js',
        to: 'assets/js/jquery.slim.min.js'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/indigo-pink.css',
        to: 'assets/css/indigo-pink.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css',
        to: 'assets/css/deeppurple-amber.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/pink-bluegrey.css',
        to: 'assets/css/pink-bluegrey.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/purple-green.css',
        to: 'assets/css/purple-green.css'
      }
    ]),

    // Hash the files using MD5 so that their names change when the content changes.
    new WebpackMd5Hash(),

    new webpack.DefinePlugin({
      config: JSON.stringify({
        apiUrl: '/api'
      }),
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    // Generate HTML file that contains references to generated bundles. See here for how this works: https://github.com/ampedandwired/html-webpack-plugin#basic-usage
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    }),

    // Generate an external css file with a hash in the filename
    new MiniCssExtractPlugin({
      filename: 'resources/css/[name].[contenthash:8].css',
      chunkFilename: 'resources/css/[name].[contenthash:8].chunk.css'
    }),

    new webpack.ContextReplacementPlugin(
      /\@angular(\\|\/)core(\\|\/)fesm5/,
      path.resolve(__dirname, 'src')
    ),

    new webpack.ProgressPlugin({
      entries: true,
      modules: true,
      modulesCount: 100,
      profile: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: ['ts-loader', 'angular2-template-loader']
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.(less)$/,
        loaders: [
          'to-string-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'less-loader'
        ]
      },
      {
        test: /\.(css|scss)$/,
        loaders: [
          'to-string-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /[\/\\]@angular[\/\\].+\.js$/,
        parser: { system: true }
      }
    ]
  },
  node: {
    module: 'empty',
    dgram: 'empty',
    dns: 'mock',
    fs: 'empty',
    http2: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },
  performance: false
}
