const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
// const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/main.ts',

  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@': path.resolve(__dirname, 'src/app/'),
      $: path.resolve(__dirname, '')
    }
  },
  output: {
    // path: __dirname + '/dist',
    publicPath: '/',
    // filename: 'bundle.js',
    chunkFilename: '[name]-chunk.js',
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },

  // build: {
  //     assetsPublicPath: '/',
  //     assetsSubDirectory: 'assets'
  // },

  module: {
    rules: [
      {
        test: /\.ts$/,
        use: ['ts-loader', 'angular2-template-loader']
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader']
      },
      // {
      //     test: /\.css$/,
      //     use: ['style-loader', 'css-loader']
      // },
      // {
      //     test: /\.(css|scss)$/,
      //     exclude: [
      //         path.join(__dirname, 'src/app/')
      //     ],
      //     loaders: ['to-string-loader', 'css-loader', 'sass-loader']
      //     // loaders: ExtractTextPlugin.extract({fallback:'style-loader', loader:'css-loader!sass-loader'})
      // },
      {
        test: /\.(css|scss)$/,
        loaders: ['to-string-loader', 'css-loader', 'sass-loader']
      },
      // {
      //     test: /\.(css|scss)$/,
      //     use: [
      //         'style-loader',
      //         'css-loader',
      //         'sass-loader'
      //     ],
      // },
      // workaround for warning: System.import() is deprecated and will be removed soon. Use import() instead.
      {
        test: /[\/\\]@angular[\/\\].+\.js$/,
        parser: { system: true }
      },
      // {
      //     test: /\.(png|jp(e*)g|svg)$/,
      //     use: [{
      //         loader: 'url-loader',
      //         options: {
      //             limit: 8000, // Convert images < 8kb to base64 strings
      //             name: 'images/[hash]-[name].[ext]'
      //         }
      //     }]
      // }
      // {
      //     test: /\.(png|svg|jpg|gif)$/,
      //     use: [
      //         'file-loader',
      //     ],
      // },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'url-loader'
        }
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'assets',
        to: 'assets'
      },
      {
        from: './node_modules/bootstrap/dist/css/bootstrap.min.css',
        to: 'assets/css/bootstrap.min.css'
      },
      {
        from: './node_modules/jquery/dist/jquery.slim.min.js',
        to: 'assets/js/jquery.slim.min.js'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/indigo-pink.css',
        to: 'assets/css/indigo-pink.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css',
        to: 'assets/css/deeppurple-amber.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/pink-bluegrey.css',
        to: 'assets/css/pink-bluegrey.css'
      },
      {
        from:
          './node_modules/@angular/material/prebuilt-themes/purple-green.css',
        to: 'assets/css/purple-green.css'
      }
    ]),

    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new webpack.DefinePlugin({
      // global app config object
      config: JSON.stringify({
        apiUrl: 'http://localhost:5000/api'
      })
    }),

    // workaround for warning: Critical dependency: the request of a dependency is an expression
    new webpack.ContextReplacementPlugin(
      /\@angular(\\|\/)core(\\|\/)fesm5/,
      path.resolve(__dirname, 'src')
    )
    // new ExtractTextPlugin({ filename: '[name].scss', allChunks: true }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    },
    runtimeChunk: true
  },
  devServer: {
    historyApiFallback: true,
    inline: true,
    port: 4000,
    host: 'localhost'
  }
}
