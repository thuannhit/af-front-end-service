const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: path.resolve(__dirname, 'src/main.ts'),

    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            '@': path.resolve(__dirname, 'src/app/'),
            '$': path.resolve(__dirname, ''),
        }
    },
    output: {
        publicPath: '/',
        chunkFilename: '[name]-chunk.js',
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },

    optimization: {
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['ts-loader', 'angular2-template-loader', 'angular-router-loader',]
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader']
            },
            {
                test: /\.(css|scss)$/,
                include: [
                    path.join(__dirname, 'src/app/')
                ],
                loaders: ['to-string-loader', 'css-loader', 'sass-loader']
            },
            // workaround for warning: System.import() is deprecated and will be removed soon. Use import() instead.
            {
                test: /[\/\\]@angular[\/\\].+\.js$/,
                parser: { system: true }
            },
            {
                test: /\.(jpg|png)$/,
                use: {
                    loader: 'url-loader',
                },
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    plugins: [
        // new CopyWebpackPlugin([{ from: 'src/app/assets', to: 'assets' }]),
        new HtmlWebpackPlugin({ template: './src/index.html' }),
        new webpack.DefinePlugin({
            // global app config object
            config: JSON.stringify({
                apiUrl: 'http://localhost:5000'
            }),
        }),

        // workaround for warning: Critical dependency: the request of a dependency is an expression
        new webpack.ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            path.resolve(__dirname, 'src')
        ),

        // new ExtractTextPlugin({ filename: '[name].scss', allChunks: true }),
    ],

    devServer: {
        historyApiFallback: true,
        inline: true,
        port: 4000,
        host: 'localhost',
    }
}