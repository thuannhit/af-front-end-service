import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AppHomeComponent } from './modules/home';
// import { AuthGuard } from './shared/_helpers';
// import { ICAdmin } from './modules/ic/ic-admin';

const routes: Routes = [
    { path: '', component: AppHomeComponent},
    // { path: 'af/trading/login', component: AFLoginComponent },
    // { path: 'af/admin/login', component: AFAdminLoginComponent },
    // { path: 'af/trading/register', component: RegisterComponent },
    // { path: 'af/admin', loadChildren: () => import('@/modules/af/af-admin/af-admin.module').then(m => m.AFAdminModule) },
    { path: 'af', loadChildren: () => import('@/modules/af/af.module').then(m => m.AFModule) },
   
    // { path: 'ic/login', component: ICLoginComponent },
    // { path: 'ic/admin/login', component: ICAdminLoginComponent },
    // { path: 'ic/register', component: RegisterComponent },
    // { path: 'ic/admin', loadChildren: () => import('@/modules/ic/ic-admin/ic-admin.module').then(m => m.ICAdminModule) },
    // { path: 'af/login', component: LoginComponent },
    // { path: 'af/admin/login', component: AdminLoginComponent },
    // { path: 'af/register', component: RegisterComponent },
    // { path: 'af/admin', loadChildren: () => import('@/modules/ic/ic-admin/ic-admin.module').then(m => m.ICAdminModule) },
    // { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules });