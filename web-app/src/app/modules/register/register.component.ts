﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
// import { TranslateService } from '@ngx-translate/core';

import { AlertService, UserService, AuthenticationService } from '@/shared/_services_old';

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'register.component.html',
    // styles: [require('./register.component.scss')],
    styleUrls: ['register.component.scss']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    docTypes: { name: string, code: string }[] = [
        { name: 'ID Card', code: 'IDCard' }, { name: 'Passport', code: 'Passport' }
    ];
    policy4Registering="test";
    //TODO: Move this province list to the common utils.
    provinces: { name: string, code: string }[] = [
        { name: 'TP Hồ Chí Minh', code: "HCM" },
        { name: 'Hà Nội', code: 'HaNoi' },
        { name: 'Đà Nẵng', code: 'DaNang' },
        { name: 'An Giang', code: 'AnGiang' },
        { name: 'Bà Rịa - Vũng Tàu', code: 'VungTau' },
        { name: 'Bắc Giang', code: 'BacGiang' },
        { name: 'Bắc Kạn', code: 'BacKan' },
        { name: 'Bạc Liêu', code: 'BacLieu' },
        { name: 'Bắc Ninh', code: 'BacBinh' },
        { name: 'Bến Tre', code: 'BenTre' },
        { name: 'Bình Định', code: 'BinhDinh' },
        { name: 'Bình Dương', code: 'BinhDuong' },
        { name: 'Bình Phước', code: 'BinhPhuoc' },
        { name: 'Bình Thuận', code: 'BinhThuan' },
        { name: 'Cà Mau', code: 'CaMau' },
        { name: 'Cao Bằng', code: 'CaoBang' },
        { name: 'Đắk Lắk', code: 'DakLak' },
        { name: 'Đắk Nông', code: 'DakNong' },
        { name: 'Điện Biên', code: 'DienBien' },
        { name: 'Đồng Nai', code: 'DongNai' },
        { name: 'Đồng Tháp', code: 'DongThap' },
        { name: 'Gia Lai', code: 'GiaLai' },
        { name: 'Hà Giang	', code: 'HaGiang' },
        { name: 'Hà Nam', code: 'HaNam' },
        { name: 'Hà Tĩnh', code: 'HaTinh' },
        { name: 'Hải Dương', code: 'HaiDuong' },
        { name: 'Hậu Giang', code: 'HauGiang' },
        { name: 'Hòa Bình', code: 'HoaBinh' },
        { name: 'Hưng Yên', code: 'HungYen' },
        { name: 'Khánh Hòa', code: 'KhanhHoa' },
        { name: 'Kiên Giang', code: 'KienGiang' },
        { name: 'Kon Tum', code: 'KonTum' },
        { name: 'Lai Châu', code: 'LaiChau' },
        { name: 'Lâm Đồng', code: 'LamDong' },
        { name: 'Lạng Sơn', code: 'LangSon' },
        { name: 'Lào Cai', code: 'LaoCai' },
        { name: 'Long An', code: 'LongAn' },
        { name: 'Nam Định', code: 'NamDinh' },
        { name: 'Nghệ An', code: 'NgheAn' },
        { name: 'Ninh Bình', code: 'NinhBinh' },
        { name: 'Ninh Thuận', code: 'NinhThuan' },
        { name: 'Phú Thọ', code: 'PhuTho' },
        { name: 'Quảng Bình	', code: 'QuangBinh' },
        { name: 'Quảng Nam', code: 'QuangNam' },
        { name: 'Quảng Ngãi', code: 'QuangNgai' },
        { name: 'Quảng Ninh', code: 'QuangNinh' },
        { name: 'Quảng Trị', code: 'QuangTri' },
        { name: 'Sóc Trăng', code: 'SocTrang' },
        { name: 'Sơn La', code: 'SonLa' },
        { name: 'Tây Ninh', code: 'TayNinh' },
        { name: 'Thái Bình', code: 'ThaiBinh' },
        { name: 'Thái Nguyên', code: 'ThaiNguyen' },
        { name: 'Thanh Hóa', code: 'ThanhHoa' },
        { name: 'Thừa Thiên Huế', code: 'ThuaThienHue' },
        { name: 'Tiền Giang', code: 'TienGiang' },
        { name: 'Trà Vinh', code: 'TraVinh' },
        { name: 'Tuyên Quang', code: 'TuyenQuang' },
        { name: 'Vĩnh Long', code: 'VinhLong' },
        { name: 'Vĩnh Phúc', code: 'VinhPhuc' },
        { name: 'Yên Bái', code: 'YenBai' },
        { name: 'Phú Yên', code: 'PhuYen' },
        { name: 'Cần Thơ', code: 'CanTho' },
        { name: 'Hải Phòng', code: 'HaiPhong' }
    ];

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
        // public translate: TranslateService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }

        this.registerForm = formBuilder.group({
            hideRequired: false,
            floatLabel: 'auto',
        });
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            dateOfBirth: [{ value: '', isReadOnly: true }, [Validators.required]],
            gender: ['', [Validators.required]],
            PIN: ['', Validators.required],
            PINType: ['', Validators.required],
            issuedDate: [{ value: '', isReadOnly: true }, [Validators.required]],
            expiredDate: [{ value: '', isReadOnly: true }, [Validators.required]],
            address: ['', Validators.required],
            province: ['', Validators.required],
            phoneNumber: ['', Validators.required],
            bankAccountNo: ['', Validators.required],
            bankName: ['', Validators.required],
            branchName: ['', Validators.required],
            // password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    get genderForm() {
        return this.registerForm.get('gender');
    }
    onSubmit() {
        debugger;
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        debugger;
        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
