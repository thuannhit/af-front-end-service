﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService, AuthenticationService } from '@/shared/_services_old';

@Component({ templateUrl: 'home.component.html' })
export class AppHomeComponent implements OnInit {
    users = [];
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService
        // state: RouterStateSnapshot
    ) {
        // TODO: Implementing UI for asking whether routing to AF or IC
        // Currently, just navigating to af
        this.router.navigate(['af']);
    }

    ngOnInit() {
        this.router.navigate(['af']);
        // this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id)
            .pipe(first())
            .subscribe(() => this.loadAllUsers());
    }

    private loadAllUsers() {
        this.userService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);
    }
}