import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { AFLeftMenuComponent } from "@/modules/af/components/left-menu";
import { ThemePickerComponent } from '@/core/theme-picker/theme-picker.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JwtInterceptor, ErrorInterceptor } from '@/shared/_helpers';
import { AFLoginComponent, AFAdminLoginComponent } from './login.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { InternationalizationService } from '@/shared/_services';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatListModule,
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';

@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        HttpClientModule,
        MatSelectModule,
        // BrowserAnimationsModule,
        CommonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        TranslateModule.forRoot(),
        MatGridListModule,
        MatCardModule,
        MatTooltipModule,
        MatMenuModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatListModule,
        FlexLayoutModule
    ],
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatSelectModule,
        // BrowserAnimationsModule,
        // CommonModule,
        MatDatepickerModule,
        MatSidenavModule,
        MatListModule,
        AFAdminLoginComponent,
        AFLoginComponent
    ],
    declarations: [
        AFAdminLoginComponent,
        AFLoginComponent
    ],
    entryComponents: [],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        InternationalizationService,
    ],
    bootstrap: [AFAdminLoginComponent,
        AFLoginComponent]
})
export class LoginModule { };