﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {
    MessageType
} from '@/shared/_common/CONSTANTS'
import { DialogOption, Actions } from '@/shared/_models'
import { UserService } from '@/shared/_services'
import { UserNetService } from '@/shared/_services/_net-services'
import { UserLocalService, MessageDialogService } from '@/shared/_services/_local-services'
import { AlertService } from '@/shared/_services_old'
import { InternationalizationService } from '../../shared/_services'
@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class ICLoginComponent implements OnInit {
    loginForm: FormGroup
    loading = false
    submitted = false
    returnUrl: string
    translationsUrl = '/assets/i18n/login'
    en: any = {
        whyjoinus: 'You will have special access',
        createaccount: 'Create Account'
    }
    et: any = {
        whyjoinus: 'ምምዝጋብ ሓለፋ ተጠቃማይነት ኣለዎ',
        createaccount: 'ተመዝገብ'
    }
    lan: any = this.en
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService
    ) {
        // redirect to home if already logged in
        if (this.userService.currentAdminUserValue) {
            this.router.navigate(['/'])
        }

        // this.inznservice.language.subscribe((val) => {
        //     if (val == "en") {
        //         this.lan = translate.use(val, this.translationsUrl).then((data) => {
        //             console.log(translate.translateObject);
        //         });
        //     }
        //     else if (val == "vn") {
        //         this.lan = translate.use(val, this.translationsUrl);
        //     }
        // });
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls
    }

    onSubmit() {
        this.submitted = true

        // reset alerts on submit
        this.alertService.clear()

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return
        }

        this.loading = true
        let oInputData = {
            username: this.f.username.value,
            password: this.f.password.value,
            url: 'user/login '
        },
            bAsync = true
        // this.userService.login(oInputData, data => {
        //     debugger;
        // }, error => {
        //     debugger;
        // }, bAsync);
        // .pipe(first())
        // .subscribe(
        //     data => {
        //         this.router.navigate([this.returnUrl]);
        //     },
        //     error => {
        //         this.alertService.error(error);
        //         this.loading = false;
        //     });
    }
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class ICAdminLoginComponent implements OnInit {
    loginForm: FormGroup
    loading = false
    submitted = false
    returnUrl: string
    translationsUrl = '/assets/i18n/login'
    // lan: any = this.en;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService
    ) {
        // redirect to home if already logged in
        if (this.userService.currentAdminUserValue) {
            this.router.navigate(['ic/admin/ic-management'])
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls
    }

    onSubmit() {
        this.submitted = true

        // reset alerts on submit
        this.alertService.clear()

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return
        }

        this.loading = true
        let oData = {
            username: this.f.username.value,
            password: this.f.password.value
        }
        this.userService.adminLogin(
            oData,
            (user) => {
                this.router.navigate(['ic/admin/ic-management'])
            },
            (error) => {
                debugger
            }
        )
    }
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class AFLoginComponent implements OnInit {
    loginForm: FormGroup
    loading = false
    submitted = false
    returnUrl: string
    translationsUrl = '/assets/i18n/login'
    en: any = {
        whyjoinus: 'You will have special access',
        createaccount: 'Create Account'
    }
    et: any = {
        whyjoinus: 'ምምዝጋብ ሓለፋ ተጠቃማይነት ኣለዎ',
        createaccount: 'ተመዝገብ'
    }
    lan: any = this.en
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService
    ) {
        // redirect to home if already logged in
        if (this.userService.currentAdminUserValue) {
            this.router.navigate(['/'])
        }

        // this.inznservice.language.subscribe((val) => {
        //     if (val == "en") {
        //         this.lan = translate.use(val, this.translationsUrl).then((data) => {
        //             console.log(translate.translateObject);
        //         });
        //     }
        //     else if (val == "vn") {
        //         this.lan = translate.use(val, this.translationsUrl);
        //     }
        // });
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })

        // get return url from route parameters or default to '/'
        this.returnUrl =
            this.route.snapshot.queryParams['returnUrl'] || '/af/trading'
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls
    }

    onSubmit() {
        this.submitted = true

        // reset alerts on submit
        this.alertService.clear()

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return
        }

        this.loading = true
        let oInputData = {
            username: this.f.username.value,
            password: this.f.password.value,
            url: 'user/login '
        },
            bAsync = true
        // this.userService.login(oInputData, data => {
        //     debugger;
        // }, error => {
        //     debugger;
        // }, bAsync);
        // .pipe(first())
        // .subscribe(
        //     data => {
        //         this.router.navigate([this.returnUrl]);
        //     },
        //     error => {
        //         this.alertService.error(error);
        //         this.loading = false;
        //     });
    }
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class AFAdminLoginComponent implements OnInit {
    loginForm: FormGroup
    loading = false
    submitted = false
    returnUrl: string
    translationsUrl = '/assets/i18n/login'
    // lan: any = this.en;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userNetService: UserNetService,
        private userLocalService: UserLocalService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
        private messageDialogService: MessageDialogService
    ) {
        //TODO: logged in but please check jwt-expired
        // // redirect to home if already logged in
        // if (this.userService.currentAdminUserValue) {
        //     this.router.navigate(['af/admin/af-admin-home']);
        // }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/af/admin'
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls
    }

    onSubmit() {
        this.submitted = true

        // reset alerts on submit
        this.alertService.clear()

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return
        }

        this.loading = true
        let oData = {
            email: this.f.username.value,
            password: this.f.password.value,
            strategy: 'local'
        }
        let that = this
        this.userNetService
            .adminLogin(oData)
            .then(<GeneralUser>(user) => {
                // TODO: If it is opening login page from a specific page, plesae navigate back to that page, instead of admin hom page
                // this.router.navigate(['af/admin/af-admin-home']);
                that.userLocalService.storeCurentUser(user)
                that.router.navigate([that.returnUrl])
            })
            .catch((error) => { 
                let action: Actions[] = [
                    {
                        actionFn: () => {
                            this.loading = false
                            this.messageDialogService.closeDialog()
                        },
                        actionText: 'Try again'
                    },
                ]
                let option: DialogOption = {
                    type: MessageType.ACTIONS,
                    title: 'Failed to login',
                    message:
                        error.message,
                    Actions: action
                }
                this.messageDialogService.openDialog(option)
            })
    }

    onClickRegisterLink(): void {
        this.router.navigate(['af/admin/register'])
    }
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.scss']
})
export class AFTradingLoginComponent implements OnInit {
    loginForm: FormGroup
    loading = false
    submitted = false
    returnUrl: string
    translationsUrl = '/assets/i18n/login'
    // lan: any = this.en;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userNetService: UserNetService,
        private userLocalService: UserLocalService,
        private alertService: AlertService,
        private messageDialogService: MessageDialogService
    ) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/af/trading'
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls
    }

    onSubmit() {
        this.submitted = true

        // reset alerts on submit
        this.alertService.clear()

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return
        }

        this.loading = true
        let oData = {
            email: this.f.username.value,
            password: this.f.password.value,
            strategy: 'local'
        }
        let that = this
        this.userNetService
            .tradingLogin(oData)
            .then(<GeneralUser>(user) => {
                // TODO: If it is opening login page from a specific page, plesae navigate back to that page, instead of admin hom page
                // this.router.navigate(['af/admin/af-admin-home']);
                that.userLocalService.storeCurentUser(user)
                that.router.navigate([that.returnUrl])
            })
            .catch((error) => {
                let action: Actions[] = [
                    {
                        actionFn: () => {
                            this.loading = false
                            this.messageDialogService.closeDialog()
                        },
                        actionText: 'Try again'
                    },
                ]
                let option: DialogOption = {
                    type: MessageType.ACTIONS,
                    title: 'Failed to login',
                    message:
                        error.message,
                    Actions: action
                }
                this.messageDialogService.openDialog(option)
            })
    }

    onClickRegisterLink(): void {
        this.router.navigate(['af/trading/register'])
    }
}
