import { Router } from '@angular/router';


import { UserService } from '@/shared/_services';
import { AdminUser } from '@/shared/_models';
import { Component, OnInit, PLATFORM_ID, APP_ID, Inject, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { InternationalizationService } from '@/shared/_services'; //this is the service

@Component({ templateUrl: 'ic-admin.component.html', styleUrls: ['ic-admin.component.scss']})
export class ICAdminComponent implements OnDestroy {
    currentUser: AdminUser;
    // translate: TranslateService;
    constructor(
        // private inznservice: InternationalizationService,
        private router: Router,
        private authenticationService: UserService,
    ) {
        this.authenticationService.currentAdminUser.subscribe(x => this.currentUser = x);
    }
    ngOnDestroy() {
    }
}
