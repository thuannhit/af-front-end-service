import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { ICProductService, InternationalizationService, ValidationService } from '@/shared/_services';

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'ic-interest-dialog.component.html', styleUrls: ['ic-interest-dialog.component.scss'] })
export class ICInterestCreationDialogComponent implements OnInit {
    icCreationForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-admin';
    constructor(
        public dialogRef: MatDialogRef<ICInterestCreationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private icProductService: ICProductService,
        private alertService: AlertService,
        private inznservice: InternationalizationService, ) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
    ngOnInit() {
        this.icCreationForm = this.formBuilder.group({
            interest: ['', [Validators.required, ValidationService.decimalValidation]],
            period: ['', [Validators.required, ValidationService.numberValidation]],
        });
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    get f() { return this.icCreationForm.controls; }
    onCloseCreationDialog() {
        this.dialogRef.close();
    }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.icCreationForm.invalid) {
            return;
        }
        debugger;
        this.loading = true;
        let oData = {
            interest: this.f.interest.value,
            period: this.f.period.value,
        }
        this.icProductService.addICInterestOption(oData, oResult => {
            this.onCloseCreationDialog();
        }, oError => {
            debugger;
        });
    }

}