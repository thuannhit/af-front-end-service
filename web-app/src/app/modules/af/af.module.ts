import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AFHomeComponent } from "@/modules/af/af-home";
import { FlexLayoutModule } from '@angular/flex-layout';
import { afRoutingModule } from './af.routing';
import { JwtInterceptor, ErrorInterceptor } from '@/shared/_helpers';
import { AFComponent } from './af.component';
import { CommonModule } from '@angular/common';
import { SidenavService } from '@/shared/_services/_local-services/sidenav.service'
import { AFFooterComponent } from '@/modules/af/components/af-footer'
import { AFHeaderComponent } from '@/modules/af/components/af-header'
import { AFSidenavListComponent } from '@/modules/af/components/sidenav-list'
import { I18nModule } from '@/i18n/i18n.module';
import { ThemePickerComponent } from '@/core/theme-picker'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatListModule,
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/af/', '.json');
}
@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        HttpClientModule,
        afRoutingModule,
        I18nModule,
        CommonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        MatGridListModule,
        MatCardModule,
        MatTooltipModule,
        MatMenuModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatListModule,
        FlexLayoutModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        })
    ],
    exports: [
    ],
    declarations: [
        AFComponent,
        AFHomeComponent,
        AFFooterComponent,
        AFHeaderComponent,
        AFSidenavListComponent,
        ThemePickerComponent,
    ],
    entryComponents: [],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        SidenavService,
    ],
    bootstrap: [AFComponent]
})
export class AFModule { };