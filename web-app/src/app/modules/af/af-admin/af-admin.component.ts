import { Router } from '@angular/router'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { onMainContentChange } from '@/modules/af/af-admin/components/left-menu/animations/animations'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'
@Component({
    templateUrl: 'af-admin.component.html',
    styleUrls: ['af-admin.component.scss'],
    animations: [onMainContentChange]
})
export class AFAdminComponent extends I18nComponent implements OnDestroy {
    constructor(
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
    }
    ngOnDestroy() { }
    ngAfterViewInit() { }
}
