import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import {
    HttpClient,
    HttpClientModule,
} from '@angular/common/http'
import { AFAdminHomePageComponent } from '@/modules/af/af-admin/pages/admin-home'
import { AFAdminLoginComponent } from '@/modules/login'
import { AFAdminRegisterComponent } from '@/modules/af/components/af-register'
import { AFLeftMenuComponent } from '@/modules/af/af-admin/components/left-menu'
import { afAdminRoutingModule } from './af-admin.routing'
import { AFAdminComponent } from './af-admin.component'
import { CommonModule } from '@angular/common'
import {
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
} from '@angular/material'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { NgMaterialMultilevelMenuModule } from '@/modules/af/shared-modules/material-multilevel-menu'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    console.log('Get i18n for af-admin')
    return new TranslateHttpLoader(http, './assets/i18n/af-admin/', '.json')
}

@NgModule({
    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afAdminRoutingModule,
        CommonModule,
        MatDatepickerModule,
        MatTooltipModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        NgMaterialMultilevelMenuModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        })
    ],
    exports: [
    ],
    declarations: [
        AFAdminComponent,
        AFAdminHomePageComponent,
        AFAdminLoginComponent,
        AFAdminRegisterComponent,
        AFLeftMenuComponent,
    ],
    entryComponents: [AFAdminComponent],
    providers: [
    ],
    bootstrap: [AFAdminComponent]
})
export class AFAdminModule { }
