import { Routes, RouterModule } from '@angular/router';

import { AFAdminHomePageComponent } from '@/modules/af/af-admin/pages/admin-home';
import { AFAdminLoginComponent } from '@/modules/login';
import { AFAdminRegisterComponent } from '@/modules/af/components/af-register';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFAdminComponent } from '@/modules/af/af-admin/af-admin.component';

const routes: Routes = [
    { path: 'login', component: AFAdminLoginComponent },
    { path: 'register', component: AFAdminRegisterComponent },
    {
        path: '',
        component: AFAdminComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: 'af-admin-home', component: AFAdminHomePageComponent, canActivate: [AFAdminAuthGuard] },
            { path: 'stock-order-management', loadChildren: () => import('@/modules/af/af-admin/pages/stock-order-management/stock-order-management.module').then(m => m.AFStockOrderManagementModule), canActivate: [AFAdminAuthGuard] },
            { path: 'cash-management', loadChildren: () => import('@/modules/af/af-admin/pages/cash-management/cash-management.module').then(m => m.AFCashManagementModule), canActivate: [AFAdminAuthGuard] },
            { path: 'system-configuration', loadChildren: () => import('@/modules/af/af-admin/pages/system-configuration/system-configuration.module').then(m => m.AFSysConfigModule), canActivate: [AFAdminAuthGuard] },
            { path: 'user-management', loadChildren: () => import('@/modules/af/af-admin/pages/user-management/user-management.module').then(m => m.AFUserManagementModule), canActivate: [AFAdminAuthGuard] },
            { path: 'reports', loadChildren: () => import('@/modules/af/af-admin/pages/reports/reports.module').then(m => m.AFReportsModule), canActivate: [AFAdminAuthGuard] },
            { path: 'company-cost', loadChildren: () => import('@/modules/af/af-admin/pages/company-cost/company-cost.module').then(m => m.AFCompanyCostManagementModule), canActivate: [AFAdminAuthGuard] },
            { path: 'deposit', loadChildren: () => import('@/modules/af/af-admin/pages/deposits/deposits.module').then(m => m.AFDepositManagementModule), canActivate: [AFAdminAuthGuard] },
        ],
    }

    // otherwise redirect to home
    // { path: '**', redirectTo: 'af/admin/af-admin-home' }
];

export const afAdminRoutingModule = RouterModule.forChild(routes);