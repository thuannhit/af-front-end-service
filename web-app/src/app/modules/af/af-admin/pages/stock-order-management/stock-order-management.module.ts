import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { AFStockOrderManagementComponent } from '@/modules/af/af-admin/pages/stock-order-management'
import { afStockOrderManagementRoutingModule } from './stock-order-management.routing'
import { StockOrderListComponent } from '@/modules/af/af-admin/components/stock-order-list'
import { StockOrderFormComponent } from '@/modules/af/af-admin/components/stock-order-form'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatListModule,
    MatRadioModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/stock-order-management/',
        '.json'
    )
}
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afStockOrderManagementRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatSelectModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule,
        MatChipsModule,
        NgxMaterialTimepickerModule,
        MatListModule,
        MatRadioModule
    ],
    exports: [HttpClientModule],
    declarations: [
        AFStockOrderManagementComponent,
        StockOrderListComponent,
        StockOrderFormComponent
    ],
    entryComponents: [],
    providers: [SystemConfigService],
    bootstrap: [AFStockOrderManagementComponent]
})
export class AFStockOrderManagementModule { }
