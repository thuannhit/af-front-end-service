import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    UserLocalService,
} from '@/shared/_services/_local-services'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'stock-order-management.component.html',
    styleUrls: ['stock-order-management.component.scss']
})
export class AFStockOrderManagementComponent extends I18nComponent implements OnInit, AfterViewInit {
    returnUrl: string
    selectedFormTab: number
    navLinks: any[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }

        this.navLinks = [
            {
                label: 'STOCK_ORDER_FORM',
                link: '/af/admin/stock-order-management/stock-order-form',
                index: 0
            },
            {
                label: 'STOCK_ORDER_LIST',
                link: '/af/admin/stock-order-management/stock-order-list',
                index: 1
            }
        ]
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        super.ngOnInit()
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }
    ngAfterViewInit() { }
    ngOnDestroy() { }

    // onChangingSelectedFrom(oData) {
    //     const aFragments = ['stock-order-form', 'stock-order-list'];
    //     const currentMainURL = this.router.parseUrl(this.router.url).root.children['primary'].segments.map(it => it.path).join('/');
    //     this.router.navigate([currentMainURL], { fragment: aFragments[!!oData.index ? oData.index : 0] })
    // }
}
