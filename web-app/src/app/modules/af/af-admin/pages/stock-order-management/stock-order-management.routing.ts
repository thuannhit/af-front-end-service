import { Routes, RouterModule } from '@angular/router'
import { AFAdminAuthGuard } from '@/shared/_helpers'
import { StockOrderFormComponent } from '@/modules/af/af-admin/components/stock-order-form'
import { StockOrderListComponent } from '@/modules/af/af-admin/components/stock-order-list'
import { AFStockOrderManagementComponent } from '@/modules/af/af-admin/pages/stock-order-management'

const routes: Routes = [
    {
        path: '',
        component: AFStockOrderManagementComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'stock-order-form' },
            { path: 'stock-order-form', component: StockOrderFormComponent },
            { path: 'stock-order-list', component: StockOrderListComponent }
        ]
    }
]

export const afStockOrderManagementRoutingModule = RouterModule.forChild(routes)
