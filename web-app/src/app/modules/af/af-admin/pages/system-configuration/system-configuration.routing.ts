import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFEscrowConfigComponent } from '@/modules/af/af-admin/components/escrow-config';
import { AFInterestConfigComponent } from '@/modules/af/af-admin/components/interest-config';
import { AFTransCostConfigComponent } from '@/modules/af/af-admin/components/trans-cost-config';
import { AFBankingConfigComponent } from '@/modules/af/af-admin/components/banking-config';
import { AFTransactionTaxConfigComponent } from '@/modules/af/af-admin/components/tax-config';
import { AFSysConfigComponent } from '@/modules/af/af-admin/pages/system-configuration';
import { AFDividendManagementComponent} from '@/modules/af/af-admin/components/dividend'


const routes: Routes = [
    {
        path: '',
        component: AFSysConfigComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'banking' },
            { path: 'banking', component: AFBankingConfigComponent },
            { path: 'tax', component: AFTransactionTaxConfigComponent },
            { path: 'interest', component: AFInterestConfigComponent },
            { path: 'trans-cost', component: AFTransCostConfigComponent },
            { path: 'escrow', component: AFEscrowConfigComponent },
            { path: 'dividend', component: AFDividendManagementComponent },
        ],
    }
];

export const afSysConfigRoutingModule = RouterModule.forChild(routes);