import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import {
    HttpClient,
    HttpClientModule,
    HTTP_INTERCEPTORS
} from '@angular/common/http'
import { JwtInterceptor, ErrorInterceptor } from '@/shared/_helpers'
import { AFSysConfigComponent } from '@/modules/af/af-admin/pages/system-configuration'
import { afSysConfigRoutingModule } from './system-configuration.routing'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import { AFEscrowConfigComponent } from '@/modules/af/af-admin/components/escrow-config'
import { AFAddNewStockComponent } from '@/modules/af/af-admin/components/escrow-config/new-stock-dialog'
import { AFInterestConfigComponent } from '@/modules/af/af-admin/components/interest-config'
import { AFAddNewInterestComponentDialog } from '@/modules/af/af-admin/components/interest-config/new-interest-dialog'
import { AFTransCostConfigComponent } from '@/modules/af/af-admin/components/trans-cost-config'
import { AFAddNewTransCostComponentDialog } from '@/modules/af/af-admin/components/trans-cost-config/new-trans-cost-dialog'
import { AFBankingConfigComponent } from '@/modules/af/af-admin/components/banking-config'
import { AFAddNewBankComponentDialog } from '@/modules/af/af-admin/components/banking-config/new-bank-dialog'
import { AFTransactionTaxConfigComponent } from '@/modules/af/af-admin/components/tax-config'
import { AFAddNewTaxComponentDialog } from '@/modules/af/af-admin/components/tax-config/new-tax-dialog'
import { AFDividendManagementComponent } from '@/modules/af/af-admin/components/dividend'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDatepickerModule,
    MatSelectModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/system-configuration/',
        '.json'
    )
}

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afSysConfigRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        MatDialogModule,
        MatDatepickerModule,
        MatSelectModule,
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule
    ],
    exports: [HttpClientModule],
    declarations: [
        AFSysConfigComponent,
        AFEscrowConfigComponent,
        AFAddNewStockComponent,
        AFInterestConfigComponent,
        AFAddNewInterestComponentDialog,
        AFTransCostConfigComponent,
        AFAddNewTransCostComponentDialog,
        AFBankingConfigComponent,
        AFAddNewBankComponentDialog,
        AFTransactionTaxConfigComponent,
        AFAddNewTaxComponentDialog,
        AFDividendManagementComponent
    ],
    entryComponents: [
        AFAddNewStockComponent,
        AFAddNewInterestComponentDialog,
        AFAddNewTransCostComponentDialog,
        AFAddNewBankComponentDialog,
        AFAddNewTaxComponentDialog
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        SystemConfigService
    ],
    bootstrap: [AFSysConfigComponent]
})
export class AFSysConfigModule { }
