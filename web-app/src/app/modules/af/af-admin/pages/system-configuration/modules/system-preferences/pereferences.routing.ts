import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFEscrowConfigComponent } from '@/modules/af/af-admin/components/escrow-config';
import { AFInterestConfigComponent } from '@/modules/af/af-admin/components/interest-config';
import { AFSysConfigComponent } from '@/modules/af/af-admin/pages/system-configuration';


const routes: Routes = [
    {
        path: '',
        component: AFSysConfigComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'banking' },
            { path: 'banking', component: AFEscrowConfigComponent },
            { path: 'interest', component: AFInterestConfigComponent },
            { path: 'escrow', component: AFEscrowConfigComponent },
        ],
    }
];

export const afSysConfigRoutingModule = RouterModule.forChild(routes);