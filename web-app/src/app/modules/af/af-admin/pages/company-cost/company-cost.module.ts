import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpClient } from '@angular/common/http'
import { AFCompanyCostManagementComponent } from '@/modules/af/af-admin/pages/company-cost'
import { AFCompanyCostSubjectsManagementComponent } from '@/modules/af/af-admin/components/company-cost-subjects'
import { afCompanyCostManagementRoutingModule } from './company-cost.routing'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatExpansionModule,
    MatChipsModule,
    MatListModule,
    MatRadioModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/company-cost/',
        '.json'
    )
}
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afCompanyCostManagementRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatSelectModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule,
        MatChipsModule,
        NgxMaterialTimepickerModule,
        MatListModule,
        MatRadioModule
    ],
    declarations: [
        AFCompanyCostManagementComponent,
        AFCompanyCostSubjectsManagementComponent
    ],
    bootstrap: [AFCompanyCostManagementComponent]
})
export class AFCompanyCostManagementModule { }
