import { Routes, RouterModule } from '@angular/router'
import { AFAdminAuthGuard } from '@/shared/_helpers'
// import { StockOrderFormComponent } from '@/modules/af/af-admin/components/stock-order-form'
// import { StockOrderListComponent } from '@/modules/af/af-admin/components/stock-order-list'
import { AFCompanyCostManagementComponent } from '@/modules/af/af-admin/pages/company-cost'
import { AFCompanyCostSubjectsManagementComponent } from '@/modules/af/af-admin/components/company-cost-subjects'

const routes: Routes = [
    {
        path: '',
        component: AFCompanyCostManagementComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            // { path: '', redirectTo: 'stock-order-form' },
            { path: 'company-cost-subjects', component: AFCompanyCostSubjectsManagementComponent },
            // { path: 'stock-order-list', component: StockOrderListComponent }
        ]
    }
]

export const afCompanyCostManagementRoutingModule = RouterModule.forChild(routes)
