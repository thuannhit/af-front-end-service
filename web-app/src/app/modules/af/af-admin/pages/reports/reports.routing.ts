import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFReportsComponent } from '@/modules/af/af-admin/pages/reports';
import { AFLoansReportComponent } from '@/modules/af/af-admin/components/loans-report';
import { AFEscrowReportComponent } from '@/modules/af/af-admin/components/escrow-report';


const routes: Routes = [
    {
        path: '',
        component: AFReportsComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'loans-report' },
            { path: 'loans-report', component: AFLoansReportComponent },
            { path: 'escrows-report', component: AFEscrowReportComponent },
        ],
    }
];

export const afReportsRoutingModule = RouterModule.forChild(routes);