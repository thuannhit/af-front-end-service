import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    UserLocalService,
} from '@/shared/_services/_local-services'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'reports.component.html',
    styleUrls: ['reports.component.scss']
})
export class AFReportsComponent extends I18nComponent implements OnInit, AfterViewInit {
    returnUrl: string
    selectedFormTab: number
    navLinks: any[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }

        this.navLinks = [
            {
                label: 'ASSETS_REPORT',
                link: '/af/admin/stock-order-management/stock-order-form',
                index: 0
            },
            {
                label: 'ESCROW_REPORT',
                link: '/af/admin/stock-order-management/stock-order-list',
                index: 1
            }
        ]
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        super.ngOnInit()
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }
    ngAfterViewInit() { }
    ngOnDestroy() { }

}
