import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { AFReportsComponent } from '@/modules/af/af-admin/pages/reports'
import { afReportsRoutingModule } from './reports.routing'
import { AFLoansReportComponent } from '@/modules/af/af-admin/components/loans-report'
import { AFEscrowReportComponent } from '@/modules/af/af-admin/components/escrow-report'
import { AFShowMatchHistoryDialog } from '@/modules/af/af-admin/components/loans-report/match-history-dialog'
import { MatchResutHistoryModule } from '@/modules/af/shared-modules/match-result-history'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatChipsModule,
    MatRadioModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/reports/',
        '.json'
    )
}
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afReportsRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatSelectModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule,
        MatChipsModule,
        NgxMaterialTimepickerModule,
        MatRadioModule,
        MatchResutHistoryModule
    ],
    exports: [HttpClientModule],
    declarations: [
        AFReportsComponent,
        AFLoansReportComponent,
        AFEscrowReportComponent,
        AFShowMatchHistoryDialog
    ],
    entryComponents: [AFShowMatchHistoryDialog],
    providers: [SystemConfigService],
    bootstrap: [AFReportsComponent]
})
export class AFReportsModule { }
