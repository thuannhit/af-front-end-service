import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { AFDepositComponent } from '@/modules/af/af-admin/pages/deposits'
import { AFDepositCustomersComponent } from '@/modules/af/af-admin/components/deposit-customers'
import { AFDepositInterestConfigComponent } from '@/modules/af/af-admin/components/deposit-config'
import { AFAddNewDepositInterestComponentDialog } from '@/modules/af/af-admin/components/deposit-config/new-interest-dialog'
import { AFDepositReportsComponent } from '@/modules/af/af-admin/components/deposit-reports'
import { afReportsRoutingModule } from './deposits.routing'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatChipsModule,
    MatRadioModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/deposits/',
        '.json'
    )
}
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afReportsRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatSelectModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule,
        MatChipsModule,
        NgxMaterialTimepickerModule,
        MatRadioModule
    ],
    exports: [HttpClientModule],
    declarations: [
        AFDepositComponent,
        AFDepositCustomersComponent,
        AFDepositInterestConfigComponent,
        AFAddNewDepositInterestComponentDialog,
        AFDepositReportsComponent
    ],
    entryComponents: [AFAddNewDepositInterestComponentDialog],
    providers: [SystemConfigService],
    bootstrap: [AFDepositComponent]
})
export class AFDepositManagementModule { }
