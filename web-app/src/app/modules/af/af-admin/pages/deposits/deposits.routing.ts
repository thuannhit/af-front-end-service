import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFDepositComponent } from '@/modules/af/af-admin/pages/deposits';
// import { AFDepositReportComponent } from '@/modules/af/af-admin/components/deposit-reports';
import { AFDepositInterestConfigComponent } from '@/modules/af/af-admin/components/deposit-config';
import { AFDepositCustomersComponent } from '@/modules/af/af-admin/components/deposit-customers';
import { AFDepositReportsComponent } from '@/modules/af/af-admin/components/deposit-reports';


const routes: Routes = [
    {
        path: '',
        component: AFDepositComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'deposit-reports' },
            { path: 'deposit-configs', component: AFDepositInterestConfigComponent },
            { path: 'deposit-customers', component: AFDepositCustomersComponent },
            { path: 'deposit-reports', component: AFDepositReportsComponent },
        ],
    }
];

export const afReportsRoutingModule = RouterModule.forChild(routes);