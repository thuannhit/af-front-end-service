import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { CashManagementComponent } from '@/modules/af/af-admin/pages/cash-management/cash-management.component';
import { TopupFormComponent } from '@/modules/af/af-admin/components/top-up';
import { WithdrawFormComponent } from '@/modules/af/af-admin/components/withdraw';


const routes: Routes = [
    {
        path: '',
        component: CashManagementComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'top-up' },
            { path: 'with-draw', component: WithdrawFormComponent },
            { path: 'top-up', component: TopupFormComponent },
            { path: 'profit-settlement', component: CashManagementComponent },
        ],
    }
];

export const afCashManagementRoutingModule = RouterModule.forChild(routes);