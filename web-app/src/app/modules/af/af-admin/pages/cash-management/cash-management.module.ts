import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { TopupFormComponent } from "@/modules/af/af-admin/components/top-up";
import { WithdrawFormComponent } from "@/modules/af/af-admin/components/withdraw";
import { CashManagementComponent } from '@/modules/af/af-admin/pages/cash-management';
import { afCashManagementRoutingModule } from './cash-management.routing';
import { FinancialStatementsModule } from '@/modules/af/shared-modules/financial-statements';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/';
import { SystemConfigService } from '@/shared/_services/_net-services/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function createTranslateLoader(http: HttpClient) {
    console.log('Loading i18n file for cash management')
    return new TranslateHttpLoader(http, './assets/i18n/cash-management/', '.json');
}
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatSidenavModule,
    MatListModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatSortModule,
    MatChipsModule
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        HttpClientModule,
        afCashManagementRoutingModule,
        MatSelectModule,
        CommonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        MatGridListModule,
        MatCardModule,
        MatTooltipModule,
        MatMenuModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatSidenavModule,
        MatListModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatSortModule,
        FinancialStatementsModule,
        NgxMaterialTimepickerModule,
        ngTHMatInputCommifiedModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        MatChipsModule
    ],
    exports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        HttpClientModule,
        CommonModule,

    ],
    declarations: [
        CashManagementComponent,
        TopupFormComponent,
        WithdrawFormComponent
    ],
    entryComponents: [
    ],
    providers: [
        SystemConfigService
    ],
    bootstrap: [CashManagementComponent]
})
export class AFCashManagementModule { };