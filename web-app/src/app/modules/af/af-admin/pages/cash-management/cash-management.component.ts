import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { UserLocalService } from '@/shared/_services';
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'
@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'cash-management.component.html',
    styleUrls: ['cash-management.component.scss'],
})
export class CashManagementComponent extends I18nComponent implements OnInit, AfterViewInit {
    stockOrderForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    selectedFormTab: number;
    title = 'angular-material-tab-router';
    navLinks: any[];
    activeLinkIndex = -1; 
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        public dialog: MatDialog,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate);
        this.navLinks = [
            {
                label: 'TOP_UP',
                link: '/af/admin/cash-management/top-up',
                index: 0
            }, {
                label: 'WITHDRAW',
                link: '/af/admin/cash-management/with-draw',
                index: 1
            }
        ];
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login']);
        }


        // this.selectedFormTab = (+this.location.path(true).substr(1) - 1) || 0;
        const aForms = {
            'stock-order-form': 0,
            'stock-order-list': 1
        }
        this.selectedFormTab = !!this.route.snapshot.fragment ? aForms[this.route.snapshot.fragment] : 0

    }

    ngOnInit() {
        super.ngOnInit();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.router.events.subscribe((res) => {
            this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
        });

    }
    ngAfterViewInit() {
    }

    onChangingSelectedFrom(oData) {
        const aFragments = ['stock-order-form', 'stock-order-list'];
        const currentMainURL = this.router.parseUrl(this.router.url).root.children['primary'].segments.map(it => it.path).join('/');
        this.router.navigate([currentMainURL], { fragment: aFragments[!!oData.index ? oData.index : 0] })
    }
}
