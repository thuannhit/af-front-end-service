import { Component, OnInit, ViewEncapsulation, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { UserLocalService, InternationalizationService, ValidationService } from '@/shared/_services/_local-services';
import { API_CONSTANT } from '@/shared/_common';
@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'admin-home.component.html', styleUrls: ['admin-home.component.scss'] })
export class AFAdminHomePageComponent implements OnInit {
    loading = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-admin';
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login']);
        }
    }

    ngOnInit() {

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
}

