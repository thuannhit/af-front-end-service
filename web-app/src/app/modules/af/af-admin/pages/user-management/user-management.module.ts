import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import { afUserManagementRoutingModule } from './user-management.routing'
import { AFUserManagementComponent } from './user-management.component'
import { AFUserGeneralManagement } from '@/modules/af/af-admin/components/user-general-management'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDialogModule,
    MatDatepickerModule,
    MatSelectModule
} from '@angular/material'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afUserManagementRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        MatDialogModule,
        MatDatepickerModule,
        MatSelectModule
    ],
    exports: [],
    declarations: [
        AFUserManagementComponent,
        AFUserGeneralManagement
    ],
    entryComponents: [
    ],
    providers: [SystemConfigService],
    bootstrap: [AFUserManagementComponent]
})
export class AFUserManagementModule { }
