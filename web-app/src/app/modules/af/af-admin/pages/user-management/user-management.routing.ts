import { Routes, RouterModule } from '@angular/router';
import { AFAdminAuthGuard } from '@/shared/_helpers';
import { AFUserGeneralManagement } from '@/modules/af/af-admin/components/user-general-management';
import { AFUserManagementComponent } from '@/modules/af/af-admin/pages/user-management/user-management.component';


const routes: Routes = [
    {
        path: '',
        component: AFUserManagementComponent,
        canActivate: [AFAdminAuthGuard],
        children: [
            { path: '', redirectTo: 'general' },
            { path: 'general', component: AFUserGeneralManagement },
        ],
    }
];

export const afUserManagementRoutingModule = RouterModule.forChild(routes);