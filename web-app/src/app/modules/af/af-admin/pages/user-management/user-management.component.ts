import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from '@/shared/_services_old';
import { UserLocalService, ICProductService, InternationalizationService, ValidationService } from '@/shared/_services';
@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'user-management.component.html',
    styleUrls: ['user-management.component.scss'],
})
export class AFUserManagementComponent implements OnInit, AfterViewInit {
    loading = false;
    submitted = false;
    returnUrl: string;
    selectedFormTab: number;
    navLinks: any[];
    activeLinkIndex = -1;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
    ) {
        this.navLinks = [
            {
                label: 'General',
                link: '/af/admin/user-management/general',
                index: 0
            },
        ];
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login']);
        }


        // this.selectedFormTab = (+this.location.path(true).substr(1) - 1) || 0;
        const aForms = {
            'stock-order-form': 0,
            'stock-order-list': 1
        }
        this.selectedFormTab = !!this.route.snapshot.fragment ? aForms[this.route.snapshot.fragment] : 0

    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.router.events.subscribe((res) => {
            this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
        });

    }
    ngAfterViewInit() {
        // this.getPublishedICsList();
    }
    ngOnDestroy() { }

    onChangingSelectedFrom(oData) {
        const aFragments = ['stock-order-form', 'stock-order-list'];
        const currentMainURL = this.router.parseUrl(this.router.url).root.children['primary'].segments.map(it => it.path).join('/');
        this.router.navigate([currentMainURL], { fragment: aFragments[!!oData.index ? oData.index : 0] })
    }
}
