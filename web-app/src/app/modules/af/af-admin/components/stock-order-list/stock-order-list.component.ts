import {
    Component,
    OnInit,
    AfterViewInit,
    ViewChild,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import { MatSort, Sort } from '@angular/material'
import { MatPaginator, PageEvent } from '@angular/material'
import { SpinnerService } from '@/shared/_services/_local-services/spinner.service'
import {
    UserLocalService,
    NetHandlerService,
    ValidationService,
    DayTimeHelperSerivice
} from '@/shared/_services/_local-services'
import {
    StockOrderService,
    StockOrderDetailService,
    SystemConfigService,
    UserNetService
} from '@/shared/_services/_net-services'
import {
    StockOrderTradingType,
    StockOrderType,
    StockOrderStatus,
    FinancialServiceCMD,
    UserRole
} from '@/shared/_common'
import { invert, find, findIndex } from 'lodash'

export interface ReturnedCustomer {
    email: string
    first_name: string
    last_name: string
    _id: number
    name?: string
}
export interface ReturnedCustomersRs {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}
export interface ReturnedAMatched {
    match_date_time: number
    match_price: number
    match_amount: number
    property_return_date: number
    tax?: number
    transaction_cost?: number
    advance_fee?: number
}
export interface ReturnedMatchedList {
    total: number
    limit: number
    skip: number
    data: ReturnedAMatched[]
}
export interface MatchedBuyRs {
    match_date_time: number
    property_return_date: number
    match_price: number
    match_amount: number
    _stock_order_id: number
    transaction_cost?: number
}

export interface MatchSellRs {
    match_date_time: number
    property_return_date: number
    tax?: number
    advance_fee?: number
    transaction_cost?: number
    match_price: number
    match_amount: number
    _stock_order_id: number
}

export interface Customer {
    _id: number
    email: string
    first_name: string
    last_name: string
}
export interface Stock {
    escrow_rate: number
    code: string
}

export interface FinanceInfo {
    cashInAccount: number
    validCashForBuying: number
}

export interface ReturnedStockOrder {
    _id: number
    user: Customer
    escrow_rca: number
    escrow_rate?: number
    order_type: number
    trading_date: any
    amount: string
    min_price: number
    max_price: number
    _user_id: number
    stock: Stock
    status: number
    matchedDetails?: object[]
    isOpenMatchResultForm?: boolean
    interest_rate: number
    property_return_date?: Date
    // disabledDate?: string;
}
export interface ReturnedStockOrderList {
    total: number
    limit: number
    skip: number
    data: ReturnedStockOrder[]
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'stock-order-list',
    templateUrl: 'stock-order-list.component.html',
    styleUrls: ['stock-order-list.component.scss']
})
export class StockOrderListComponent implements OnInit, AfterViewInit {
    listOfCustomers: ReturnedCustomer[]
    stockOrderTradingType = StockOrderTradingType

    stockOrderType = StockOrderType
    stockOrderStatus = StockOrderStatus
    stockOrderStatusString = invert(StockOrderStatus)
    stockOrderStatusColor = {
        NEW: 'yellow',
        APPROVED: 'blue',
        IN_PROGRESS: 'pink',
        DONE: 'green',
        REJECTED: 'red'
    }
    newMatchedBUYRecordForm: FormGroup
    newMatchedSELLRecordForm: FormGroup
    loading = false
    returnUrl: string
    tradingActions = [
        { code: 'BUY', value: 'Buy' },
        { code: 'SELL', value: 'Sell' }
    ]
    currentFinanceInfo = {
        cashInAccount: 0,
        validCashForBuying: 0
    }
    startDate: Date
    minValid4SelectingDate: Date
    maxValid4SelectingDate: Date
    invalidPublishedDate = (d: Date): boolean => {
        const day = d.getDay()
        // Prevent Saturday and Sunday from being selected.
        return day !== 0 && day !== 6
    }
    @ViewChild(MatSort, { static: true }) sort: MatSort
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    stockOrderLists: ReturnedStockOrder[]
    matchedDetailsTableCols = [
        'matchedTime',
        'matchedPrice',
        'matchedAmount',
        'transactionCost',
        'propertyReturnDate'
    ]
    //
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    selectedOrderTransactionCostRate: number = 0
    selectedOrderTransactionTaxRate: number = 0
    todayTax: number = 0
    defaultMatchTime: string
    targetUser: ReturnedCustomer = {
        _id: null,
        email: '',
        first_name: '',
        last_name: '',
        name: '',
    }
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        public dialog: MatDialog,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private stockOrderService: StockOrderService,
        private stockOrderDetailService: StockOrderDetailService,
        private sysConfigService: SystemConfigService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private dayTimeHelperService: DayTimeHelperSerivice,
        private userNetService: UserNetService,
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareMatchedBUYRecordForm()
        this.prepareMatchedSELLRecordForm()

        const today = new Date()
        this.minValid4SelectingDate = new Date(today)
        this.maxValid4SelectingDate = new Date(today)
        this.minValid4SelectingDate.setDate(
            this.minValid4SelectingDate.getDate() - 7
        )
        this.maxValid4SelectingDate.setDate(
            this.maxValid4SelectingDate.getDate() + 30
        )
        this.defaultMatchTime = '09:30'
    }

    onChangingCustomer(customer: ReturnedCustomer): void {
        if (!!customer) {
            this.targetUser = customer;
            this.getStockOrderList()
        }
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }

    getUserList(sTypingValue: string) {
        this.userNetService
            .getAllValidUsers({
                $limit: 10,
                $skip: 0,
                '$select[]': ['email', 'first_name', 'last_name', '_id', 'mobile'],
                'email[$like]': `%${sTypingValue}%`,
                _role_id: UserRole.TRADING_USER
            })
            .then((user: ReturnedCustomersRs) => {
                this.listOfCustomers = user.data.map((value) => {
                    value.name = value.last_name + ' ' + value.first_name
                    return value
                })
                // TODO: check whether it needs this folowwing line or not
                // this.changeDetectorRefs.detectChanges();
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => { })
    }

    prepareMatchedBUYRecordForm(): void {
        this.newMatchedBUYRecordForm = this.formBuilder.group({
            stockOrderId: ['', Validators.required],
            matchedDate: ['', Validators.required],
            matchedTime: ['', Validators.required],
            propertyReturnDate: ['', Validators.required],
            matchedPrice: [{ value: '', disabled: false }, Validators.required],
            matchedAmount: [{ value: '', disabled: false }, [Validators.required]],
            transactionFee: [{ value: 0, disabled: true }, Validators.required]
        })
    }
    prepareMatchedSELLRecordForm(): void {
        this.newMatchedSELLRecordForm = this.formBuilder.group({
            stockOrderId: ['', Validators.required],
            matchedDate: ['', Validators.required],
            matchedTime: ['', Validators.required],
            tax: ['', Validators.required], // Tax is for SELL order
            propertyReturnDate: [new Date(), Validators.required],
            matchedPrice: [{ value: '', disabled: false }, Validators.required],
            matchedAmount: [{ value: '', disabled: false }, [Validators.required]],
            transactionFee: [{ value: 0, disabled: true }, Validators.required]
        })
    }
    onOpeningNewMatchedRecordForm(index: number): void {
        this.stockOrderLists[index].isOpenMatchResultForm = true
    }
    onCompletingAStockOrder(i: number, item: ReturnedStockOrder): void {
        let spinnerRef = this.spinnerService.start()
        this.stockOrderService
            .changeStockOrderStatus({
                status: this.stockOrderStatus.DONE,
                _stock_order_id: item._id
            })
            .then((oRs: ReturnedStockOrder) => {
                this.stockOrderLists[i].status = oRs.status
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
        console.log('Completing a stock order')
    }
    // onCancellingNewMatchedRecordForm(index: number): void {
    //     this.stockOrderLists[index].isOpenMatchResultForm = false
    // }
    onClickAcceptOrder(index: number, item: ReturnedStockOrder): void {
        let spinnerRef = this.spinnerService.start()
        this.stockOrderService
            .changeStockOrderStatus({
                status: this.stockOrderStatus.APPROVED,
                _stock_order_id: item._id
            })
            .then((oRs: ReturnedStockOrder) => {
                this.stockOrderLists[index].status = oRs.status
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    onClickRejectOrder(index: number, item: ReturnedStockOrder): void {
        let spinnerRef = this.spinnerService.start()
        this.stockOrderService
            .changeStockOrderStatus({
                status: this.stockOrderStatus.REJECTED,
                _stock_order_id: item._id
            })
            .then((oRs: ReturnedStockOrder) => {
                this.stockOrderLists[index].status = oRs.status
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onClickCancelMatchResultForm(index: number) {
        this.stockOrderLists[index].isOpenMatchResultForm = false
        if (this.stockOrderLists[index].order_type === this.stockOrderType.BUY) {
            this.prepareMatchedBUYRecordForm()
        } else {
            this.prepareMatchedSELLRecordForm()
        }
        this.changeDetectorRefs.detectChanges()
    }

    onClickSaveMatchResultForm(index: number, item: ReturnedStockOrder) {
        switch (item.order_type) {
            case this.stockOrderType.BUY:
                this.onSaveMatchBuyResultForm(index, item)
                break
            case this.stockOrderType.SELL:
                this.onSaveMatchSellResultForm(index, item)
                break
            default:
                break
        }
    }

    onSaveMatchBuyResultForm(index: number, item: ReturnedStockOrder): void {
        let spinnerRef = this.spinnerService.start()
        let time = this.fB.matchedTime.value
        let matchedData: MatchedBuyRs = {
            match_date_time: this.fB.matchedDate.value.setHours(
                this.getHourFromString(time),
                this.getMinuteFromString(time)
            ),
            property_return_date: this.fB.propertyReturnDate.value.setHours(1, 1, 1),
            match_price: Number(this.fB.matchedPrice.value),
            match_amount: Number(this.fB.matchedAmount.value),
            _stock_order_id: Number(item._id)
            // transaction_cost: 0
        }
        this.stockOrderDetailService
            .addNewMatched(matchedData)
            .then((oRs) => {
                this.netHandlerService.handleSuccess(
                    `New match result ${matchedData.match_amount} was saved`,
                    'OK'
                )
                this.getMatchedHistory(item._id)
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.getStockOrderInfo(item._id)
                this.onClickCancelMatchResultForm(index)
                this.spinnerService.stop(spinnerRef)
            })
    }
    onSaveMatchSellResultForm(index: number, item: ReturnedStockOrder): void {
        let spinnerRef = this.spinnerService.start()
        let time = this.fS.matchedTime.value
        let matchedData: MatchSellRs = {
            match_date_time: this.fS.matchedDate.value.setHours(
                this.getHourFromString(time),
                this.getMinuteFromString(time)
            ),
            property_return_date: this.fS.propertyReturnDate.value.setHours(17, 0, 0),
            match_price: Number(this.fS.matchedPrice.value),
            match_amount: Number(this.fS.matchedAmount.value),
            _stock_order_id: Number(item._id)
        }
        this.stockOrderDetailService
            .addNewMatched(matchedData)
            .then((oRs) => {
                this.netHandlerService.handleSuccess(
                    `New match result ${matchedData.match_amount} was saved`,
                    'OK'
                )
                this.getMatchedHistory(item._id)
            })
            .catch((oError) => { this.netHandlerService.handleError(oError) })
            .finally(() => {
                this.onClickCancelMatchResultForm(index)
                this.getStockOrderInfo(item._id)
                this.spinnerService.stop(spinnerRef)
            })
    }

    getStockOrderInfo(_stock_order_id: number) {
        let query: {
            $limit: number,
            $skip: number,
            '$select[]': any,
            '$sort[created_at]': any,
            _id: any
        }
        query = {
            $limit: 1,
            $skip: 0,
            '$select[]': [
                '_id',
                '_user_id',
                'min_price',
                'max_price',
                'trading_date',
                'escrow_rca',
                'escrow_rate',
                'interest_rate',
                'amount',
                'trading_type',
                'order_type',
                'transaction_cost_rate',
                'tax_rate',
                'status'
            ],
            '$sort[created_at]': -1,
            _id: _stock_order_id
        }
        this.stockOrderService.getStockOrders(query).then((oRs: ReturnedStockOrderList) => {
            if (oRs.total === 1) {
                let index = findIndex(this.stockOrderLists, { _id: _stock_order_id })
                this.stockOrderLists[index].status = oRs.data[0].status
                this.changeDetectorRefs.detectChanges()
            }

        }).catch(error => {
            this.netHandlerService.handleError(error)
        }).finally(() => {

        })
    }

    getStockOrderList() {
        let spinnerRef = this.spinnerService.start()
        let query: {
            $limit: number,
            $skip: number,
            '$select[]': any,
            '$sort[created_at]': any,
            include: any,
            _user_id: any,
            status:any
        }
        query = {
            $limit: this.pageSize,
            $skip: this.skip,
            '$select[]': [
                '_id',
                '_user_id',
                'min_price',
                'max_price',
                'trading_date',
                'escrow_rca',
                'interest_rate',
                'amount',
                'trading_type',
                'order_type',
                'transaction_cost_rate',
                'tax_rate',
                'status'
            ],
            '$sort[created_at]': -1,
            include: 1,
            _user_id: {
                $nin: []
            },
            status: {
                $nin: [StockOrderStatus.INACTIVE]
            }
        }
        if (!!this.targetUser._id) {
            delete query._user_id
            query._user_id = {
                $in: [this.targetUser._id]
            }
        }
        this.stockOrderService
            .getStockOrders(query)
            .then((oRs: ReturnedStockOrderList) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((element) => {
                    element.isOpenMatchResultForm = false
                    element.trading_date = new Date(element.trading_date)
                    element.escrow_rca = Number(element.escrow_rca)
                    element.interest_rate = Number(element.interest_rate)
                    if (element.order_type === StockOrderType.BUY) {
                        element.property_return_date = new Date(
                            this.dayTimeHelperService.getExpectedPropertyReturnDayForBuy(
                                element.trading_date.getTime()
                            )
                        )
                    } else if (element.order_type === StockOrderType.SELL) {
                        element.property_return_date = new Date(
                            this.dayTimeHelperService.getExpectedPropertyReturnDayForSell(
                                element.trading_date.getTime()
                            )
                        )
                    }
                    element.stock.escrow_rate = Number(element.stock.escrow_rate)
                    element.min_price = Number(element.min_price)
                    element.max_price = Number(element.max_price)

                    return element
                })
                this.stockOrderLists = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    step = -1

    setStep(index: number) {
        this.step = index
        // this.stockOrderLists[index].matchedDetails =
        this.getMatchedHistory(this.stockOrderLists[index]._id)
    }

    getMatchedHistory(orderId: number) {
        const that = this
        let spinnerRef = this.spinnerService.start()
        this.stockOrderDetailService
            .getMatchedResults({
                '$select[]': [
                    '_id',
                    'match_date_time',
                    'property_return_date',
                    'match_price',
                    'match_amount'
                ],
                $limit: 1000,
                $skip: 0,
                _stock_order_id: orderId,
                '$sort[match_date_time]': -1
            })
            .then((oRs: ReturnedMatchedList) => {
                let index = findIndex(this.stockOrderLists, { _id: orderId })
                this.stockOrderLists[index].matchedDetails = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    nextStep() {
        this.step++
    }

    prevStep() {
        this.step--
    }

    onOpeningAnOrder(index: number, order: ReturnedStockOrder) {
        if (this.stockOrderLists[index].status !== StockOrderStatus.NEW) {
            this.setStep(index)
        }
        this.getSelectedOrderTransactionCostRate(
            this.stockOrderLists[index].trading_date.getTime()
        )
        this.getSelectedOrderTaxRate(this.stockOrderLists[index].trading_date.getTime())
        // if (!!order && order.order_type==StockOrderType.SELL){
        //     this.getSelectedOrderTaxRate(this.stockOrderLists[index].trading_date.getTime())
        // }
    }

    get fB() {
        return this.newMatchedBUYRecordForm.controls
    }

    get fS() {
        return this.newMatchedSELLRecordForm.controls
    }

    ngAfterViewInit() {
        this.getStockOrderList()
        // this.getBackgroundData()
    }

    ngOnDestroy() { }

    getBackgroundData() {
        // this.getTodayTax()
        // this.getTransactionCostRate()
    }

    getTodayTax(): void {
        this.todayTax = 0
    }

    getSelectedOrderTaxRate(datetime: number): void {
        this.sysConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_TRANSACTION_TAX_RATE,
                date: datetime
            })
            .then((oRs: number) => {
                this.selectedOrderTransactionTaxRate = oRs || 0
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                console.log(`Getting current Transaction Tax Rate`)
            })
    }

    getSelectedOrderTransactionCostRate(dateTime: number): void {
        this.sysConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_TRANSACTION_COST_RATE,
                date: dateTime
            })
            .then((oRs: number) => {
                this.selectedOrderTransactionCostRate = oRs || 0
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                console.log(`Getting current Transaction Cost Rate`)
            })
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getStockOrderList()
    }

    getHourFromString(s: string): number {
        const times = s.split(':')
        const hour = times[0]
        return isNaN(parseInt(hour)) ? 0 : parseInt(hour)
    }

    getMinuteFromString(s: string): number {
        const times = s.split(':')
        const minute = times[1]
        return isNaN(parseInt(minute)) ? 0 : parseInt(minute)
    }

    public getColor(input): string {
        if (input.status == this.stockOrderStatus.NEW) {
            return 'primary'
        }
        return 'accent'
    }

    public displayProperty(value) {
        if (value) {
            return value.email;
        }
    }

    public displayTotalMatchAmount(item: ReturnedStockOrder): number {
        if (!!item && !!item.matchedDetails && !isNaN(item.matchedDetails.length) && item.matchedDetails.length > 0) {
            let aMatched = item.matchedDetails
            let totalMatchAmount = aMatched.reduce((currentSum: number, element: any) => {
                return currentSum + Number(element.match_amount)
            }, 0)
            return totalMatchAmount

        }
        return 0
    }

    public displayAverageMatchPrice(item: ReturnedStockOrder): number {
        if (!!item && !!item.matchedDetails && !isNaN(item.matchedDetails.length) && item.matchedDetails.length > 0) {
            let aMatched = item.matchedDetails
            let totalMatchValue = aMatched.reduce((currentSum: number, element: any) => {
                return currentSum + Number(element.match_amount) * Number(element.match_price)
            }, 0)

            let totalMathAmount = aMatched.reduce((currentSum: number, element: any) => {
                return currentSum + Number(element.match_amount)
            }, 0)
            return Number((totalMatchValue / totalMathAmount).toFixed(3))
        }
        return 0
    }
}
