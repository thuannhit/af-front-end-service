import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import { ValidationService, SpinnerService, NetHandlerService } from '@/shared/_services/_local-services'
import { DepositNetServices } from '@/shared/_services/_net-services'

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'new-interest-dialog.component.html',
    styleUrls: ['new-interest-dialog.component.scss']
})
export class AFAddNewDepositInterestComponentDialog implements OnInit {
    newInterestFrom: FormGroup
    loading = false
    submitted = false
    constructor(
        public dialogRef: MatDialogRef<AFAddNewDepositInterestComponentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private depositNetServices: DepositNetServices,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService
    ) { }

    onNoClick(): void {
        this.dialogRef.close()
    }
    ngOnInit() {
        this.newInterestFrom = this.formBuilder.group({
            interest_rate: [
                '',
                [Validators.required, ValidationService.decimalValidation]
            ],
            period: ['', [Validators.required]],
            description: ['']
        })
    }
    get f() {
        return this.newInterestFrom.controls
    }
    onCloseCreationDialog() {
        this.dialogRef.close()
    }

    onSubmit() {
        // stop here if form is invalid
        if (this.newInterestFrom.invalid) {
            return
        }
        let spinnerRef = this.spinnerService.start()
        let oData = {
            interest_rate: this.f.interest_rate.value,
            period: this.f.period.value,
            description: this.f.description.value
        }
        this.depositNetServices
            .addNewDepositInterestConfig(oData)
            .then((oRs: any) => {
                this.onCloseCreationDialog()
                this.netHandlerService.handleSuccess('A new interest was added', 'OK')
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
