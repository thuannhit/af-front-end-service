import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
    NetHandlerService,
    UserLocalService,
    SpinnerService
} from '@/shared/_services/_local-services'
import { DepositNetServices } from '@/shared/_services/_net-services'
import { AFAddNewDepositInterestComponentDialog } from '@/modules/af/af-admin/components/deposit-config/new-interest-dialog'
import {
    DepositInterestConfigStatus
} from '@/shared/_common/CONSTANTS'
export interface ADepositInterest {
    _id: number
    period: number
    interest_rate: string
    checked?: boolean
}
export interface ReturnedDepositInterest {
    total: number
    limit: number
    skip: number
    data: ADepositInterest[]
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'interest-config.component.html',
    styleUrls: ['interest-config.component.scss']
})
export class AFDepositInterestConfigComponent implements OnInit, AfterViewInit {
    displayedColumns = ['checked', 'period', 'interest_rate', 'description']
    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    returnUrl: string
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    interestList: ADepositInterest[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private depositNetServices: DepositNetServices,
        private dialog: MatDialog,
        private userLocalSvc: UserLocalService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if not already logged in
        if (!this.userLocalSvc.currentUserValue) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    ngAfterViewInit() {
        this.getInterestList()
    }

    getInterestList(): void {
        let spinnerRef = this.spinnerService.start()
        this.depositNetServices
            .getCurrentInterestConfig({
                $limit: this.pageSize ? this.pageSize : 10,
                $skip: this.skip ? this.skip : 0,
                '$select[]': ['_id', 'period', 'interest_rate', 'description'],
                '$sort[period]': -1,
                status: DepositInterestConfigStatus.ACTIVE
            })
            .then((oRs: ReturnedDepositInterest) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((value) => {
                    value.checked = false
                    return value
                })
                this.interestList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onOpenAddInterestDialog(): void {
        const dialogRef = this.dialog.open(AFAddNewDepositInterestComponentDialog, {
            width: '40%'
        })

        dialogRef.afterClosed().subscribe((result) => {
            this.onRefreshInterestTable()
            console.log('The dialog was closed')
        })
    }

    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.interestList.every(
            (element) => element.checked == false
        )
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getInterestList()
    }

    // onRemoveInterestOptions(): void {
    // let aCheckedRows = this.tableInterestData.data.filter(
    //     (element) => element.checked == true
    // )
    // let oData = Object.assign([], aCheckedRows || []).map(
    //     (element) => element.id
    // )
    // this.icProductService.removeICInterestOptions(
    //     oData,
    //     (oResult) => {
    //         this.getICInterestList()
    //     },
    //     (oError) => {
    //         console.log(oError)
    //     }
    // )
    // }
    onUpdateInterestOptions(): void {
        let aCheckedRows = this.interestList.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                period: element.value,
                interest_rate: element.effective_date,
                description: element.description,
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.depositNetServices
        .updateCurrentInterestConfig(
            {
                period: oData[0].period,
                interest_rate: oData[0].interest_rate,
                description: oData[0].description,
                },
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getInterestList()
                    this.netHandlerService.handleSuccess(`Interest was updated`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshInterestTable(): void {
        this.getInterestList()
    }
}
