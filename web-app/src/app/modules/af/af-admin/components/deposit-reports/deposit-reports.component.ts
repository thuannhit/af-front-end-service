import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PageEvent } from '@angular/material/paginator'
import { invert } from 'lodash'
import {
    UserLocalService,
    SpinnerService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import {
    UserNetService,
    SystemConfigService,
    DepositNetServices
} from '@/shared/_services/_net-services'
import {
    DepositInterestPaymentMethod,
    DepositCustomerStatus,
    DepositInterestConfigStatus
} from '@/shared/_common/CONSTANTS'
const INFINITY_PROPERTY_RETURN_DATE = 4102419600000

export interface ReturnedCustomer {
    _id: number
    first_name?: string
    last_name?: string
    phone_number?: string,
    email?: string,
    bank_name?: string,
    bank_branch?: string,
    bank_account_number?: string,
    checked?: boolean,
    customer_code?: string
}

export interface ReturnedCustomers {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}
export interface ReturnedPeriod {
    _id?: number
    period: number
    interest_rate: number
}
export interface ReturnedPeriods {
    total: number
    limit: number
    skip: number
    data: ReturnedPeriod[]
}
export interface ReturnedDeposit {
    _id: number
    checked?: boolean
    customer: any
    deposit_value: number
    deposit_period: number
    deposit_start_date: number
    deposit_end_date: number
    interest_rate: number
    interest_rate_before_maturity: number
    interest_payment_method: number
    latest_interest_payment_date: number
}

export interface ReturnedDeposits {
    total: number
    data: ReturnedDeposit[]
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'deposit-reports.component.html',
    styleUrls: ['deposit-reports.component.scss']
})
export class AFDepositReportsComponent implements OnInit, AfterViewInit {
    newDepositForm: FormGroup
    returnUrl: string
    displayedColumns: string[] = ['checked', 'customer', 'deposit_value', 'deposit_period', 'deposit_start_date', 'deposit_end_date', 'interest_rate', 'interest_rate_before_maturity', 'interest_payment_method', 'latest_interest_payment_date']
    // MatPaginator Output
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    pageEvent: PageEvent
    isOpeningNewDepositCreationForm: boolean = false
    listOfDeposits: ReturnedDeposit[]
    listOfCustomers: ReturnedCustomer[]
    listOfPeriods: ReturnedPeriod[]
    interestPaymentMethod = DepositInterestPaymentMethod
    interestPaymentString = invert(DepositInterestPaymentMethod)
    listOfInterestPaymentMethod: { value: number, text: string }[] = [{
        value: this.interestPaymentMethod.UNDEFINED,
        text: this.interestPaymentString[this.interestPaymentMethod.UNDEFINED]
    },
    {
        value: this.interestPaymentMethod.COMPOUND_INTEREST,
        text: this.interestPaymentString[this.interestPaymentMethod.COMPOUND_INTEREST]
    },
    {
        value: this.interestPaymentMethod.EVERY_3_MONTHS,
        text: this.interestPaymentString[this.interestPaymentMethod.EVERY_3_MONTHS]
    },
    {
        value: this.interestPaymentMethod.IN_THE_EARLY_OF_MONTH,
        text: this.interestPaymentString[this.interestPaymentMethod.IN_THE_EARLY_OF_MONTH]
    },
    {
        value: this.interestPaymentMethod.IN_THE_EARLY_OF_QUATER,
        text: this.interestPaymentString[this.interestPaymentMethod.IN_THE_EARLY_OF_QUATER]
    },
    ]
    selectedInterestPaymentMethod: { value: number, text: string }
    selectedPeriod: ReturnedPeriod = {
        period: 0,
        interest_rate: 0
    }

    today: Date
    defaultSelectedStartDay: Date
    selectedEndDay: Date
    selectedStartDay: Date
    sTypingValue: string = ''
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private depositNetServices: DepositNetServices,
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        this.today = new Date()
        this.selectedStartDay = new Date(this.today.setHours(0, 0, 1))
        this.selectedEndDay = new Date(this.today.setHours(0, 0, 1))
    }

    prepareForm(): void {
        this.newDepositForm = this.formBuilder.group({
            customer: ['', Validators.required],
            period: ['', Validators.required],
            deposit_value: ['', Validators.required],
            deposit_interest_rate: ['', Validators.required],
            deposit_interest_payment_method: ['', Validators.required],
            deposit_start_date: ['', Validators.required],
            deposit_end_date: ['', Validators.required],
            interest_rate_before_maturity: ['', Validators.required],
            latest_interest_payment_date: ['', Validators.required],
        }, {
            validator: (formGroup: FormGroup) => {
                const deposit_value = formGroup.controls['deposit_value']
                const deposit_interest_rate = formGroup.controls['deposit_interest_rate']
                deposit_value.setErrors(null)
                deposit_interest_rate.setErrors(null)
                if (parseInt(deposit_interest_rate.value) && parseInt(deposit_interest_rate.value) < 0) {
                    deposit_interest_rate.setErrors({ notAPositiveNumber: true })
                }
                if (parseInt(deposit_value.value) && parseInt(deposit_value.value) < 0) {
                    deposit_value.setErrors({ notAPositiveNumber: true })
                }
                if (isNaN(parseInt(deposit_value.value)) && isNaN(parseInt(deposit_interest_rate.value))) {
                    deposit_value.setErrors({ required: true })
                    deposit_interest_rate.setErrors({ required: true })
                }
            }
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.newDepositForm.controls
    }

    ngAfterViewInit() {
        this.getCurrentCustomers()
        this.getCurrentPeriods()
        this.getCurrentDeposits()
    }

    ngOnDestroy() { }

    onClickNewDepositCreation(): void {
        this.isOpeningNewDepositCreationForm = true
    }

    onClickCancel(): void {
        this.isOpeningNewDepositCreationForm = false
    }

    onChangingPeriod(oEvent: ReturnedPeriod){
        // let currentSelectedStartDay = $.extend({}, this.selectedStartDay).getTime()
        let currentSelectedStartDay = this.selectedStartDay.getTime()
        this.selectedEndDay = new Date(new Date(currentSelectedStartDay).setMonth(new Date(currentSelectedStartDay).getMonth() + Number(oEvent.period)))
    }

    onSearchStock(s?: string): void {
        this.getCurrentDeposits()
    }

    onRefreshDepositsTable(): void {
        this.getCurrentDeposits()
    }

    getCurrentCustomers(): void {
        let spinnerRef = this.spinnerService.start()
        this.depositNetServices
            .getCurrentCustomers({
                $limit: 1000,
                $skip: 0,
                '$select[]': ['_id', 'customer_code', 'first_name', 'last_name', 'phone_number', 'email'],
                '$sort[created_at]': 1,
                status: DepositCustomerStatus.ACTIVE
                // customerSearch: this.sTypingValue || ''
            })
            .then((oRs: ReturnedCustomers) => {
                this.totalItems = oRs.total ? oRs.total : 0
                this.listOfCustomers = oRs.data.map((value) => {
                    value.checked = false
                    return value
                })
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    getCurrentPeriods(): void {
        let spinnerRef = this.spinnerService.start()
        this.depositNetServices
            .getCurrentInterestConfig({
                $limit: 100,
                $skip: 0,
                '$select[]': ['_id', 'period', 'interest_rate'],
                '$sort[period]': -1,
                status: DepositInterestConfigStatus.ACTIVE
            })
            .then((oRs: ReturnedPeriods) => {
                this.totalItems = oRs.total ? oRs.total : 0
                this.listOfPeriods = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    getCurrentDeposits(): void {
        let spinnerRef = this.spinnerService.start()
        this.depositNetServices
            .getCurrentDeposits({
                $limit: this.pageSize,
                $skip: this.skip,
            })
            .then((oRs: ReturnedDeposits) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data.map(element => {
                    return element
                })
                this.listOfDeposits = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getCurrentDeposits()
    }

    onSubmit(): void {
        let spinnerRef = this.spinnerService.start()
        if (this.newDepositForm.invalid) {
            this.spinnerService.stop(spinnerRef)
            return
        }
        let oInputData: {
            customer_id: number,
            deposit_period: number,
            deposit_value: number,
            interest_rate: number,
            interest_payment_method: number,
            deposit_start_date: number,
            deposit_end_date: number,
            interest_rate_before_maturity: number,
            latest_interest_payment_date: number,
        }
        oInputData = {
            customer_id: this.f.customer.value._id,
            deposit_period: this.f.period.value.period,
            deposit_value: this.f.deposit_value.value,
            interest_rate: this.f.deposit_interest_rate.value,
            interest_payment_method: this.f.deposit_interest_payment_method.value.value,
            deposit_start_date: this.f.deposit_start_date.value.getTime(),
            deposit_end_date: this.f.deposit_end_date.value.getTime(),
            interest_rate_before_maturity: this.f.interest_rate_before_maturity.value,
            latest_interest_payment_date: this.f.latest_interest_payment_date.value.getTime(),
        }
        this.depositNetServices
            .addNewDeposit(oInputData)
            .then((oRs: ReturnedDeposit) => {
                this.netHandlerService.handleSuccess(
                    `A New Deposit was created`,
                    'OK'
                )
                this.getCurrentDeposits()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })

    }
}
