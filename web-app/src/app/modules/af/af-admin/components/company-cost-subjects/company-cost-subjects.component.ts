import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PageEvent } from '@angular/material/paginator'
import { invert } from 'lodash'
import {
    UserLocalService,
    SpinnerService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import {
    UserNetService,
    SystemConfigService,
    DividendService
} from '@/shared/_services/_net-services'
import {
    StockStatus,
    DividendStatus,
} from '@/shared/_common/CONSTANTS'
const INFINITY_PROPERTY_RETURN_DATE = 4102419600000

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'company-cost-subjects.component.html',
    styleUrls: ['company-cost-subjects.component.scss']
})
export class AFCompanyCostSubjectsManagementComponent implements OnInit, AfterViewInit {
    newCostSubjectForm: FormGroup
    returnUrl: string
    displayedColumns: string[] = ['checked', 'stock', 'dividend_in_stock', 'dividend_in_cash', 'ex_dividend_date', 'status', 'actions']
    // MatPaginator Output
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    pageEvent: PageEvent
    isOpeningNewDividendCreationForm: boolean = false
    today: Date
    defaultSelectedDay: Date
    sTypingValue: string = ''
    displayScheduleStatus = invert(DividendStatus)
    dividendScheduleStatus = DividendStatus
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
    ) {
        // redirect to home if already logged in
        // if (!this.userLocalService.currentUser) {
        //     this.router.navigate(['/af/admin/login'])
        // }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        this.today = new Date()
        this.defaultSelectedDay = new Date(this.today.setHours(0, 0, 1))
    }

    prepareForm(): void {
        this.newCostSubjectForm = this.formBuilder.group({
            stock: ['', Validators.required],
            ex_dividend_date: ['', Validators.required],
            in_stock: [''],
            in_cash: ['']
        }, {
            validator: (formGroup: FormGroup) => {
                const instock = formGroup.controls['in_stock']
                const incash = formGroup.controls['in_cash']
                incash.setErrors(null)
                instock.setErrors(null)
                if (parseInt(instock.value) && parseInt(instock.value) < 0) {
                    instock.setErrors({ notAPositiveNumber: true })
                }
                if (parseInt(incash.value) && parseInt(incash.value) < 0) {
                    incash.setErrors({ notAPositiveNumber: true })
                }
                if (isNaN(parseInt(incash.value)) && isNaN(parseInt(instock.value))) {
                    incash.setErrors({ required: true })
                    instock.setErrors({ required: true })
                }
            }
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.newCostSubjectForm.controls
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() { }

    onClickNewDividendCreation(): void {
        this.isOpeningNewDividendCreationForm = true
    }

    onClickCancel(): void {
        this.isOpeningNewDividendCreationForm = false
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        // this.getCurrentDividendSchedules()
    }

    onSubmit(): void {
        if (this.newCostSubjectForm.invalid) {
            return
        }
        let oInputData: {
            _stock_id,
            ex_dividend_date,
            dividend_in_cash?,
            dividend_in_stock?
        }
        oInputData = {
            _stock_id: this.f.stock.value._id,
            ex_dividend_date: this.f.ex_dividend_date.value.setHours(23, 59, 59)
        }
        if (this.f.in_cash.value) {
            oInputData.dividend_in_cash = Number(this.f.in_cash.value)
        }
        if (this.f.in_stock.value) {
            oInputData.dividend_in_stock = Number(this.f.in_stock.value)
        }
        let spinnerRef = this.spinnerService.start()

    }
}
