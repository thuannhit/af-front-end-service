import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
    AbstractControl,
    FormControl
} from '@angular/forms'
import { AlertService } from '@/shared/_services_old'
import {
    SpinnerService,
    UserLocalService,
    ValidationService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import {
    SystemConfigService,
    UserNetService,
    StockOrderService
} from '@/shared/_services/_net-services'
import {
    StockStatus,
    StockOrderType,
    UserRole,
    StockOrderTradingType,
    FinancialServiceCMD,
    INTEREST_TYPE,
    COST_AND_TAX_FREE,
    InterestSettlementMethod
} from '@/shared/_common/'
import { threadId } from 'worker_threads'
import moment from 'moment'
import { DayTimeHelperSerivice } from '@/shared/_services/_local-services'
export interface ValidStockForBuying {
    code: string
    escrow_rate: number
}
export interface ReturnedCustomer {
    email: string
    first_name: string
    last_name: string
    _id: number
    name?: string
}
export interface ReturnedCustomersRs {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}
export interface ReturnedStock {
    _id: number
    code: string
    escrow_rate?: number
    total_quantity?: number
    valid_quantity?: number
    checked?: boolean
}
export interface ReturnedStocks {
    total: number
    limit: number
    skip: number
    data: ReturnedStock[]
}
export interface FinanceInfo {
    currentBalance: number
    validCashForBuying: number
}
export interface UserFinanceStatus {
    affordableWithdraw: number
    balance: number
}
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'stock-order-form',
    templateUrl: 'stock-order-form.component.html',
    styleUrls: ['stock-order-form.component.scss']
})
export class StockOrderFormComponent implements OnInit, AfterViewInit {
    stockOrderForm: FormGroup
    loading = false
    returnUrl: string
    listOfStocks: ReturnedStock[]
    listOfCustomers: ReturnedCustomer[]
    orderTypes = [
        { code: 1, value: 'Buy' },
        { code: 2, value: 'Sell' }
    ]
    currentFinanceInfo = {
        currentBalance: 0,
        validCashForBuying: 0
    }
    startDate: Date
    minValidOrderDate: Date
    maxValidOrderDate: Date
    today: Date
    defaultOrderDate: Date
    invalidPublishedDate = (d: Date): boolean => {
        const day = d.getDay()
        // Prevent Saturday and Sunday from being selected.
        return day !== 0 && day !== 6
    }
    targetCustomer: ReturnedCustomer = {
        _id: null,
        email: '',
        first_name: '',
        last_name: '',
        name: ''
    }
    orderType = StockOrderType
    currentApplyingInterest: number
    currentApplyingTax: number
    currentApplyingTransactionCost: number
    selectedStockAction: ReturnedStock = {
        _id: 0,
        code: '',
        escrow_rate: 0
    }
    typeOfInterest = INTEREST_TYPE
    interestSettlementMethod = InterestSettlementMethod
    defaultInterestType = this.typeOfInterest.FLOATING_INTEREST
    defaultInterestSettlementMethod = this.interestSettlementMethod.AT_THE_END_OF_MONTH
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private alertService: AlertService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private changeDetectorRefs: ChangeDetectorRef,
        private netHandlerService: NetHandlerService,
        private userNetService: UserNetService,
        private stockOrderService: StockOrderService,
        private dayTimeHelperService: DayTimeHelperSerivice
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareOrderForm()
    }

    prepareOrderForm(): void {
        this.stockOrderForm = this.formBuilder.group(
            {
                orderType: ['', Validators.required],
                tradingType: [StockOrderTradingType.LO, Validators.required],
                orderDate: [null, Validators.required],
                orderStock: [null, Validators.required],
                orderQuantity: [
                    null,
                    [Validators.required, ValidationService.numberValidation]
                ],
                orderNote: [null],
                minPriceAction: [null, Validators.required],
                maxPriceAction: [null, Validators.required],
                escrowRate: [{ value: 0, disabled: true }, Validators.required],
                escrowRCA: [{ value: 0, disabled: false }, Validators.required],
                minEscrowValue: [{ value: 0, disabled: false }, Validators.required],
                customer: [null, Validators.required],
                interest: [{ value: 0 }, Validators.required],
                interest_fixed: [
                    { value: this.typeOfInterest.FLOATING_INTEREST },
                    Validators.required
                ],
                interest_settlement_method: [
                    { value: this.interestSettlementMethod.AT_THE_END_OF_MONTH },
                    Validators.required
                ],
                transaction_cost_rate: [{ value: 0 }, Validators.required],
                tax_rate: [{ value: 0 }, Validators.required],
                period: [0, Validators.required],
                totalCost: [null, [Validators.required]]
            },
            {
                validator: (formGroup: FormGroup) => {
                    const orderType = formGroup.controls['orderType']
                    const minEscrowValue = formGroup.controls['minEscrowValue']

                    if (
                        minEscrowValue.errors &&
                        !minEscrowValue.errors.insufficentToWithdraw
                    ) {
                        // return if another validator has already found an error on the matchingControl
                        return
                    }

                    // set error on matchingControl if validation fails
                    if (orderType.value.code === StockOrderType.BUY) {
                        if (Number(minEscrowValue.value) >= 0 &&
                            !!this.currentFinanceInfo &&
                            Number(this.currentFinanceInfo.validCashForBuying) >= 0 &&
                            minEscrowValue.value < Number(this.currentFinanceInfo.validCashForBuying)) {
                            minEscrowValue.setErrors(null)
                        } else {
                            minEscrowValue.setErrors({ insufficentToWithdraw: true })
                        }
                    } else {
                        minEscrowValue.setErrors(null)
                    }
                }
            }
        )

        this.today = new Date()
        let currentTime = moment()

        // let a=moment(1585725260670).diff(moment(1585908000670), 'days')
        // debugger;
        let cutOffTime = moment('03:45 pm', 'HH:mm a')
        this.defaultOrderDate = this.today
        if (currentTime.isAfter(cutOffTime)) {
            this.defaultOrderDate = new Date(
                this.dayTimeHelperService.addBudinessDays(this.today.getTime(), 1)
            )
        }
        if (
            this.dayTimeHelperService.isSaturday(this.defaultOrderDate.getTime()) ||
            this.dayTimeHelperService.isSunday(this.defaultOrderDate.getTime())
        ) {
            this.defaultOrderDate = new Date(
                this.dayTimeHelperService.nextMonday(new Date().getTime())
            )
        }
        this.minValidOrderDate = new Date(this.today)
        this.maxValidOrderDate = new Date(this.today)
        this.minValidOrderDate.setDate(this.minValidOrderDate.getDate() - 365)
        this.maxValidOrderDate.setDate(this.maxValidOrderDate.getDate() + 30)
    }

    get f() {
        return this.stockOrderForm.controls
    }

    ngAfterViewInit() {
        this.getBackgroundData()

        // var spinnerRef = this.spinnerService.start();
        // setTimeout(() => {
        //     this.spinnerService.stop(spinnerRef);
        // }, 30000)
    }
    ngOnDestroy() { }

    getBackgroundData() {
        // this.getValidStocksForBuying()
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }

    getUserList(sTypingValue: string) {
        this.userNetService
            .getAllValidUsers({
                $limit: 10,
                $skip: 0,
                '$select[]': ['email', 'first_name', 'last_name', '_id', 'mobile'],
                'email[$like]': `%${sTypingValue}%`,
                _role_id: UserRole.TRADING_USER
            })
            .then((user: ReturnedCustomersRs) => {
                this.listOfCustomers = user.data.map((value) => {
                    value.name = value.last_name + ' ' + value.first_name
                    return value
                })
                // TODO: check whether it needs this folowwing line or not
                // this.changeDetectorRefs.detectChanges();
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => { })
    }
    getValidStocksForBuying() {
        let that = this
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getEscrowConfig({
                $limit: 1000,
                $skip: 0,
                '$select[]': ['_id', 'code', 'escrow_rate'],
                '$sort[created_at]': 1,
                status: StockStatus.ACTIVE
            })
            .then((oRs: any) => {
                that.listOfStocks = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    getValidStocksForSelling(user: ReturnedCustomer) {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_VALID_STOCK_FOR_SELLING,
                _user_id: user._id
            })
            .then((oRs: any) => {
                let aValidStock: ReturnedStock
                if (oRs && oRs.length && oRs.length > 0) {
                    oRs.map((element) => {
                        element.code = element.stock.code
                        delete element.stock
                        return element
                    })
                    this.listOfStocks = oRs
                }
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
                console.log(`Getting Valid stocks for selling`)
            })
    }

    onChangingOrderType(oEvent): void {
        //TODO: Change the way to generate orderTypes to make sure it is consistent with orderType
        // this.f.orderStock = new FormControl(null, [Validators.required]);
        this.getOndayTransactionCost(this.f.orderDate.value.getTime())
        if (oEvent.code === this.orderType.BUY) {
            this.getValidStocksForBuying()
            this.getOndayInterest(this.f.orderDate.value.getTime())
            if (this.f.customer.value !== null && this.f.customer.value !== '') {
                //TODO
                this.getFinanceInfoOfCurrentUser(this.targetCustomer)
            }
        } else {
            this.getOndayTaxRate(this.f.orderDate.value.getTime())
            if (this.f.customer.value !== null && this.f.customer.value !== '') {
                //TODO:
                this.getValidStocksForSelling(this.targetCustomer)
            }
        }
    }

    onChangingCustomer(customer: ReturnedCustomer): void {
        if (!!customer) {
            this.targetCustomer = customer
            if (this.f.orderType.value.code == StockOrderType.BUY) {
                //TODO:
                this.getFinanceInfoOfCurrentUser(customer)
            } else {
                if (this.f.orderType.value.code == StockOrderType.SELL) {
                    //TODO:
                    this.getValidStocksForSelling(customer)
                }
            }
        }
    }

    getOndayInterest(inputDay: number): void {
        this.sysConfigSvc
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_INTEREST,
                date: inputDay
            })
            .then((oRs: number) => {
                this.currentApplyingInterest = oRs || 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                console.log(`Getting current Interest`)
            })
    }
    getOndayTransactionCost(inputDay: number): void {
        this.sysConfigSvc
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_TRANSACTION_COST_RATE,
                date: inputDay
            })
            .then((oRs: number) => {
                this.currentApplyingTransactionCost = oRs || 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                console.log(`Getting current TransactionCost`)
            })
    }
    getOndayTaxRate(inputDay: number): void {
        this.sysConfigSvc
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_TRANSACTION_TAX_RATE,
                date: inputDay
            })
            .then((oRs: { value: number }) => {
                this.currentApplyingTax = oRs.value || 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                console.log(`Getting current TransactionCost`)
            })
    }
    getFinanceInfoOfCurrentUser(user: ReturnedCustomer) {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_AFFORDABLE_WITHDRAW,
                _user_id: user._id
            })
            .then((oRs: UserFinanceStatus) => {
                this.currentFinanceInfo = {
                    currentBalance: oRs.balance ? Number(oRs.balance.toFixed(0)) : 0,
                    validCashForBuying: oRs.affordableWithdraw
                        ? Number(oRs.affordableWithdraw.toFixed(0))
                        : 0
                }
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onSubmitTradingAction(): void {
        // reset alerts on submit
        this.alertService.clear()
        // stop here if form is invalid
        if (this.stockOrderForm.invalid) {
            return
        }
        let oOrderData: {
            trading_date: string
            trading_note: string
            trading_type: string
            order_type: number
            amount: string
            min_price: string
            max_price: string
            _user_id: string
            _stock_id: string
            escrow_rca?: number
            escrow_rate?: number
            interest_rate?: number
            interest_fixed?: number,
            interest_settlement_method?: number,
            transaction_cost_rate?: number,
            tax_rate?: number,
            period?: string
        }

        if (this.f.orderType.value.code === StockOrderType.BUY) {
            try {
                oOrderData = {
                    trading_date: this.f.orderDate.value.getTime(),
                    trading_note: this.f.orderNote.value,
                    trading_type: this.f.tradingType.value,
                    order_type: this.f.orderType.value.code,
                    amount: this.f.orderQuantity.value,
                    min_price: this.f.minPriceAction.value,
                    max_price: this.f.maxPriceAction.value,
                    _user_id: this.f.customer.value._id,
                    _stock_id: this.f.orderStock.value._id,
                    escrow_rca: Number(this.f.escrowRCA.value),
                    escrow_rate: Number(this.f.escrowRate.value),
                    interest_fixed: this.f.interest_fixed.value,
                    interest_rate: Number(this.f.interest.value),
                    interest_settlement_method: this.interestSettlementMethod.UN_DEFINED,
                    tax_rate: COST_AND_TAX_FREE.TAX_FREE,
                    transaction_cost_rate: Number(this.f.transaction_cost_rate.value),
                    period: this.f.period.value
                }
            } catch (error) {
                return
            }
        } else {
            //this.f.orderType.value.code === StockOrderType.SELL
            oOrderData = {
                trading_date: this.f.orderDate.value.getTime(),
                trading_note: this.f.orderNote.value,
                trading_type: this.f.tradingType.value,
                order_type: this.f.orderType.value.code,
                amount: this.f.orderQuantity.value,
                min_price: this.f.minPriceAction.value,
                max_price: this.f.maxPriceAction.value,
                _user_id: this.f.customer.value._id,
                _stock_id: this.f.orderStock.value._id,
                interest_settlement_method: Number(this.f.interest_settlement_method.value),
                tax_rate: Number(this.f.tax_rate.value),
                transaction_cost_rate: Number(this.f.transaction_cost_rate.value)
            }
        }
        var spinnerRef = this.spinnerService.start()
        this.stockOrderService
            .addNewOrder(oOrderData)
            .then((oRs: any) => {
                let type = oOrderData.order_type == StockOrderType.BUY ? 'Buy' : 'Sell'
                this.netHandlerService.handleSuccess(
                    `New ${type} Order ${oRs.amount} ${this.f.orderStock.value.code} created successfully`,
                    'OK'
                )
                this.prepareOrderForm()
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    radioInterestTypeChange() { }
    radioInterestInterestSettlementMethodChange() { }

    public displayProperty(value) {
        if (value) {
            return value.email
        }
    }

    public getMinEscrowValue() {
        if (this.f.orderQuantity) {
            //TODO: Check transaction cost also
            return (
                (Number(this.f.orderQuantity.value) *
                    Number(this.f.maxPriceAction.value) *
                    (Number(this.f.escrowRate.value) + Number(this.f.escrowRCA.value))) /
                100
            )
        }
        return 0
    }
}
