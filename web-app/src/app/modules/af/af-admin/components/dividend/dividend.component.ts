import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PageEvent } from '@angular/material/paginator'
import { invert } from 'lodash'
import {
    UserLocalService,
    SpinnerService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import {
    UserNetService,
    SystemConfigService,
    DividendService
} from '@/shared/_services/_net-services'
import {
    StockStatus,
    DividendStatus,
} from '@/shared/_common/CONSTANTS'
const INFINITY_PROPERTY_RETURN_DATE = 4102419600000
export interface ReturnedStock {
    _id?: number
    code: string
    escrow_rate?: number
    quantity?: number
    checked?: boolean
}

export interface ReturnedStocks {
    total: number
    limit: number
    skip: number
    data: ReturnedStock[]
}
export interface ReturnedSchedule {
    _id: number
    dividend_in_cash: number
    dividend_in_stock: number
    ex_dividend_date: number
    stock: ReturnedStock,
    status: number
}

export interface ReturnedSchedules {
    total: number
    limit: number
    skip: number
    data: ReturnedSchedule[]
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'dividend.component.html',
    styleUrls: ['dividend.component.scss']
})
export class AFDividendManagementComponent implements OnInit, AfterViewInit {
    newDividendForm: FormGroup
    returnUrl: string
    displayedColumns: string[] = ['checked', 'stock', 'dividend_in_stock', 'dividend_in_cash', 'ex_dividend_date', 'status', 'actions']
    // MatPaginator Output
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    pageEvent: PageEvent
    isOpeningNewDividendCreationForm: boolean = false
    listOfStocks: ReturnedStock[]
    listOfDividendSchedules: ReturnedSchedule[]
    today: Date
    defaultSelectedDay: Date
    sTypingValue: string = ''
    displayScheduleStatus = invert(DividendStatus)
    dividendScheduleStatus = DividendStatus
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private dividendService: DividendService,
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        this.today = new Date()
        this.defaultSelectedDay = new Date(this.today.setHours(0, 0, 1))
    }

    prepareForm(): void {
        this.newDividendForm = this.formBuilder.group({
            stock: ['', Validators.required],
            ex_dividend_date: ['', Validators.required],
            in_stock: [''],
            in_cash: ['']
        }, {
            validator: (formGroup: FormGroup) => {
                const instock = formGroup.controls['in_stock']
                const incash = formGroup.controls['in_cash']
                incash.setErrors(null)
                instock.setErrors(null)
                if (parseInt(instock.value) && parseInt(instock.value) < 0) {
                    instock.setErrors({ notAPositiveNumber: true })
                }
                if (parseInt(incash.value) && parseInt(incash.value) < 0) {
                    incash.setErrors({ notAPositiveNumber: true })
                }
                if (isNaN(parseInt(incash.value)) && isNaN(parseInt(instock.value))) {
                    incash.setErrors({ required: true })
                    instock.setErrors({ required: true })
                }
            }
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.newDividendForm.controls
    }

    ngAfterViewInit() {
        this.getValidStocks()
        this.getCurrentDividendSchedules()
    }

    ngOnDestroy() { }

    onClickNewDividendCreation(): void {
        this.isOpeningNewDividendCreationForm = true
    }

    onClickCancel(): void {
        this.isOpeningNewDividendCreationForm = false
    }

    getValidStocks(): void {
        let that = this
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getEscrowConfig({
                $limit: 1000,
                $skip: 0,
                '$select[]': ['_id', 'code', 'escrow_rate'],
                '$sort[created_at]': 1,
                status: StockStatus.ACTIVE
            })
            .then((oRs: ReturnedStocks) => {
                that.listOfStocks = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onSearchStock(s?: string): void {
        this.getCurrentDividendSchedules()
    }

    onRefreshDividendScheduleTable(): void {
        this.getCurrentDividendSchedules()
    }

    getCurrentDividendSchedules(): void {
        let spinnerRef = this.spinnerService.start()
        this.dividendService
            .getDividendSchedules({
                $limit: this.pageSize,
                $skip: this.skip,
                '$select[]': ['_id', 'dividend_in_stock', 'dividend_in_cash', 'ex_dividend_date', 'status'],
                '$sort[ex_dividend_date]': 1,
                status: {
                    $ne: DividendStatus.DONE
                },
                include: 1,
                stockSearch: this.sTypingValue || ''
            })
            .then((oRs: ReturnedSchedules) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data.map(element => {
                    element.dividend_in_stock = Number(element.dividend_in_stock)
                    return element
                })
                this.listOfDividendSchedules = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getCurrentDividendSchedules()
    }

    onSubmit(): void {
        if (this.newDividendForm.invalid) {
            return
        }
        let oInputData: {
            _stock_id,
            ex_dividend_date,
            dividend_in_cash?,
            dividend_in_stock?
        }
        oInputData = {
            _stock_id: this.f.stock.value._id,
            ex_dividend_date: this.f.ex_dividend_date.value.setHours(23, 59, 59)
        }
        if (this.f.in_cash.value) {
            oInputData.dividend_in_cash = Number(this.f.in_cash.value)
        }
        if (this.f.in_stock.value) {
            oInputData.dividend_in_stock = Number(this.f.in_stock.value)
        }
        let spinnerRef = this.spinnerService.start()
        this.dividendService
            .addNewSchedule(oInputData)
            .then((oRs: ReturnedSchedule) => {
                this.netHandlerService.handleSuccess(
                    `A New Schedule was created`,
                    'OK'
                )
                this.getCurrentDividendSchedules()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })

    }

    onClickApplySchedule(items): void {
        let spinnerRef = this.spinnerService.start()
        this.dividendService
            .applyASchedule({ status: this.dividendScheduleStatus.APPLY, property_return_date: INFINITY_PROPERTY_RETURN_DATE},
                items._id)
            .then((oRs: ReturnedSchedule) => {
                this.netHandlerService.handleSuccess(
                    `A New Schedule was apply`,
                    'OK'
                )
                this.getCurrentDividendSchedules()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onClickDoneSchedule(items): void {
        let spinnerRef = this.spinnerService.start()
        this.dividendService
            .doneASchedule({ status: this.dividendScheduleStatus.DONE, property_return_date: new Date().getTime()},
                items._id)
            .then((oRs: ReturnedSchedule) => {
                this.netHandlerService.handleSuccess(
                    `A New Schedule was apply`,
                    'OK'
                )
                this.getCurrentDividendSchedules()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
