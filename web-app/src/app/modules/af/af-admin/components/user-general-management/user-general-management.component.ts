import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms'
import { PageEvent } from '@angular/material/paginator'
import {
    SpinnerService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import { UserLocalService, MessageDialogService } from '@/shared/_services/_local-services'
import {
    UserNetService,
    SystemConfigService
} from '@/shared/_services/_net-services'
import { MessageType, GeneralStatus } from '@/shared/_common/CONSTANTS'
export interface Bank {
    name: string,
    _id: number
}
export interface ReturnedUser {
    email: string
    first_name: string
    last_name: string
    _id: number
    name?: string
    mobile?: string
    isVerified?: number,
    status?: string
    address?: string,
    _role_id?: number
}
export interface ReturnedUsersRs {
    total: number
    limit: number
    skip: number
    data: ReturnedUser[]
}
export interface FinanceInfo {
    cashInAccount: number
    validCashForBuying: number
}
export interface ReturnedBankingData {
    total: number
    limit: number
    skip: number
    data: Bank[]
}
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'user-general-management.component.html',
    styleUrls: ['user-general-management.component.scss']
})
export class AFUserGeneralManagement implements OnInit, AfterViewInit {
    newUserCreationForm: FormGroup
    returnUrl: string
    listOfUsers: ReturnedUser[]
    // stateCtrl = new FormControl();
    isOpeningNewUserCreationForm: boolean = false
    displayedColumns: string[] = [
        'checked',
        'user_name',
        'email',
        'mobile',
        'status',
        'role'
    ]
    bankList: Bank[]
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private messageDialogService: MessageDialogService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
    }

    prepareForm(): void {
        this.newUserCreationForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            PIN: ['', Validators.required],
            PIN_issue_location: ['', Validators.required],
            PIN_issue_date: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            address: ['', Validators.required],
            mobile: ['', Validators.required],
            bank_branch: ['', Validators.required],
            bankaccount_number: ['', Validators.required],
            _bank_id: ['', Validators.required],
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.newUserCreationForm.controls
    }

    ngAfterViewInit() {
        this.getBackgroundData()

        // let spinnerRef = this.spinnerService.start();
        // setTimeout(() => {
        //     this.spinnerService.stop(spinnerRef);
        // }, 30000)
    }
    ngOnDestroy() { }

    onClickNewUserCreation(): void {
        this.isOpeningNewUserCreationForm = true
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }
    getBackgroundData() {
        this.getUserList()
        this.getValidBanks()
    }
    onSearchUser(s?: string) {
        this.getUserList(s || "")
    }
    getUserList(sTypingValue?: string) {
        this.userNetService
            .getAllValidUsers({
                $limit: this.pageSize ? this.pageSize : 10,
                $skip: this.skip ? this.skip : 0,
                '$select[]': [
                    'email',
                    'first_name',
                    'last_name',
                    '_id',
                    'mobile',
                    'isVerified',
                    'address',
                    'status',
                    '_role_id'
                ],
                'email[$like]': `%${sTypingValue || ''}%`,
            })
            .then((user: ReturnedUsersRs) => {
                this.totalItems = user.total ? user.total : 0
                this.listOfUsers = user.data.map((value) => {
                    value.name = value.last_name + ' ' + value.first_name
                    return value
                })
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => { })
    }
    getValidBanks(): void {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getBanks({
                $limit: 1000,
                $skip: 0,
                '$select[]': ['_id', 'name'],
                'status': GeneralStatus.ACTIVE,
            })
            .then((oRs: ReturnedBankingData) => {
                this.bankList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            }).finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    onSubmit(): void {
        // stop here if form is invalid
        if (this.newUserCreationForm.invalid) {
            return
        }
        let registerInfo = {
            first_name: this.f.firstName.value,
            last_name: this.f.lastName.value,
            gender: 0,
            email: this.f.email.value,
            mobile: this.f.mobile.value,
            password: 'Toor1234',
            address: this.f.address.value,
            _bank_id: this.f._bank_id.value._id,
            PIN: this.f.PIN.value,
            PIN_issue_date: this.f.PIN_issue_date.value.getTime(),
            PIN_issue_location: this.f.PIN_issue_location.value,
            bankaccount_number: this.f.bankaccount_number.value,
            bank_branch: this.f.bank_branch.value,
        }
        this.userNetService
            .adminCreateUser(registerInfo)
            .then((oRs) => {
                let messConfig = {
                    type: MessageType.INFORMATION,
                    title: 'A new user has been created successfully',
                    message: 'It is not required to verify email this time',
                    confirmText: 'OK',
                    OKAction: () => {
                        this.prepareForm();
                        this.isOpeningNewUserCreationForm = false
                        this.onRefreshUserTable()
                    },
                }
                this.messageDialogService.openDialog(messConfig)
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
            })
    }

    public getRoleString(role: number) {
        switch (role) {
            case 1:
                return "Trading user"
            case 2:
                return "Môi giới"
            case 3: 
                return "Admin"
            case 4:
                return "Super admin"
            default:
                return "Not defined yet"
        }
    }

    public getStatusString(status: number) {
        if (status === 1) {
            return "Active"
        } else if (status === 2) {
            return "Not active"
        }
        return "Not defined yet"
    }

    onClickCancel(): void {
        this.prepareForm();
        this.isOpeningNewUserCreationForm = false
    }

    onRefreshUserTable(): void {
        this.getUserList()
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.onRefreshUserTable()
    }
}
