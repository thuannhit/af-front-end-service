import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
    SpinnerService,
    ValidationService,
    NetHandlerService,
    MessageDialogService
} from '@/shared/_services/_local-services'
import { UserLocalService } from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'
import { AFAddNewStockComponent } from '@/modules/af/af-admin/components/escrow-config/new-stock-dialog'
import { StockStatus, MessageType } from '@/shared/_common'
import { DialogOption, Actions } from '@/shared/_models'
export interface ReturnedStock {
    _id: number
    code: string
    escrow_rate: number
    checked?: boolean
}
export interface ReturnedStocks {
    total: number
    limit: number
    skip: number
    data: ReturnedStock[]
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    // selector: 'escrow-component',
    templateUrl: 'escrow-config.component.html',
    styleUrls: ['escrow-config.component.scss']
})
export class AFEscrowConfigComponent implements OnInit, AfterViewInit {
    returnUrl: string
    listOfStocks: ReturnedStock[]
    isSomeRowBeingChecked: boolean = false
    displayedColumns = ['checked', '_id', 'code', 'escrow_rate']
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    pageEvent: PageEvent
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private sysConfigSvc: SystemConfigService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private dialog: MatDialog,
        private messDialogService: MessageDialogService
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    ngAfterViewInit() {
        this.getBackgroundData()
    }

    ngOnDestroy() { }

    getBackgroundData() {
        this.getCurrentEscrowConfig()
    }

    getCurrentEscrowConfig() {
        let that = this
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getEscrowConfig({
                $limit: this.pageSize,
                $skip: this.skip,
                '$select[]': ['_id', 'code', 'escrow_rate'],
                '$sort[created_at]': 1,
                status: StockStatus.ACTIVE
            })
            .then((oRs: any) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((value) => {
                    value.checked = false
                    value.escrow_rate = parseInt(value.escrow_rate)
                    return value
                })
                that.listOfStocks = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onOpenAddStockDialog(): void {
        const dialogRef = this.dialog.open(AFAddNewStockComponent, {
            width: '40%'
        })

        dialogRef.afterClosed().subscribe((result) => {
            this.getCurrentEscrowConfig()
            console.log('The dialog was closed')
        })
    }

    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.listOfStocks.every(
            (element) => element.checked == false
        )
    }

    onRemoveInterestOptions(): void {
        let aCheckedRows = this.listOfStocks.filter(
            (element) => element.checked == true
        )
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                code: element.code,
                escrow_rate: element.escrow_rate
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .removeEscrowConfig(oData[0]._id)
            .then(
                (oResult) => {
                    this.getCurrentEscrowConfig()
                    this.netHandlerService.handleSuccess('Stock was removed', 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onUpdateInterestOptions(): void {
        let aCheckedRows: ReturnedStock[] = this.listOfStocks.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let action: Actions[] = [
            {
                actionFn: () => {
                    console.log('Apply to new and ongoing loans')
                    //TODO:
                },
                actionText: 'Apply to All'
            },
            {
                actionFn: () => {
                    console.log('Apply to new loans only')
                    this.updateInterest(aCheckedRows)
                },
                actionText: 'Apply to New'
            }
        ]
        let option: DialogOption = {
            type: MessageType.ACTIONS,
            title: 'Confirmation',
            message:
                'Would you like to apply for all ongoing loans or just up-coming loans?',
            Actions: action
        }
        this.messDialogService.openDialog(option)
    }

    updateInterest(aCheckedRows: ReturnedStock[]): void {
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                code: element.code,
                escrow_rate: element.escrow_rate
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .updateEscrowConfig(
                { escrow_rate: oData[0].escrow_rate, code: oData[0].code },
                oData[0]._id
            )
            .then(
                (oResult) => {
                    this.getCurrentEscrowConfig()
                    this.netHandlerService.handleSuccess('Stock was updated', 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    onRefreshEscrowTable(): void {
        this.getCurrentEscrowConfig()
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getCurrentEscrowConfig()
    }
}
