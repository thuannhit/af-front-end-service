import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Inject
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms'
import {
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import {
    SpinnerService,
    ValidationService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import { UserLocalService } from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'

export interface ReturnedStock {
    _id: number
    code: string
    escrow_rate: number
}
export interface ReturnedStocks {
    total: number
    limit: number
    skip: number
    data: ReturnedStock[]
}

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'new-stock-dialog.component.html',
    styleUrls: ['new-stock-dialog.component.scss']
})
export class AFAddNewStockComponent implements OnInit, AfterViewInit {
    newStockForm: FormGroup
    returnUrl: string
    constructor(
        public dialogRef: MatDialogRef<AFAddNewStockComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private sysConfigSvc: SystemConfigService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private formBuilder: FormBuilder
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'

        this.newStockForm = this.formBuilder.group({
            stock: ['', [Validators.required, ValidationService.alphaNumValidation]],
            escrow_rate: [
                '',
                [Validators.required, ValidationService.numberValidation]
            ]
        })
    }

    ngAfterViewInit() { }
    ngOnDestroy() { }
    get f() {
        return this.newStockForm.controls
    }

    onCloseCreationDialog() {
        this.dialogRef.close()
    }
    onSubmit(): void {
        // stop here if form is invalid
        if (this.newStockForm.invalid) {
            return
        }
        let spinnerRef = this.spinnerService.start()
        let oData = {
            code: this.f.stock.value,
            escrow_rate: parseInt(this.f.escrow_rate.value)
        }
        this.sysConfigSvc
            .addNewStock(oData)
            .then((oRs: any) => {
                this.onCloseCreationDialog()
                this.netHandlerService.handleSuccess('A new Stock was added', 'OK')
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
