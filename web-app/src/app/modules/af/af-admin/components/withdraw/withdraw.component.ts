import { Component, OnInit, AfterViewInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { SpinnerService, ValidationService, NetHandlerService } from '@/shared/_services/_local-services';
import { UserLocalService } from '@/shared/_services/_local-services';
import { FinanceService, UserNetService, SystemConfigService } from '@/shared/_services/_net-services';
import { FinancialStatementType, FinancialServiceCMD, UserRole, UserStatus} from '@/shared/_common/CONSTANTS';

export interface UserFinanceStatus {
    affordableWithdraw: number,
    balance: number
}

export interface ReturnedCustomer {
    email: string,
    first_name: string,
    last_name: string,
    _id: number,
    name?: string
}
export interface ReturnedCustomersRs {
    total: number,
    limit: number,
    skip: number,
    data: ReturnedCustomer[]
}
export interface FinanceInfo {
    cashInAccount: number,
    validCashForBuying: number
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'withdraw-form',
    templateUrl: 'withdraw.component.html',
    styleUrls: ['withdraw.component.scss']
})
export class WithdrawFormComponent implements OnInit, AfterViewInit {
    withdrawCashForm: FormGroup;
    returnUrl: string;
    listOfCustomers: ReturnedCustomer[]
    // stateCtrl = new FormControl();
    filteredCustomers: Observable<ReturnedCustomer[]>;
    currentFinanceInfo = {
        currentBalance: 0,
        validCashForBuying: 0
    }
    selectedCustomer: {}
    startDate: Date;
    minPublishedDate: Date;
    maxPublishedDate: Date;
    invalidTopupDate = (d: Date): boolean => {
        const day = d.getDay();
        // Prevent Saturday and Sunday from being selected.
        return day !== 0;
    };
    targetUser: ReturnedCustomer = {
        _id: null,
        email: '',
        first_name: '',
        last_name: '',
        name: '',
    };
    today = new Date()
    defaultWithdrawTime: string
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private financeService: FinanceService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private systemConfigService: SystemConfigService
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login']);
        }

    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.defaultWithdrawTime = "09:00"
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.prepareForm();
        const today = new Date();
        this.minPublishedDate = new Date(today);
        this.maxPublishedDate = new Date(today);
        this.minPublishedDate.setDate(this.minPublishedDate.getDate() - 1000);
        this.maxPublishedDate.setDate(this.maxPublishedDate.getDate() + 30);
    }

    prepareForm(): void {
        this.withdrawCashForm = this.formBuilder.group({
            customer: ['', Validators.required],
            amount: ['', Validators.required],
            date: [this.today, Validators.required],
            time: [''],
            transactionRef: ['', [Validators.required]],
        }, {
            validator: (formGroup: FormGroup) => {
                const amount = formGroup.controls['amount']

                if (amount.errors && !amount.errors.notEnoughForBuying) {
                    // return if another validator has already found an error on the matchingControl
                    return
                }

                // set error on matchingControl if validation fails
                // if (
                //     Number(amount.value) &&
                //     !!this.currentFinanceInfo &&
                //     !!this.currentFinanceInfo.validCashForBuying &&
                //     amount.value >= this.currentFinanceInfo.validCashForBuying
                // ) {
                //     console.log('Insufficient to withdraw')
                //     amount.setErrors({ notEnoughForWithdrawing: true })
                // } else {
                //     amount.setErrors(null)
                // }
                amount.setErrors(null)
            }
        });
        this.changeDetectorRefs.detectChanges();
    }

    get f() { return this.withdrawCashForm.controls; }

    ngAfterViewInit() {
        this.getBackgroundData();
    }
    ngOnDestroy() { }

    onChangingCustomer(customer: ReturnedCustomer): void {
        if (!!customer) {
            this.getFinanceInfoOfCurrentUser(customer);
            this.targetUser = customer;
        }
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }
    getBackgroundData() {
        // this.listOfCustomers = this.getUserList();
    }

    getUserList(sTypingValue: string) {

        this.userNetService.getAllValidUsers({
            "$limit": 10,
            "$skip": 0,
            "$select[]": ["email", "first_name", "last_name", '_id', 'mobile'],
            "email[$like]": `%${sTypingValue}%`,
            status: UserStatus.ACTIVE,
            _role_id: UserRole.TRADING_USER,
        }).then((user: ReturnedCustomersRs) => {
            this.listOfCustomers = user.data.map((value) => {
                value.name = value.last_name + ' ' + value.first_name
                return value;
            })

        }).catch(error => {
            this.netHandlerService.handleError(error)
        }).finally(() => {
        })
    }

    onSubmit(): void {
        // stop here if form is invalid
        if (this.withdrawCashForm.invalid) {
            return;
        }
        let time = this.f.time.value;
        let oData = {
            date: this.f.date.value.setHours(this.getHourFromString(time), this.getMinuteFromString(time)),
            amount: parseInt(this.f.amount.value),
            type: FinancialStatementType.DOWN,
            _user_id: this.f.customer.value._id,
            note: this.f.transactionRef.value,
        }
        let that = this;
        let spinnerRef = this.spinnerService.start()
        this.financeService.withdraw(oData).then((oRs: any) => {
            that.netHandlerService.handleSuccess('Withdraw success', 'OK');
            that.prepareForm();
        }).catch(error => {
            this.netHandlerService.handleError(error)
        }).finally(() => {
            this.spinnerService.stop(spinnerRef)
        });
    }

    getHourFromString(s: string): number {
        const times = s.split(':')
        const hour = times[0]
        return isNaN(parseInt(hour)) ? 0 : parseInt(hour)
    }

    getMinuteFromString(s: string): number {
        const times = s.split(':')
        const minute = times[1]
        return isNaN(parseInt(minute)) ? 0 : parseInt(minute)
    }

    onTesting() {
        // this.f.
        // console.log('Testing')
        // this.messageDialogService.openDialog({
        //     title: 'CONFIRM.DOWNLOAD.JOB.TITLE',
        //     message: 'CONFIRM.DOWNLOAD.JOB.MESSAGE',
        //     cancelText: 'CONFIRM.DOWNLOAD.JOB.CANCELTEXT',
        //     confirmText: 'CONFIRM.DOWNLOAD.JOB.CONFIRMTEXT'
        // })
    }

    getFinanceInfoOfCurrentUser(user: ReturnedCustomer) {
        let spinnerRef = this.spinnerService.start()
        this.systemConfigService.getInfoFromTradingSVC({
            cmd: FinancialServiceCMD.GET_AFFORDABLE_WITHDRAW, _user_id: user._id, to_date: !!(this.f.date.value) ? this.f.date.value.getTime() : this.today.getTime()
        })
            .then((oRs: UserFinanceStatus) => {
                this.currentFinanceInfo = {
                    currentBalance: oRs.balance ? Number(oRs.balance.toFixed(0)) : 0,
                    validCashForBuying: oRs.affordableWithdraw ? Number(oRs.affordableWithdraw.toFixed(0)) : 0
                }
            }).catch(error => {
                this.netHandlerService.handleError(error)
            }).finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    public displayProperty(value) {
        if (value) {
            return value.email;
        }
    }
}
