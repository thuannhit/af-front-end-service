import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Inject
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms'
import {
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import {
    SpinnerService,
    ValidationService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import { UserLocalService } from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'

export interface ReturnedStock {
    _id: number
    code: string
    escrow_rate: number
}
export interface ReturnedStocks {
    total: number
    limit: number
    skip: number
    data: ReturnedStock[]
}

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'new-bank-dialog.component.html',
    styleUrls: ['new-bank-dialog.component.scss']
})
export class AFAddNewBankComponentDialog implements OnInit, AfterViewInit {
    newBankForm: FormGroup
    returnUrl: string
    constructor(
        public dialogRef: MatDialogRef<AFAddNewBankComponentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private sysConfigSvc: SystemConfigService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private formBuilder: FormBuilder
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'

        this.newBankForm = this.formBuilder.group({
            name: ['', [Validators.required]],
        })
    }

    ngAfterViewInit() { }
    ngOnDestroy() { }
    get f() {
        return this.newBankForm.controls
    }

    onCloseCreationDialog() {
        this.dialogRef.close()
    }
    onSubmit(): void {
        // stop here if form is invalid
        if (this.newBankForm.invalid) {
            return
        }
        let spinnerRef = this.spinnerService.start()
        let oData = {
            name: this.f.name.value,
        }
        this.sysConfigSvc
            .addNewBank(oData)
            .then((oRs: any) => {
                this.onCloseCreationDialog()
                this.netHandlerService.handleSuccess(`A new Bank ${oRs.name} was added`, 'OK')
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
