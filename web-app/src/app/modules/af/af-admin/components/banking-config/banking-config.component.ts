import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
    NetHandlerService,
    UserLocalService,
    SpinnerService
} from '@/shared/_services/_local-services'
import {
    SystemConfigService
} from '@/shared/_services/_net-services'
import { GeneralStatus } from '@/shared/_common'
import { AFAddNewBankComponentDialog } from '@/modules/af/af-admin/components/banking-config/new-bank-dialog'
export interface ABank {
    _id: number
    name: string
    created_at: Date
    checked?: boolean
}
export interface ReturnedBankingData {
    total: number
    limit: number
    skip: number
    data: ABank[]
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'banking-config.component.html',
    styleUrls: ['banking-config.component.scss']
})
export class AFBankingConfigComponent implements OnInit, AfterViewInit {
    displayedColumns = ['checked', 'id', 'name', 'created_at']
    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    returnUrl: string
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    bankList: ABank[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sysConfigSvc: SystemConfigService,
        private dialog: MatDialog,
        private userLocalSvc: UserLocalService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if not already logged in
        if (!this.userLocalSvc.currentUserValue) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    ngAfterViewInit() {
        this.getBankList()
    }

    getBankList(): void {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getBanks({
                $limit: this.pageSize,
                $skip: this.skip,
                '$select[]': ['_id', 'name', 'created_at'],
                'status': GeneralStatus.ACTIVE,
            })
            .then((oRs: ReturnedBankingData) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((value) => {
                    value.checked = false
                    value.created_at = new Date(value.created_at)
                    return value
                })
                this.bankList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            }).finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onOpenAddNewBankDialog(): void {
        const dialogRef = this.dialog.open(AFAddNewBankComponentDialog, {
            width: '40%'
        })

        dialogRef.afterClosed().subscribe((result) => {
            this.getBankList()
            console.log('The dialog was closed')
        })
    }


    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.bankList.every(
            (element) => element.checked == false
        )
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getBankList()
    }

    onRemoveBankItem(): void {
        let aCheckedRows = this.bankList.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length>1){
            alert("Sorry, deleting multi items is not supported yet")
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                name: element.name,
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .deleteBankItem(
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getBankList()
                    this.netHandlerService.handleSuccess(`Bank ${oResult.name} was deleted`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    onUpdateBankItem(): void {
        let aCheckedRows = this.bankList.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                name: element.name,
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .updateBankConfig(
                { name: oData[0].name },
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getBankList()
                    this.netHandlerService.handleSuccess(`Bank ${oResult.name} was updated`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshBankTable(): void {
        this.getBankList()
    }
}
