import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {
    SpinnerService,
    NetHandlerService,
    UserLocalService
} from '@/shared/_services/_local-services'
import { ReportServices, SystemConfigService } from '@/shared/_services/_net-services'
import { ReportServiceCMD, FinancialServiceCMD } from '@/shared/_common/CONSTANTS'
import { ReturnedCustomersRs } from '../stock-order-form'
export interface ReturnedStockLoan {
    code: string

    total_amount: number
    total_transaction_cost: number
    total_transaction_value: number
    _stock_id: number
}
export interface ReturnedStockLoans {
    total: number
    limit: number
    skip: number
    data: ReturnedStockLoan[]
}
export interface ReturnedAlert {
    _id: number
    analysed_time: number
    original_escrow_rate: number
    current_escrow_rate: number
    profit: number
    net_assets: number
    loan_amount: number
    min_escrows_value: number
    current_escrows_value: number
    agreement_escrow_rate: number
    user: {
        email: string
        first_name: string
        last_name: string
        _id: number
    }
    _user_id?: number
}
export interface ReturnedAlerts {
    total: number
    limit: number
    skip: number
    data: ReturnedAlert[]
}
export interface FinanceInfo {
    cashInAccount: number
    validCashForBuying: number
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'escrow-report.component.html',
    styleUrls: ['escrow-report.component.scss']
})
export class AFEscrowReportComponent implements OnInit, AfterViewInit {
    generalForm: FormGroup
    defaultDate: Date
    maxValidInterestDate: Date
    today: Date
    returnUrl: string
    displayedStocksTableColumns = [
        'stock',
        'total_quantity',
        'total_stock_value',
    ]
    displayedColumns = [
        'user_id',
        // 'analysed_time',
        'agreement_escrow_rate',
        'original_escrow_rate',
        'current_escrow_rate',
        'profit',
        'net_assets',
        'loan_amount',
        'needed_amount',
        'details'
    ]
    stockTablePageSize: number = 10
    totalStocksTableItems: number = 0
    stocksTableSkip: number = 0
    pageStockTableEvent: PageEvent
    stocksLoanList: any


    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    alertList: ReturnedAlert[]
    filteredTypeOfUser: 'NONE'
    currentInterestValueForOnGoingLoans: number
    currentInterestValueForDoneLoans: number
    latestAnalyzedTime: Date
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private spinnerService: SpinnerService,
        private reportService: ReportServices,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private systemConfigService: SystemConfigService
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.currentInterestValueForOnGoingLoans = 0
        this.currentInterestValueForDoneLoans = 0
        this.prepareForm()
        this.today = new Date()
        this.maxValidInterestDate = new Date(this.today)
        this.defaultDate = new Date(this.today)
    }

    prepareForm(): void {
        this.generalForm = this.formBuilder.group({
            interestDate: ['', Validators.required],
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.generalForm.controls
    }

    ngAfterViewInit() { }
    ngOnDestroy() { }

    onInterestDateChange() {
        this.getInterestValueOfUserForOnGoingLoans()
        this.getInterestValueOfUserForDoneLoans()
    }

    getInterestValueOfUserForOnGoingLoans() {
        // let spinnerRef = this.spinnerService.start()
        let to_date = this.f.interestDate.value.getTime()
        this.systemConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_ALL_USERS_INTEREST_VALUE_FOR_ONGOING_LOANS,
                to_date: to_date
            })
            .then((oRs: any) => {
                this.currentInterestValueForOnGoingLoans = oRs.value
                    ? Number(oRs.value.toFixed(0))
                    : 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }
    getInterestValueOfUserForDoneLoans() {
        // let spinnerRef = this.spinnerService.start()
        let to_date = this.f.interestDate.value.getTime()
        this.systemConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_ALL_USERS_INTEREST_VALUE_FOR_DONE_LOANS,
                to_date: to_date
            })
            .then((oRs: any) => {
                this.currentInterestValueForDoneLoans = oRs.value
                    ? Number(oRs.value.toFixed(0))
                    : 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }
    onClickAnalyzeBtn(): void {
        let spinnerRef = this.spinnerService.start()
        this.reportService
            .analyzeEscrowsForAllCustomer({
                cmd: ReportServiceCMD.ANALYZE_ESCROWS_FOR_ALL
            })
            .then((oRs: any) => {
                this.netHandlerService.handleSuccess(
                    'Analyze finished successfully',
                    'OK'
                )
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshAlertTable(): void {
        if (this.filteredTypeOfUser == undefined) {
            this.filteredTypeOfUser = 'NONE'
        }
        let spinnerRef = this.spinnerService.start()
        this.reportService
            .getAlerts({
                params: {
                    $limit: this.pageSize ? this.pageSize : 10,
                    $skip: this.skip ? this.skip : 0,
                    columns: [
                        '_id',
                        'analysed_time',
                        'original_escrow_rate',
                        'current_escrow_rate',
                        'profit',
                        'net_assets',
                        '_user_id',
                        'loan_amount',
                        'min_escrows_value',
                        'current_escrows_value',
                        'agreement_escrow_rate',
                    ],
                    '$sort[analysed_time]': -1,
                    type: this.filteredTypeOfUser
                }
            })
            .then((oRs: ReturnedAlerts) => {
                this.totalItems = oRs.total ? oRs.total : 0
                this.alertList = oRs.data
                if (oRs.data.length > 0) {
                    this.latestAnalyzedTime = new Date(oRs.data[0].analysed_time)
                }
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshStocksLoanTable(): void {
        let spinnerRef = this.spinnerService.start()
        this.reportService
            .getStocksLoan({
                cmd: ReportServiceCMD.GET_STOCKS_LOANS_REPORT,
                limit: this.stockTablePageSize ? this.stockTablePageSize : 10,
                offset: this.stocksTableSkip ? this.stocksTableSkip : 0
            })
            .then((oRs: ReturnedStockLoans) => {
                this.totalStocksTableItems = oRs.total ? oRs.total : 0
                this.stocksLoanList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    redirectToDetails(element: ReturnedAlert) {
        this.router.navigate(['/af/admin/reports/loans-report'], { queryParams: { _user_id: element._user_id } });
    }

    radioFilterChange(): void {
        this.onRefreshAlertTable()
    }
    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.onRefreshAlertTable()
    }
    public onStocksTableChange(oParams) {
        this.stockTablePageSize = oParams.pageSize
        this.stocksTableSkip = oParams.pageIndex * this.stockTablePageSize
        this.onRefreshStocksLoanTable()
    }

    public getNeededAmount(element) {
        if (element.current_escrow_rate < element.original_escrow_rate) {
            return element.min_escrows_value -
                element.current_escrows_value
        }
        return 0
    }
    public getTotalLoansOfAStock(element: ReturnedStockLoan) {

        return element.total_transaction_cost + element.total_transaction_value
    }
}
