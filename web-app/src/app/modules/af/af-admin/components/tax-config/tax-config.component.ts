import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
    NetHandlerService,
    UserLocalService,
    SpinnerService
} from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'
import { PreferenceType } from '@/shared/_common'
import { AFAddNewTaxComponentDialog } from '@/modules/af/af-admin/components/tax-config/new-tax-dialog'
export interface APreferenceStatement {
    _id: number
    effective_date: Date
    key: string
    value: string
    checked?: boolean
}
export interface ReturnedPreferenceStatement {
    total: number
    limit: number
    skip: number
    data: APreferenceStatement[]
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'tax-config.component.html',
    styleUrls: ['tax-config.component.scss']
})
export class AFTransactionTaxConfigComponent implements OnInit, AfterViewInit {
    displayedColumns = ['checked', 'tax', 'effective_date', 'description']
    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    returnUrl: string
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    taxList: APreferenceStatement[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sysConfigSvc: SystemConfigService,
        private dialog: MatDialog,
        private userLocalSvc: UserLocalService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if not already logged in
        if (!this.userLocalSvc.currentUserValue) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    ngAfterViewInit() {
        this.getTaxList()
    }

    getTaxList(): void {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getTaxs({
                $limit: this.pageSize ? this.pageSize : 10,
                $skip: this.skip ? this.skip : 0,
                '$select[]': ['_id', 'value', 'effective_date', 'description'],
                type: PreferenceType.TRANSACTION_TAX.toString(),
                '$sort[effective_date]': -1,
            })
            .then((oRs: ReturnedPreferenceStatement) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((value) => {
                    value.checked = false
                    value.effective_date = new Date(value.effective_date)
                    return value
                })
                this.taxList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onOpenAddTaxDialog(): void {
        const dialogRef = this.dialog.open(AFAddNewTaxComponentDialog, {
            width: '40%'
        })

        dialogRef.afterClosed().subscribe((result) => {
            this.onRefreshTaxTable()
            console.log('The dialog was closed')
        })
    }

    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.taxList.every(
            (element) => element.checked == false
        )
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getTaxList()
    }

    onUpdateTaxOptions(): void {
        let aCheckedRows = this.taxList.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                value: element.value,
                effective_date: element.effective_date,
                description: element.description,
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
        .updateTaxConfig(
            {
                effective_date: new Date(oData[0].effective_date).getTime(),
                value: oData[0].value,
                description: oData[0].description,
                type: PreferenceType.TRANSACTION_TAX.toString()
                },
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getTaxList()
                    this.netHandlerService.handleSuccess(`Tax was updated`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshTaxTable(): void {
        this.getTaxList()
    }
}
