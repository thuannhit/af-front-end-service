import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
    AbstractControl
} from '@angular/forms'
import {
    SpinnerService,
    ValidationService,
    NetHandlerService
} from '@/shared/_services/_local-services'
import { UserLocalService } from '@/shared/_services/_local-services'
import {
    FinanceService,
    UserNetService
} from '@/shared/_services/_net-services'
import { FinancialStatementType, UserRole, UserStatus } from '@/shared/_common/CONSTANTS'

export interface ReturnedCustomer {
    email: string
    first_name: string
    last_name: string
    _id: number
    name?: string
}
export interface ReturnedCustomersRs {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}
export interface FinanceInfo {
    cashInAccount: number
    validCashForBuying: number
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'top-up-form',
    templateUrl: 'top-up.component.html',
    styleUrls: ['top-up.component.scss']
})
export class TopupFormComponent implements OnInit, AfterViewInit {
    topupCashForm: FormGroup
    returnUrl: string
    listOfCustomers: ReturnedCustomer[]
    currentFinanceInfo = {
        cashInAccount: 0,
        validCashForBuying: 0
    }
    startDate: Date
    minPublishedDate: Date
    maxPublishedDate: Date
    invalidTopupDate = (d: Date): boolean => {
        const day = d.getDay()
        // Prevent Saturday and Sunday from being selected.
        return day !== 0
    }
    today = new Date()
    targetUser: ReturnedCustomer = {
        _id: null,
        email: '',
        first_name: '',
        last_name: '',
        name: ''
    }
    defaultTopupTime: string
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private financeService: FinanceService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        this.defaultTopupTime = '09:00'
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        const today = new Date()
        this.minPublishedDate = new Date(today)
        this.maxPublishedDate = new Date(today)
        this.minPublishedDate.setDate(this.minPublishedDate.getDate() - 1000)
        this.maxPublishedDate.setDate(this.maxPublishedDate.getDate() + 30)
    }

    prepareForm(): void {
        this.topupCashForm = this.formBuilder.group({
            customer: ['', Validators.required],
            amount: ['', Validators.required],
            date: ['', Validators.required],
            time: [''],
            transactionRef: ['', [Validators.required]]
            // transactionRef: [
            //     '',
            //     [Validators.required, ValidationService.alphaNumValidation]
            // ]
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.topupCashForm.controls
    }

    ngAfterViewInit() {
        this.getBackgroundData()

        // var spinnerRef = this.spinnerService.start();
        // setTimeout(() => {
        //     this.spinnerService.stop(spinnerRef);
        // }, 30000)
    }
    ngOnDestroy() { }

    onChangingCustomer(customer: ReturnedCustomer): void {
        if (!!customer) {
            this.currentFinanceInfo = this.getFinanceInfoOfCurrentCustomer(
                this.f.customer.value
            )
            this.targetUser = customer
        }
    }

    getFinanceInfoOfCurrentCustomer(user_email: object): FinanceInfo {
        // TODO: Implement this API
        return {
            cashInAccount: 1000000,
            validCashForBuying: 90000000000
        }
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }
    getBackgroundData() {
        // this.listOfCustomers = this.getUserList();
    }

    getUserList(sTypingValue: string) {
        this.userNetService
            .getAllValidUsers({
                $limit: 10,
                $skip: 0,
                '$select[]': ['email', 'first_name', 'last_name', '_id', 'mobile'],
                'email[$like]': `%${sTypingValue}%`,
                status: UserStatus.ACTIVE,
                _role_id: UserRole.TRADING_USER
            })
            .then((user: ReturnedCustomersRs) => {
                this.listOfCustomers = user.data.map((value) => {
                    value.name = value.last_name + ' ' + value.first_name
                    return value
                })
                // TODO: check whether it needs this folowwing line or not
                // this.changeDetectorRefs.detectChanges();
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => { })
    }

    onSubmit(): void {
        // stop here if form is invalid
        if (this.topupCashForm.invalid) {
            return
        }
        let time = this.f.time.value
        let oData = {
            date: this.f.date.value.setHours(
                this.getHourFromString(time),
                this.getMinuteFromString(time)
            ),
            amount: parseInt(this.f.amount.value),
            type: FinancialStatementType.UP,
            _user_id: this.f.customer.value._id,
            note: this.f.transactionRef.value
        }
        let that = this
        let spinnerRef = this.spinnerService.start()
        this.financeService
            .topup(oData)
            .then((oRs: any) => {
                that.netHandlerService.handleSuccess('Topup success', 'OK')
                that.prepareForm()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    getHourFromString(s: string): number {
        const times = s.split(':')
        const hour = times[0]
        return isNaN(parseInt(hour)) ? 0 : parseInt(hour)
    }

    getMinuteFromString(s: string): number {
        const times = s.split(':')
        const minute = times[1]
        return isNaN(parseInt(minute)) ? 0 : parseInt(minute)
    }

    onTesting() {
        // this.f.
        // console.log('Testing')
        // this.messageDialogService.openDialog({
        //     title: 'CONFIRM.DOWNLOAD.JOB.TITLE',
        //     message: 'CONFIRM.DOWNLOAD.JOB.MESSAGE',
        //     cancelText: 'CONFIRM.DOWNLOAD.JOB.CANCELTEXT',
        //     confirmText: 'CONFIRM.DOWNLOAD.JOB.CONFIRMTEXT'
        // })
    }

    public displayProperty(value) {
        if (value) {
            return value.email
        }
    }
}
