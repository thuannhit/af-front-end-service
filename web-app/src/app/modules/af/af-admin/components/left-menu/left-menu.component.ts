import { Component, OnInit } from '@angular/core'
import { onSideNavChange, animateText } from './animations/animations'
import { SidenavService } from '@/shared/_services/_local-services/sidenav.service'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core';
interface Page {
    link: string
    name: string
    icon: string
}
import {
    UserLocalService,
} from '@/shared/_services/_local-services'
import { GeneralUser } from '@/shared/_models'
import { UserRole } from '@/shared/_common';

@Component({
    selector: 'af-left-menu',
    templateUrl: './left-menu.component.html',
    styleUrls: ['./left-menu.component.scss'],
    animations: [onSideNavChange, animateText]
})
export class AFLeftMenuComponent implements OnInit {
    public sideNavState: boolean = false
    public linkText: boolean = false
    currentUser: GeneralUser
    public pages: Page[] = [
        { name: 'Alerts', link: 'af/admin/alert', icon: 'notification_important' },
        {
            name: "{{'USER_MANAGEMENT'}}",
            link: 'af/admin/user-management',
            icon: 'supervised_user_circle'
        },
        {
            name: 'Stock trading management',
            link: 'af/admin/stock-order-management',
            icon: 'add_shopping_cart'
        },
        {
            name: 'Cash management',
            link: 'af/admin/cash-management',
            icon: 'money'
        },
        { name: 'Reports', link: 'af/admin/report', icon: 'bubble_chart' }
    ]

    constructor(
        private _sidenavService: SidenavService,
        private router: Router,
        private translate: TranslateService,
        private userLocalService: UserLocalService
    ) { }
    config = {
        // highlightOnSelect: true,
        // collapseOnSelect: true,
        interfaceWithRoute: true
    }
    appitems = []
    currentUserName: string
    ngOnInit() {
        let that = this
        this.userLocalService.currentUser.subscribe(x => {
            this.currentUser = x;
        });
        if (this.userLocalService.currentUserValue && this.userLocalService.currentUserValue.user && this.userLocalService.currentUserValue.user.email) {
            this.currentUserName = this.userLocalService.currentUserValue.user.email || ''

        } else {
            this.currentUserName = ''
        }
        // debugger
        // this.translate.stream('REPORTS').subscribe(
        //     value => {
        //         console.log(value)
        //     }
        // )
        this.appitems = [
            {
                label: 'Reports',
                icon: 'report_off',
                activeIcon: 'report',
                items: [{
                    label: 'Assets report',
                    icon: 'library_books',
                    link: 'af/admin/reports/loans-report'
                }, {
                    label: 'Esrow alert',
                    icon: 'warning',
                    link: 'af/admin/reports/escrows-report'
                }],
                hidden: (this.currentUser.user._role_id === UserRole.SUPER_USER || this.currentUser.user._role_id === UserRole.ADVANCED_ADMIN) ? false : true
                // link: 'af/admin/report',
            },
            {
                label: 'System config',
                icon: 'settings',
                hidden: (this.currentUser.user._role_id === UserRole.SUPER_USER || this.currentUser.user._role_id === UserRole.ADVANCED_ADMIN) ? false : true,
                items: [
                    {
                        label: 'Banking',
                        link: '/af/admin/system-configuration/banking',
                        icon: 'account_balance',
                        activeIcon: 'account_balance'
                    },
                    {
                        label: 'Interests',
                        link: 'af/admin/system-configuration/interest',
                        icon: 'show_chart'
                    },
                    {
                        label: 'Transaction cost',
                        link: 'af/admin/system-configuration/trans-cost',
                        imageIcon: '/assets/images/transaction.jpg'
                    },
                    {
                        label: 'Escrow',
                        link: 'af/admin/system-configuration/escrow',
                        icon: 'money_off',
                        activeIcon: 'attach_money'
                    },
                    {
                        label: 'Tax',
                        link: 'af/admin/system-configuration/tax',
                        imageIcon: '/assets/images/tax-icon.png',
                    },
                    {
                        label: 'Dividend Action',
                        link: 'af/admin/system-configuration/dividend',
                        icon: 'redeem',
                        activeIcon: 'attach_money'
                    },
                    {
                        label: 'Interests',
                        // link: 'af/admin/interest-management',
                        icon: 'favorite_border',
                        activeIcon: 'favorite',
                        navigationExtras: {
                            queryParams: { order: 'popular', filter: 'new' }
                        },
                        hidden: true
                    }
                ]
            },
            {
                label: 'User management',
                icon: 'supervised_user_circle',
                link: 'af/admin/user-management',
                hidden: (this.currentUser.user._role_id === UserRole.SUPER_USER || this.currentUser.user._role_id === UserRole.ADVANCED_ADMIN) ? false : true
            },
            {
                label: 'Stock trading management',
                link: 'af/admin/stock-order-management',
                icon: 'add_shopping_cart'
            },
            {
                label: 'Cash management',
                link: 'af/admin/cash-management',
                icon: 'money'
            },
            {
                label: 'Stock trading management',
                link: 'af/admin/stock-order-management',
                icon: 'star_rate',
                hidden: true
            },
            {
                label: 'Company cost',
                icon: 'polymer',
                items: [{
                    label: 'Cost topic',
                    icon: 'library_books',
                    link: 'af/admin/company-cost/company-cost-subjects'
                }, {
                    label: 'Cost report',
                    icon: 'warning',
                    link: 'af/admin/company-cost/cost-report'
                }],
                hidden: (this.currentUser.user.email != 'thuannh0206@gmail.com') ? true : false
            },
            {
                label: 'Deposits management',
                icon: 'business_center',
                items: [{
                    label: 'Deposite config',
                    icon: 'confirmation_number',
                    link: 'af/admin/deposit/deposit-configs'
                }, {
                    label: 'Deposit cusomers',
                    icon: 'supervised_user_circle',
                    link: 'af/admin/deposit/deposit-customers'
                }, {
                    label: 'Deposit reports',
                    icon: 'warning',
                    link: 'af/admin/deposit/deposit-reports'
                }],
                hidden: (this.currentUser.user._role_id != UserRole.SUPER_USER) ? true : false
            }
        ]
    }

    onSinenavToggle() {
        this.sideNavState = !this.sideNavState

        setTimeout(() => {
            this.linkText = this.sideNavState
        }, 200)
        this._sidenavService.sideNavState$.next(this.sideNavState)
    }

    onClickMenu(url: string) {
        this.router.navigate([url])
    }

    selectedItem(tests) {
        return ''
    }
}
