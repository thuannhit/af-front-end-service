import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Inject
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import { StockOrderDetailService } from '@/shared/_services/_net-services'
import {
    UserLocalService,
    NetHandlerService,
    SpinnerService
} from '@/shared/_services/_local-services'

export interface DialogData {
    _stock_order_id: number
}

// export interface ReturnedAMatched {
//     _id?: number
//     match_date_time: number
//     match_price: number
//     match_amount: number
//     property_return_date: number
// }
// export interface ReturnedMatchedList {
//     total: number
//     limit: number
//     skip: number
//     data: ReturnedAMatched[]
// }

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'match-history-dialog.component.html',
    styleUrls: ['match-history-dialog.component.scss']
})
export class AFShowMatchHistoryDialog implements OnInit, AfterViewInit {
    _stock_order_id: number
    // matchedRSList: ReturnedAMatched[]
    // displayedColumns: string[] = [
    //     'matchedTime',
    //     'matchedPrice',
    //     'matchedAmount',
    //     'propertyReturnDate'
    // ]
    constructor(
        public dialogRef: MatDialogRef<AFShowMatchHistoryDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private router: Router,
        private userLocalService: UserLocalService,
        private changeDetectorRefs: ChangeDetectorRef,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private stockOrderDetailService: StockOrderDetailService,
    ) {
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
        this._stock_order_id = data._stock_order_id
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        // this.getMatchedHistory()
    }
    ngOnDestroy() { }

    getMatchedHistory() {
        // debugger;
        // let spinnerRef = this.spinnerService.start()
        // this.stockOrderDetailService
        //     .getMatchedResults({
        //         '$select[]': [
        //             '_id',
        //             'match_date_time',
        //             'property_return_date',
        //             'match_price',
        //             'match_amount',
        //         ],
        //         $limit: 1000,
        //         $skip: 0,
        //         _stock_order_id: this._stock_order_id,
        //         '$sort[match_date_time]': -1
        //     })
        //     .then((oRs: ReturnedMatchedList) => {
        //         console.log('GOT IT', oRs.data)
        //         this.matchedRSList = oRs.data
        //         this.changeDetectorRefs.detectChanges()
        //     })
        //     .catch((oError) => {
        //         this.netHandlerService.handleError(oError)
        //     })
        //     .finally(() => {
        //         this.spinnerService.stop(spinnerRef)
        //     })
    }

    onCloseDialog() {
        this.dialogRef.close()
        this.changeDetectorRefs.detectChanges()
    }
}
