import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import {
    SpinnerService,
    NetHandlerService,
    UserLocalService,
    ReportHelperLocalService,
    MessageDialogService,
    DayTimeHelperSerivice
} from '@/shared/_services/_local-services'
import {
    FinanceService,
    UserNetService,
    ReportServices,
    SystemConfigService,
} from '@/shared/_services/_net-services'
import {
    FinancialStatementType,
    ReportServiceCMD,
    FinancialServiceCMD,
    MessageType,
    UserRole,
    UserStatus
} from '@/shared/_common/CONSTANTS'
import { DialogOption, Actions } from '@/shared/_models'
import { AFShowMatchHistoryDialog } from '@/modules/af/af-admin/components/loans-report/match-history-dialog/'

export interface ReturnedAlert {
    _id: number
    analysed_time?: number
    original_escrow_rate?: number
    current_escrow_rate?: number
    profit?: number
    net_assets?: number
    loan_amount?: number
    min_escrows_value?: number
    current_escrows_value?: number
    agreement_escrow_rate?: number
}
export interface ReturnedAlerts {
    total: number
    limit: number
    skip: number
    data: ReturnedAlert[]
}
export interface UserFinanceStatus {
    affordableWithdraw: number
    balance: number
}
export interface AStock {
    _id: number
    code: string
    currentPrice?: number
    valid_quantity?: number
}
export interface ReturnedCustomer {
    email: string
    first_name: string
    last_name: string
    _id: number
    name?: string
}
export interface ReturnedCustomersRs {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}
export interface FinanceInfo {
    cashInAccount: number
    validCashForBuying: number
}
export interface AnOnGoingLoan {
    _id: number
    price: number
    quantity: number
    transaction_value: number
    transaction_cost: number
    start_date: number
    property_return_date: number
    _stock_id: number
    stock: {
        code: string
    }
    stock_order: {
        _id?: number
        escrow_rate: number
        escrow_rca: number,
        interest_rate?: number
    }
    interest: number,
    estimated_profit?: number
}
export interface ADoneLoan {
    sell_price: number
    buy_price: number
    quantity: number
    buy_cost: number
    buy_value: number
    start_date: number
    end_date: number
    sell_order_id: number
    buy_order_id: number
    code: string
    interest: number
    interest_settlement_status: number
}

const NumberOfDaysForStockReturnDate: number = 3
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'loans-report.component.html',
    styleUrls: ['loans-report.component.scss']
})
export class AFLoansReportComponent implements OnInit, AfterViewInit {
    loansForm: FormGroup
    returnUrl: string
    listOfCustomers: ReturnedCustomer[]
    targetUser: ReturnedCustomer = {
        _id: null,
        email: '',
        first_name: '',
        last_name: '',
        name: ''
    }
    displayedColumns = [
        'stock',
        'quantity',
        'price',
        'current_price',
        'start_date',
        'min_required_reserve_rate',
        'current_esrcrow_rate',
        'escrow_value',
        'loans_value',
        'interest',
        'estimated_profit'
    ]
    displayedColumnsOfEndedLoans = [
        'stock',
        'quantity',
        'buy_price',
        'sell_price',
        'start_date',
        'end_date',
        'loans_value',
        'not_settlement_interest'
    ]
    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    onGoingLoansList: AnOnGoingLoan[]
    doneLoansList: ADoneLoan[]
    listOfStocks: AStock[]
    maxValidInterestDate: Date
    today: Date
    defaultDate: Date
    currentFinanceInfo = {
        currentBalance: 0,
        validCashForBuying: 0,
    }
    currentInterestValueForOnGoingLoans = 0
    currentInterestValueForDoneLoans = 0
    isOnGoingLoansSettlement = true
    isDoneLoansSettlement = true
    role = UserRole
    currentEscrowRate: number = 0
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private loansService: ReportServices,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private reportHelperLocalService: ReportHelperLocalService,
        private dayTimeHelperService: DayTimeHelperSerivice,
        private systemConfigService: SystemConfigService,
        private messageDialogService: MessageDialogService,
        private dialog: MatDialog
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        this.today = new Date()
        this.maxValidInterestDate = new Date(this.today)
        this.defaultDate = new Date(this.today)
    }

    prepareForm(): void {
        this.loansForm = this.formBuilder.group({
            customer: ['', Validators.required],
            interestDate: ['', Validators.required],
            is_ongoing_loans_settlement: [{ value: true }, Validators.required],
            is_done_loans_settlement: [{ value: true }, Validators.required]
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.loansForm.controls
    }

    ngAfterViewInit() {
        let _user_id = this.route.snapshot.queryParams['_user_id'] || null
        if (_user_id !== null) {
            this.userNetService
                .getAllValidUsers({
                    $limit: 1,
                    $skip: 0,
                    '$select[]': ['email', 'first_name', 'last_name', '_id', 'mobile'],
                    _id: _user_id,
                    status: UserStatus.ACTIVE,
                    _role_id: this.role.TRADING_USER
                })
                .then((user: ReturnedCustomersRs) => {
                    if (user.data.length > 0) {
                        this.targetUser =
                            user.data.map((value) => {
                                value.name = value.last_name + ' ' + value.first_name
                                return value
                            })[0]
                        this.f.customer.setValue(this.targetUser)
                        this.onRefreshPage()
                    }
                    this.changeDetectorRefs.detectChanges()
                })
                .catch((error) => {
                    this.netHandlerService.handleError(error)
                })
                .finally(() => { })
        }
    }
    ngOnDestroy() { }

    onRefreshPage() {
        if (!!this.targetUser && !!this.targetUser._id && this.targetUser._id !== null) {
            this.onChangingCustomer(this.targetUser)
        }
    }

    async onChangingCustomer(customer: ReturnedCustomer) {
        if (!!customer) {
            this.targetUser = customer
            this.getFinanceInfoOfCurrentUser(customer)
            this.getInterestValueOfUserForOnGoingLoans(this.targetUser)
            this.getInterestValueOfUserForDoneLoans(this.targetUser)
            this.listOfStocks = await this.getValidStockOfUser(customer)
            this.getCurrentPriceForStocks()
            this.getLoansOfUser(customer)
            this.getEndedLoansButNotSettlementInterestOfUser(customer)
            this.getEscrowAllInformation(customer)
        }
    }

    onInterestDateChange() {
        if (this.targetUser && this.targetUser._id !== null) {
            this.getLoansOfUser(this.targetUser)
            this.getInterestValueOfUserForOnGoingLoans(this.targetUser)
            this.getInterestValueOfUserForDoneLoans(this.targetUser)
        }
    }
    async getValidStockOfUser(customer: ReturnedCustomer): Promise<AStock[]> {
        return new Promise(async (resolve, reject) => {
            this.systemConfigService
                .getInfoFromTradingSVC({
                    cmd: FinancialServiceCMD.GET_STOCK_LIST_OF_USER,
                    _user_id: customer._id
                })
                .then((oRs: any) => {
                    if (oRs && oRs.length && oRs.length > 0) {
                        oRs.map((element) => {
                            element.code = element.stock.code
                            element.currentPrice = 0
                            delete element.stock
                            return element
                        })
                        resolve(oRs)
                    }
                })
                .catch((oError) => {
                    this.netHandlerService.handleError(oError)
                    reject(oError)
                })
                .finally(() => {
                    console.log(`Getting Valid stocks for selling`)
                })
        })
    }

    getFinanceInfoOfCurrentUser(user: ReturnedCustomer) {
        let spinnerRef = this.spinnerService.start()
        this.systemConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_AFFORDABLE_WITHDRAW,
                _user_id: user._id,
                to_date: this.f.interestDate.value.setHours(22, 30, 30),
            })
            .then((oRs: UserFinanceStatus) => {
                this.currentFinanceInfo.currentBalance = oRs.balance
                    ? Number(oRs.balance.toFixed(0))
                    : 0
                this.currentFinanceInfo.validCashForBuying = oRs.affordableWithdraw
                    ? Number(oRs.affordableWithdraw.toFixed(0))
                    : 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    getCurrentPriceForStocks(): void {
        this.listOfStocks.map(async (element: AStock) => {
            let newPrice = await this.getCurrentPriceForStock(element.code)
            element.currentPrice = newPrice
            this.changeDetectorRefs.detectChanges()
            return element
        })
    }

    getCurrentPriceForStock(code: string): Promise<number> {
        return new Promise(async (resolve, reject) => {
            this.loansService
                .getStockPriceFromBackend({
                    cmd: 'get_realtime_stock_price',
                    stock: code
                })
                .then((oRs: number) => {
                    resolve(oRs)
                })
                .catch((error) => reject(error))
        })
    }
    getLoansOfUser(customer: ReturnedCustomer) {
        // let spinnerRef = this.spinnerService.start()
        this.loansService
            .getLoansOfUser({
                cmd: ReportServiceCMD.GET_LOANS_OF_USER,
                _user_id: customer._id,
                to_date: this.f.interestDate.value.setHours(22, 30, 30),
                limit: this.pageSize,
                offset: this.skip
            })
            .then((oRs: any) => {
                this.totalItems = oRs.total ? oRs.total : 0
                if (this.totalItems > 0) {
                    oRs.data.map((element) => {
                        element.transaction_value = Number(element.transaction_value)
                        element.transaction_cost = Number(element.transaction_cost)
                        element.price = Number(element.price)
                        return element
                    })
                }
                this.onGoingLoansList = oRs.data
                this.netHandlerService.handleSuccess('Get successfully', 'OK')
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }

    getEndedLoansButNotSettlementInterestOfUser(customer: ReturnedCustomer) {
        // let spinnerRef = this.spinnerService.start()
        this.loansService
            .getLoansOfUser({
                cmd: ReportServiceCMD.GET_ENDED_LOANS_NOT_SETTLEMENT_INTEREST_OF_USER,
                _user_id: customer._id,
            })
            .then((oRs: any) => {
                this.totalItems = oRs.total ? oRs.total : 0
                if (!!oRs && oRs.length > 0) {
                    oRs.map((element) => {
                        element.transaction_value = Number(element.transaction_value)
                        element.transaction_cost = Number(element.transaction_cost)
                        element.price = Number(element.price)
                        return element
                    })
                }
                this.doneLoansList = oRs
                this.netHandlerService.handleSuccess('Get successfully', 'OK')
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }
    getInterestValueOfUserForOnGoingLoans(customer: ReturnedCustomer) {
        // let spinnerRef = this.spinnerService.start()
        let to_date = this.f.interestDate.value.getTime()
        this.systemConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_INTEREST_VALUE_FOR_ONGOING_LOANS,
                _user_id: customer._id,
                to_date: to_date
            })
            .then((oRs: any) => {
                this.currentInterestValueForOnGoingLoans = oRs.value
                    ? Number(oRs.value.toFixed(0))
                    : 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }
    getInterestValueOfUserForDoneLoans(customer: ReturnedCustomer) {
        // let spinnerRef = this.spinnerService.start()
        let to_date = this.f.interestDate.value.getTime()
        this.systemConfigService
            .getInfoFromTradingSVC({
                cmd: FinancialServiceCMD.GET_CURRENT_INTEREST_VALUE_FOR_DONE_LOANS,
                _user_id: customer._id,
                to_date: to_date
            })
            .then((oRs: any) => {
                this.currentInterestValueForDoneLoans = oRs.value
                    ? Number(oRs.value.toFixed(0))
                    : 0
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                // this.spinnerService.stop(spinnerRef)
            })
    }

    onTyping(sValue: string) {
        this.getUserList(sValue)
    }

    getUserList(sTypingValue: string) {
        this.userNetService
            .getAllValidUsers({
                $limit: 10,
                $skip: 0,
                '$select[]': ['email', 'first_name', 'last_name', '_id', 'mobile'],
                'email[$like]': `%${sTypingValue}%`,
                status: UserStatus.ACTIVE,
                _role_id: this.role.TRADING_USER
            })
            .then((user: ReturnedCustomersRs) => {
                this.listOfCustomers = user.data.map((value) => {
                    value.name = value.last_name + ' ' + value.first_name
                    return value
                })
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => { })
    }

    onClickToSettleInterest() {
        let action: Actions[] = [
            {
                actionFn: () => {
                    this.onInterestDeductionFromCash()
                    this.messageDialogService.closeDialog()
                },
                actionText: 'Interest deduction from cash'
            },
            {
                actionFn: () => {
                    this.onInterestDeductionFromBalance()
                    this.messageDialogService.closeDialog()
                },
                actionText: 'Interest deduction from balance'
            }
        ]
        let settlemtAmount = 0
        if (this.isOnGoingLoansSettlement) {
            settlemtAmount += this.currentInterestValueForOnGoingLoans
        }
        if (this.isDoneLoansSettlement) {
            settlemtAmount += this.currentInterestValueForDoneLoans
        }
        let option: DialogOption = {
            type: MessageType.ACTIONS,
            title: 'Settlement Interest',
            message:
                `You are going to do interest settlement with amount <b> ${settlemtAmount} </b> VND to ${this.defaultDate}.
                 <br /><b>If you choose option 1:</b> Interest settlement from cash. System just note this amount is calculated already.
                 <br /><b>If you choose option 2:</b> Interest settlement from customer's account. System will do a withdraw order directly to customer's balance in case user's balance is sufficient to withdraw`,
            Actions: action
        }
        this.messageDialogService.openDialog(option)
    }

    onInterestDeductionFromBalance(): void {
        let spinnerRef = this.spinnerService.start()
        let interest_date = this.f.interestDate.value.getTime()
        let settlement_for = []
        let settlement_amount = 0
        if (this.isDoneLoansSettlement) {
            settlement_for.push('done_loans')
            settlement_amount += this.currentInterestValueForDoneLoans
        }
        if (this.isOnGoingLoansSettlement) {
            settlement_for.push('ongoing_loans')
            settlement_amount += this.currentInterestValueForOnGoingLoans
        }
        this.loansService
            .interestDeductionFromBalance({
                cmd: 'interest_deduction_from_balance',
                _user_id: this.targetUser._id,
                interest_date: interest_date,
                interest_amount: settlement_amount,
                interest_amount_of_done_loans: this.currentInterestValueForDoneLoans,
                interest_amount_of_ongoing_loans: this.currentInterestValueForOnGoingLoans,
                settlement_for: settlement_for
            })
            .then((oRs: any) => {
                this.netHandlerService.handleSuccess('Interest deduction completed successfully', 'OK')
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onInterestDeductionFromCash(): void {
        let spinnerRef = this.spinnerService.start()
        let interest_date = this.f.interestDate.value.getTime()
        let settlement_for = []
        let settlement_amount = 0
        if (this.isDoneLoansSettlement) {
            settlement_for.push('done_loans')
            settlement_amount += this.currentInterestValueForDoneLoans
        }
        if (this.isOnGoingLoansSettlement) {
            settlement_for.push('ongoing_loans')
            settlement_amount += this.currentInterestValueForOnGoingLoans
        }
        this.loansService
            .interestDeductionFromCash({
                cmd: 'interest_deduction_from_cash',
                _user_id: this.targetUser._id,
                interest_date: interest_date,
                interest_amount: settlement_amount,
                interest_amount_of_done_loans: this.currentInterestValueForDoneLoans,
                interest_amount_of_ongoing_loans: this.currentInterestValueForOnGoingLoans,
                settlement_for: settlement_for
            })
            .then((oRs: any) => {
                this.netHandlerService.handleSuccess('Interest deduction completed successfully', 'OK')
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    getEscrowAllInformation(customer: ReturnedCustomer) {
        let spinnerRef = this.spinnerService.start()
        this.loansService
            .getAlerts({
                params: {
                    $limit: 1,
                    $skip: 0,
                    columns: [
                        '_id',
                        'analysed_time',
                        'original_escrow_rate',
                        'current_escrow_rate',
                        'profit',
                        'net_assets',
                        '_user_id',
                        'loan_amount',
                        'min_escrows_value',
                        'current_escrows_value',
                        'agreement_escrow_rate'
                    ],
                    _user_id: customer._id
                }
            })
            .then((oRs: ReturnedAlerts) => {
                if (oRs.data.length > 0) {
                    this.currentEscrowRate = oRs.data[0].current_escrow_rate
                }
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    openMatchDetailHistoryDialog(_stock_order_id: number): void {
        const dialogRef = this.dialog.open(AFShowMatchHistoryDialog, {
            width: '40%',
            data: { '_stock_order_id': _stock_order_id }
        })

        dialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed')
        })
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getLoansOfUser(this.targetUser)
    }

    public getLoansValue(element: AnOnGoingLoan) {
        return Number(element.transaction_value) + Number(element.transaction_cost)
    }

    public getRealLoansValue(element: AnOnGoingLoan) {
        let totalLoan = Number(element.transaction_value) + Number(element.transaction_cost)
        return totalLoan - totalLoan * Number(element.stock_order.escrow_rate) / 100
    }

    public getRealEscrowValue(element: AnOnGoingLoan) {
        let totalLoan = Number(element.transaction_value) + Number(element.transaction_cost)
        return totalLoan * Number(element.stock_order.escrow_rate) / 100
    }

    public getLoansValueOfDoneLoans(element: ADoneLoan) {
        return Number(element.buy_cost) + Number(element.buy_value)
    }
    getEstimatedProfit(element: AnOnGoingLoan) {
        let currentPrice = 0
        if (
            this.listOfStocks &&
            this.listOfStocks.length > 0 &&
            this.listOfStocks.findIndex((x: any) => x.code === element.stock.code) >=
            0
        ) {
            let index = this.listOfStocks.findIndex(
                (x: any) => x.code === element.stock.code
            )
            currentPrice = this.listOfStocks[index].currentPrice | 0
        }
        let estimatedProfit =
            this.reportHelperLocalService.estimateLoansProfit({
                buyPrice: element.price,
                quantity: element.quantity,
                sellPrice: currentPrice,
                transaction_value: element.transaction_value,
                buy_transaction_cost: element.transaction_cost,
                start_date: element.start_date,
                interest: element.interest
            })
        for (let i = 0; i < this.onGoingLoansList.length; i++) {
            if (this.onGoingLoansList[i]._id === element._id) {
                this.onGoingLoansList[i].estimated_profit = estimatedProfit
            }
        }
        return estimatedProfit
    }

    public getCurrentPrice(element: AnOnGoingLoan) {
        let currentPrice = 0
        if (
            this.listOfStocks &&
            this.listOfStocks.length > 0 &&
            this.listOfStocks.findIndex((x: any) => x.code === element.stock.code) >=
            0
        ) {
            let index = this.listOfStocks.findIndex(
                (x: any) => x.code === element.stock.code
            )
            currentPrice = this.listOfStocks[index].currentPrice | 0
        }
        return currentPrice
    }

    public getMinRequiredReserve(element: AnOnGoingLoan) {
        return (
            Number(element.stock_order.escrow_rate) +
            Number(element.stock_order.escrow_rca)
        )
    }

    public getRealTimeEscrowValue(element: AnOnGoingLoan): number {
        let escrow_value = (Math.abs(element.transaction_value) + Math.abs(element.transaction_cost)) * element.stock_order.escrow_rate / 100
        let estimated_profit = this.getEstimatedProfit(element)
        return escrow_value + estimated_profit

    }

    public displayRealTimeEscrowValue(element: AnOnGoingLoan): string {
        let current_escrow_value = Math.round(this.getRealTimeEscrowValue(element));
        return current_escrow_value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    }

    public getCurrentEscrowRate(element: AnOnGoingLoan) {

        let current_escrow_value = this.getRealTimeEscrowValue(element)
        let currentPrice = 0
        if (
            this.listOfStocks &&
            this.listOfStocks.length > 0 &&
            this.listOfStocks.findIndex((x: any) => x.code === element.stock.code) >=
            0
        ) {
            let index = this.listOfStocks.findIndex(
                (x: any) => x.code === element.stock.code
            )
            currentPrice = this.listOfStocks[index].currentPrice | 0
        }
        // let current_sell_value = this.reportHelperLocalService.getEstimatedNETSellValue({
        //     buyPrice: element.price,
        //     quantity: element.quantity,
        //     sellPrice: currentPrice,
        //     transaction_value: element.transaction_value,
        //     buy_transaction_cost: element.transaction_cost,
        //     start_date: element.start_date,
        //     interest: element.interest
        // })

        return current_escrow_value / (Math.abs(element.transaction_value) + Math.abs(element.transaction_cost)) * 100

    }

    public setMyStyles(element: AnOnGoingLoan) {
        return {
            'color': (element.estimated_profit > 0) ? 'green' : 'red',
        };
    }

    public displayProperty(value) {
        if (value) {
            return value.email
        }
    }

    public displayStockReturnDate(element: AnOnGoingLoan): string {
        let property_return_date = element.property_return_date
        let numberOfBussinessDays = this.dayTimeHelperService.howManyWorkingDays(new Date().getTime(), property_return_date)
        if (numberOfBussinessDays > NumberOfDaysForStockReturnDate) {
            return 'T-0'
        }
        if (numberOfBussinessDays > 0) {
            return 'T-' + (NumberOfDaysForStockReturnDate - numberOfBussinessDays)
        }
        return ''
    }
}
