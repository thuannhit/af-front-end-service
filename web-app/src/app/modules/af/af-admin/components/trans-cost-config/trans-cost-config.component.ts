import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectorRef
} from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import {
    NetHandlerService,
    UserLocalService,
    SpinnerService
} from '@/shared/_services/_local-services'
import {
    SystemConfigService
} from '@/shared/_services/_net-services'
import { PreferenceType } from '@/shared/_common'
import { AFAddNewTransCostComponentDialog } from '@/modules/af/af-admin/components/trans-cost-config/new-trans-cost-dialog'
export interface APreferenceStatement {
    _id: number
    effective_date: Date
    key: string
    value: string,
    checked?: boolean
}
export interface ReturnedPreferenceStatement {
    total: number
    limit: number
    skip: number
    data: APreferenceStatement[]
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'trans-cost-config.component.html',
    styleUrls: ['trans-cost-config.component.scss']
})
export class AFTransCostConfigComponent implements OnInit, AfterViewInit {
    displayedColumns = ['checked', 'trans_cost', 'effective_date']
    isSomeRowBeingChecked = false
    loading = false
    submitted = false
    returnUrl: string
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    costList: APreferenceStatement[]
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sysConfigSvc: SystemConfigService,
        private dialog: MatDialog,
        private userLocalSvc: UserLocalService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        // redirect to home if not already logged in
        if (!this.userLocalSvc.currentUserValue) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }

    ngAfterViewInit() {
        this.getTransCostList()
    }

    getTransCostList(): void {
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .getTransCost({
                $limit: this.pageSize,
                $skip: this.skip,
                '$select[]': ['_id', 'type', 'value', 'effective_date'],
                'type': PreferenceType.TRANSACTION_COST.toString(),
                '$sort[effective_date]': -1,
            })
            .then((oRs: ReturnedPreferenceStatement) => {
                this.totalItems = oRs.total ? oRs.total : 0
                oRs.data = oRs.data.map((value) => {
                    value.checked = false
                    value.effective_date = new Date(value.effective_date)
                    return value
                })
                this.costList = oRs.data
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            }).finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onOpenAddTransCostDialog(): void {
        const dialogRef = this.dialog.open(AFAddNewTransCostComponentDialog, {
            width: '40%'
        })

        dialogRef.afterClosed().subscribe((result) => {
            this.getTransCostList()
            console.log('The dialog was closed')
        })
    }


    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.costList.every(
            (element) => element.checked == false
        )
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getTransCostList()
    }

    onRemoveInterestOptions(): void {
        // let aCheckedRows = this.tableInterestData.data.filter(
        //     (element) => element.checked == true
        // )
        // let oData = Object.assign([], aCheckedRows || []).map(
        //     (element) => element.id
        // )
        // this.icProductService.removeICInterestOptions(
        //     oData,
        //     (oResult) => {
        //         this.getICInterestList()
        //     },
        //     (oError) => {
        //         console.log(oError)
        //     }
        // )
    }
    onUpdateInterestOptions(): void {
        let aCheckedRows = this.costList.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element) => {
            return {
                _id: element._id,
                value: element.value,
                effective_date: element.effective_date,
                description: element.description,
            }
        })
        let spinnerRef = this.spinnerService.start()
        this.sysConfigSvc
            .updateTransactionCostConfig(
                {
                    effective_date: new Date(oData[0].effective_date).getTime(),
                    value: oData[0].value,
                    description: oData[0].description,
                    type: PreferenceType.TRANSACTION_COST.toString(),
                },
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getTransCostList()
                    this.netHandlerService.handleSuccess(`Transaction cost was updated`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onRefreshTransCostTable():void{
        this.getTransCostList()
    }
}
