import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import { ValidationService, SpinnerService, NetHandlerService } from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'
import { PreferenceType } from '@/shared/_common'

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'new-trans-cost-dialog.component.html',
    styleUrls: ['new-trans-cost-dialog.component.scss']
})
export class AFAddNewTransCostComponentDialog implements OnInit {
    newTransCostForm: FormGroup
    loading = false
    submitted = false
    constructor(
        public dialogRef: MatDialogRef<AFAddNewTransCostComponentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private sysConfigSvc: SystemConfigService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService
    ) { }

    onNoClick(): void {
        this.dialogRef.close()
    }
    ngOnInit() {
        this.newTransCostForm = this.formBuilder.group({
            cost: [
                '',
                [Validators.required, ValidationService.decimalValidation]
            ],
            effective_date: ['', [Validators.required]],
            description: ['']
        })
    }
    get f() {
        return this.newTransCostForm.controls
    }
    onCloseCreationDialog() {
        this.dialogRef.close()
    }

    onSubmit() {
        // stop here if form is invalid
        if (this.newTransCostForm.invalid) {
            return
        }
        let spinnerRef = this.spinnerService.start()
        let oData = {
            effective_date: new Date(this.f.effective_date.value).getTime(),
            value: this.f.cost.value.toString(),
            type: PreferenceType.TRANSACTION_COST.toString(),
            description: this.f.description.value
        }
        this.sysConfigSvc
            .addNewTransCost(oData)
            .then((oRs: any) => {
                this.onCloseCreationDialog()
                this.netHandlerService.handleSuccess('A new Trans Cost was added', 'OK')
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
