import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { PageEvent } from '@angular/material/paginator'
import { invert } from 'lodash'
import {
    UserLocalService,
    SpinnerService,
    NetHandlerService,
    MessageDialogService
} from '@/shared/_services/_local-services'
import {
    UserNetService,
    SystemConfigService,
    DepositNetServices,

} from '@/shared/_services/_net-services'
import {
    DepositCustomerStatus
} from '@/shared/_common/CONSTANTS'
import { DialogOption, Actions } from '@/shared/_models'
const INFINITY_PROPERTY_RETURN_DATE = 4102419600000
export interface ReturnedBank {
    _id?: number
    bank_name: string
}

export interface ReturnedBanks {
    total: number
    limit: number
    skip: number
    data: ReturnedBank[]
}
export interface ReturnedCustomer {
    _id: number
    first_name: string
    last_name: string
    phone_number: string,
    email: string,
    bank_name: string,
    bank_id: number,
    bank_branch: string,
    bank_account_number: string,
    checked?: boolean,
    customer_code: string
}

export interface ReturnedCustomers {
    total: number
    limit: number
    skip: number
    data: ReturnedCustomer[]
}

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'deposit-customers.component.html',
    styleUrls: ['deposit-customers.component.scss']
})
export class AFDepositCustomersComponent implements OnInit, AfterViewInit {
    newCustomerForm: FormGroup
    returnUrl: string
    displayedColumns: string[] = ['checked', 'customer_name', 'phone_number', 'email', 'bank_name', 'bank_branch', 'bank_account_no']
    // MatPaginator Output
    isSomeRowBeingChecked: boolean = false
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    pageEvent: PageEvent
    isOpeningNewCustomerCreationForm: boolean = false
    listOfBanks: ReturnedBank[]
    listOfCurrentCustomers: ReturnedCustomer[]
    today: Date
    defaultSelectedDay: Date
    sTypingValue: string = ''
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private userNetService: UserNetService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private sysConfigSvc: SystemConfigService,
        private netHandlerService: NetHandlerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private depositNetService: DepositNetServices,
        private messDialogService: MessageDialogService
    ) {
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.prepareForm()
        this.today = new Date()
        this.defaultSelectedDay = new Date(this.today.setHours(0, 0, 1))
    }

    prepareForm(): void {
        this.newCustomerForm = this.formBuilder.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', Validators.required],
            phone_number: ['', Validators.required],
            bank_account_no: ['', Validators.required],
            bank_branch: ['', Validators.required],
            bank_name: ['', Validators.required],
            customer_code: ['', Validators.required]
        })
        this.changeDetectorRefs.detectChanges()
    }

    get f() {
        return this.newCustomerForm.controls
    }

    ngAfterViewInit() {
        // this.getValidBanks()
        this.getCurrentCustomers()
    }

    ngOnDestroy() { }

    onClickNewCustomerCreation(): void {
        this.isOpeningNewCustomerCreationForm = true
    }

    onClickCancel(): void {
        this.isOpeningNewCustomerCreationForm = false
    }


    onSearchName(s?: string): void {
        this.getCurrentCustomers()
    }

    onRefreshCustomersTable(): void {
        this.getCurrentCustomers()
    }

    getCurrentCustomers(): void {
        let spinnerRef = this.spinnerService.start()
        this.depositNetService
            .getCurrentCustomers({
                $limit: this.pageSize,
                $skip: this.skip,
                '$select[]': ['_id', 'customer_code', 'first_name', 'last_name', 'phone_number', 'email', 'bank_name', 'bank_branch', 'bank_account_number'],
                '$sort[created_at]': 1,
                status: DepositCustomerStatus.ACTIVE
                // customerSearch: this.sTypingValue || ''
            })
            .then((oRs: ReturnedCustomers) => {
                this.totalItems = oRs.total ? oRs.total : 0
                this.listOfCurrentCustomers = oRs.data.map((value) => {
                    value.checked = false
                    return value
                })
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

    onTableChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.onRefreshCustomersTable()
    }

    onSubmit(): void {
        let spinnerRef = this.spinnerService.start()
        if (this.newCustomerForm.invalid) {
            return
        }
        let oInputData: {
            first_name: string,
            last_name: string,
            email: string,
            phone_number: string,
            bank_name: string,
            bank_branch: string,
            bank_account_number: string,
            customer_code: string
        }
        oInputData = {
            first_name: this.f.first_name.value,
            last_name: this.f.last_name.value,
            email: this.f.email.value,
            phone_number: this.f.phone_number.value,
            bank_name: this.f.bank_name.value,
            bank_account_number: this.f.bank_account_no.value,
            bank_branch: this.f.bank_branch.value,
            customer_code: this.f.customer_code.value
        }
        
        this.depositNetService
            .addNewCustomer(oInputData)
            .then((oRs: ReturnedCustomers) => {
                this.prepareForm();
                this.isOpeningNewCustomerCreationForm = false
                this.netHandlerService.handleSuccess(
                    `A New customer was added`,
                    'OK'
                )
                this.getCurrentCustomers()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                
                this.spinnerService.stop(spinnerRef)
            })

    }

    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.listOfCurrentCustomers.every(
            (element) => element.checked == false
        )
    }

    onUpdateCustomer(): void {
        let spinnerRef = this.spinnerService.start()
        let aCheckedRows: ReturnedCustomer[] = this.listOfCurrentCustomers.filter(
            (element) => element.checked == true
        )
        if (aCheckedRows.length > 1) {
            alert('Sorry, updating multi items at once is not supported yet')
            return
        }
        let oData = Object.assign([], aCheckedRows || []).map((element: ReturnedCustomer) => {
            return {
                _id: element._id,
                first_name: element.first_name,
                last_name: element.last_name,
                phone_number: element.phone_number,
                email: element.email,
                bank_account_number: element.bank_account_number,
                bank_name: element.bank_name,
                bank_branch: element.bank_branch,
                customer_code: element.customer_code
            }
        })

        this.depositNetService.updateCustomer
            (
                {
                    first_name: oData[0].first_name,
                    last_name: oData[0].last_name,
                    phone_number: oData[0].phone_number,
                    email: oData[0].email,
                    bank_account_number: oData[0].bank_account_number,
                    bank_name: oData[0].bank_name,
                    bank_branch: oData[0].bank_branch,
                    customer_code: oData[0].customer_code
                },
                oData[0]._id
            )
            .then(
                (oResult: any) => {
                    this.getCurrentCustomers()
                    this.netHandlerService.handleSuccess(`Customer was updated`, 'OK')
                },
                (error) => {
                    this.netHandlerService.handleError(error)
                }
            )
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }

}
