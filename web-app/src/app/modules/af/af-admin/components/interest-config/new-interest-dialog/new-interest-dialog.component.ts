import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { first } from 'rxjs/operators'
import {
    MatDialog,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog'
import { ValidationService, SpinnerService, NetHandlerService } from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services'
import { PreferenceType } from '@/shared/_common'

export interface DialogData {
    // animal: string;
    // name: string;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'new-interest-dialog.component.html',
    styleUrls: ['new-interest-dialog.component.scss']
})
export class AFAddNewInterestComponentDialog implements OnInit {
    newInterestFrom: FormGroup
    loading = false
    submitted = false
    constructor(
        public dialogRef: MatDialogRef<AFAddNewInterestComponentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private sysConfigSvc: SystemConfigService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService
    ) { }

    onNoClick(): void {
        this.dialogRef.close()
    }
    ngOnInit() {
        this.newInterestFrom = this.formBuilder.group({
            interest: [
                '',
                [Validators.required, ValidationService.decimalValidation]
            ],
            effective_date: ['', [Validators.required]],
            description: ['']
        })
    }
    get f() {
        return this.newInterestFrom.controls
    }
    onCloseCreationDialog() {
        this.dialogRef.close()
    }

    onSubmit() {
        // stop here if form is invalid
        if (this.newInterestFrom.invalid) {
            return
        }
        let spinnerRef = this.spinnerService.start()
        let oData = {
            effective_date: new Date(this.f.effective_date.value).getTime(),
            value: this.f.interest.value.toString(),
            type: PreferenceType.BASE_INTEREST.toString(),
            description: this.f.description.value
        }
        this.sysConfigSvc
            .addNewInterest(oData)
            .then((oRs: any) => {
                this.onCloseCreationDialog()
                this.netHandlerService.handleSuccess('A new interest was added', 'OK')
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
