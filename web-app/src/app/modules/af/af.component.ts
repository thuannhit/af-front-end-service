import { UserLocalService } from '@/shared/_services/_local-services';
import { GeneralUser } from '@/shared/_models';
import { Component, OnDestroy, OnInit} from '@angular/core';
import { I18nComponent } from '@/i18n/container/i18n.component';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers';
@Component({ templateUrl: 'af.component.html', styleUrls: ['af.component.scss'] })
export class AFComponent extends I18nComponent implements OnInit, OnDestroy {
    currentUser: GeneralUser;
    returnUrl: string;
    constructor(
        private userLocalService: UserLocalService,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate);
        this.userLocalService.currentUser.subscribe(x => this.currentUser = x);
        // TODO: Checking whether navigating to admin or trading UIs
        // Currently, just navigating to admin ui
        // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/af/admin';
        // this.router.navigate([this.returnUrl]);
    }
    ngOnDestroy() {
    }
    ngOnInit() {
        super.ngOnInit();
    }
}
