import {
    Component,
    OnInit,
    AfterViewInit,
    ViewEncapsulation
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import {
    UserLocalService,
} from '@/shared/_services/_local-services'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'reports.component.html',
    styleUrls: ['reports.component.scss']
})
export class AFTradingReportsComponent extends I18nComponent implements OnInit, AfterViewInit {
    returnUrl: string
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }

        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/trading/login'])
        }
    }

    ngOnInit() {
        super.ngOnInit()
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
    }
    ngAfterViewInit() { }
    ngOnDestroy() { }

}
