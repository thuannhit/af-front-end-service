import { Routes, RouterModule } from '@angular/router';
import { AFTradingAuthGuard } from '@/shared/_helpers';
import { AFTradingAssetsReportComponent } from '@/modules/af/af-trading/components/assets-report';
import { AFTradingReportsComponent } from '@/modules/af/af-trading/pages/reports';


const routes: Routes = [
    {
        path: '',
        component: AFTradingReportsComponent,
        canActivate: [AFTradingAuthGuard],
        children: [
            { path: '', redirectTo: 'assets-report' },
            { path: 'assets-report', component: AFTradingAssetsReportComponent }
        ],
    }
];

export const afTradingReportsRoutingModule = RouterModule.forChild(routes);