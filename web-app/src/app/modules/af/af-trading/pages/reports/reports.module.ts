import { NgModule } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { HttpClient, HttpClientModule } from '@angular/common/http'
import { AFTradingReportsComponent } from '@/modules/af/af-trading/pages/reports'
import { AFTradingAssetsReportComponent } from '@/modules/af/af-trading/components/assets-report'
import { afTradingReportsRoutingModule } from './reports.routing'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatChipsModule,
    MatRadioModule,
    MatProgressBarModule,
    MatRippleModule
} from '@angular/material'
import { ngTHMatInputCommifiedModule } from '@/modules/af/shared-modules/mat-input-commified/'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(
        http,
        './assets/i18n/af-trading-reports/',
        '.json'
    )
}
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'

@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afTradingReportsRoutingModule,
        CommonModule,
        FormsModule,
        MatCardModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatAutocompleteModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatSelectModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        }),
        ngTHMatInputCommifiedModule,
        MatChipsModule,
        NgxMaterialTimepickerModule,
        MatRadioModule,
        MatProgressBarModule,
        MatRippleModule
    ],
    exports: [HttpClientModule],
    declarations: [
        AFTradingReportsComponent,
        AFTradingAssetsReportComponent
    ],
    entryComponents: [],
    providers: [SystemConfigService],
    bootstrap: [AFTradingReportsComponent]
})
export class AFTradingReportsModule { }
