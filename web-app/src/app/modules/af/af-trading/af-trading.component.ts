import { Router } from '@angular/router';

import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({ templateUrl: 'af-trading.component.html', styleUrls: ['af-trading.component.scss'] })
export class AFTradingComponent extends I18nComponent implements OnInit, OnDestroy {
    constructor(
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
    }
    ngOnDestroy() {
    }
    ngOnInit() {
        super.ngOnInit();
    }
}
