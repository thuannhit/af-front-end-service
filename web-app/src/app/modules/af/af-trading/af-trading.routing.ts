import { Routes, RouterModule } from '@angular/router';

// import { HomeComponent } from '@/modules/home';
import { AFTradingAuthGuard } from '@/shared/_helpers';
import { AFTradingComponent } from '@/modules/af/af-trading/af-trading.component';
import { AFTradingLoginComponent } from '@/modules/login';
import { AFAdminRegisterComponent, AFTradingRegisterComponent } from '@/modules/af/components/af-register';

const routes: Routes = [
    { path: 'login', component: AFTradingLoginComponent },
    { path: 'register', component: AFTradingRegisterComponent },
    {
        path: '',
        component: AFTradingComponent,
        canActivate: [AFTradingAuthGuard],
        children: [
            { path: 'reports', loadChildren: () => import('@/modules/af/af-trading/pages/reports/reports.module').then(m => m.AFTradingReportsModule), canActivate: [AFTradingAuthGuard] },
        ],
    }
];

export const afTradingRoutingModule = RouterModule.forChild(routes);