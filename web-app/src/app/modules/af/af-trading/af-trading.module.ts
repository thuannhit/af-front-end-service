import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'
import { AFTradingLoginComponent } from '@/modules/login'
import { AFTradingRegisterComponent } from '@/modules/af/components/af-register'
import { afTradingRoutingModule } from './af-trading.routing';
import { AFTradingComponent } from './af-trading.component';
import {
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatTooltipModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule
} from '@angular/material';
import { NgMaterialMultilevelMenuModule } from '@/modules/af/shared-modules/material-multilevel-menu'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import {
    HttpClient,
    HttpClientModule
} from '@angular/common/http'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
export function createTranslateLoader(http: HttpClient) {
    console.log('Get i18n for af-trading')
    return new TranslateHttpLoader(http, './assets/i18n/af-trading/', '.json')
}
@NgModule({

    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        afTradingRoutingModule,
        CommonModule,
        MatDatepickerModule,
        MatTooltipModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        NgMaterialMultilevelMenuModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: true
        })
    ],
    exports: [
        HttpClientModule
    ],
    declarations: [
        AFTradingComponent,
        AFTradingLoginComponent,
        AFTradingRegisterComponent
    ],
    entryComponents: [],
    bootstrap: [AFTradingComponent]
})
export class AFTradingModule { };