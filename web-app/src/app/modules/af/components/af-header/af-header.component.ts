import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InternationalizationService } from '@/shared/_services';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { GeneralUser } from '@/shared/_models'
import { UserLocalService } from '@/shared/_services/_local-services';
import {
    UserRole,
} from '@/shared/_common/'
@Component({
    selector: 'af-header',
    templateUrl: 'af-header.component.html',
    styleUrls: ['af-header.component.scss']
})
export class AFHeaderComponent implements OnInit {
    userPictureOnly: boolean = false;
    translationsUrl = '/assets/i18n/header';
    language: any = {};
    isDisable: boolean;
    currentUser: GeneralUser
    userRole: number
    role = UserRole
    themes = [
        {
            value: 'default',
            name: 'Light',
        },
        {
            value: 'dark',
            name: 'Dark',
        },
        {
            value: 'cosmic',
            name: 'Cosmic',
        },
        {
            value: 'corporate',
            name: 'Corporate',
        },
    ];
    currentTheme = 'default';
    @Output() public sidenavToggle = new EventEmitter();
    constructor(
        private inznservice: InternationalizationService,
        private router: Router,
        public dialog: MatDialog,
        private userLocalService: UserLocalService,
    ) {

        this.userLocalService.currentUser.subscribe(x => {
            this.currentUser = x;
        });
    }
    ngOnInit() {

        this.inznservice.language.subscribe((val) => {
            if (val == "en") {
                this.inznservice.use(val, this.translationsUrl).then((data) => {
                    this.language = data;
                });
            }
            else if (val == "vn") {
                this.inznservice.use(val, this.translationsUrl).then((data) => {
                    this.language = data;
                });
            }
        });
    }

    onOpenAssetReportsPage() {
        if (this.currentUser.user._role_id === this.role.MG_USER || this.currentUser.user._role_id === this.role.TRADING_USER) {
            this.router.navigate(['/af/trading/reports/assets-report']);
            return;
        }
        return
    }

    public onToggleSidenav = () => {
        this.sidenavToggle.emit();
    }

    logout() {
        this.userLocalService.logout();
        if (this.router.url.includes('admin') && this.router.url.includes('af')) {
            this.router.navigate(['/af/admin/login']);
            return;
        };

        this.router.navigate(['/af/trading/login']);
    }
}


