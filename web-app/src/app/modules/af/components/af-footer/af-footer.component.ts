import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { InternationalizationService } from '@/shared/_services';
@Component({
    selector: 'af-footer',
    templateUrl: 'af-footer.component.html',
    styleUrls: ['af-footer.component.scss']
})
export class AFFooterComponent implements OnInit {
    private destroy$: Subject<void> = new Subject<void>();
    userPictureOnly: boolean = false;
    translationsUrl = '/assets/i18n/footer';
    language: any = {};
    @Output() public sidenavToggle = new EventEmitter();
    constructor(
        private inznservice: InternationalizationService,
        private http: HttpClient,
    ) {
    }
    ngOnInit() {


    }

}


