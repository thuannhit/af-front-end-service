﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { Router } from '@angular/router'
import {
    FormBuilder,
    FormGroup,
    Validators,
} from '@angular/forms'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

import { MessageType } from '@/shared/_common'
import { UserNetService } from '@/shared/_services/_net-services'
import { ValidationService, MessageDialogService, NetHandlerService } from '@/shared/_services/_local-services'

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'register.component.html',
    styleUrls: ['register.component.scss']
})
export class AFAdminRegisterComponent extends I18nComponent implements OnInit {
    registerForm: FormGroup
    loading = false
    submitted = false
    hide = true
    isAdminUI = true
    constructor(
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService,
        private formBuilder: FormBuilder,
        private router: Router,
        private netUserService: UserNetService,
        private messDialogService: MessageDialogService,
        private netHandlerService: NetHandlerService,
    ) {
        super(store, translate)
        this.registerForm = formBuilder.group({
            hideRequired: false,
            floatLabel: 'auto'
        })
    }

    ngOnInit() {
        super.ngOnInit();
        this.registerForm = this.formBuilder.group(
            {
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                PIN: ['', Validators.required],
                PIN_issue_location: ['', Validators.required],
                PIN_issue_date: ['', Validators.required],
                email: ['', [Validators.required, Validators.email]],
                address: ['', Validators.required],
                mobile: ['', Validators.required],
                // bank_branch: ['', Validators.required],
                // bankaccount_number: ['', Validators.required],
                // _bank_id: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(6)]],
                confirmPassword: ['', Validators.required]
            },
            { validator: ValidationService.MustMatch('password', 'confirmPassword') }
        )
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls
    }

    onSubmit() {
        this.submitted = true

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return
        }
        this.loading = true
        let registerInfo = {
            first_name: this.f.firstName.value,
            last_name: this.f.lastName.value,
            gender: 0,
            email: this.f.email.value,
            mobile: this.f.mobile.value,
            password: this.f.password.value,
            address: this.f.address.value,
            _bank_id: 0,
            PIN: this.f.PIN.value,
            PIN_issue_date: this.f.PIN_issue_date.value.getTime(),
            PIN_issue_location: this.f.PIN_issue_location.value,
            bankaccount_number: '000',
            bank_branch: 'NO Branch',
        }
        this.netUserService
            .registerAdmin(registerInfo)
            .then((oRs) => {
                let messConfig = {
                    type: MessageType.INFORMATION,
                    title: 'User registered successfully',
                    message: 'Please check your email and click on the confirmation link',
                    cancelText: 'Cancel',
                    confirmText: 'OK',
                    OKAction: () => {
                        this.router.navigate(['af/admin/login'])
                    },
                    CANCELAction: () => {
                        this.router.navigate(['af/admin/login'])
                    }
                }
                this.messDialogService.openDialog(messConfig)
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.loading = false
            })
    }
    onClickCancel(): void {
        this.router.navigate(['af/admin/login'])
    }
}

@Component({
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'register.component.html',
    styleUrls: ['register.component.scss']
})
export class AFTradingRegisterComponent extends I18nComponent implements OnInit {
    registerForm: FormGroup
    loading = false
    submitted = false
    hide = true
    isAdminUI = false
    constructor(
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService,
        private formBuilder: FormBuilder,
        private router: Router,
        private netUserService: UserNetService,
        private messDialogService: MessageDialogService,
        private netHandlerService: NetHandlerService,
    ) {
        super(store, translate)
        this.registerForm = formBuilder.group({
            hideRequired: false,
            floatLabel: 'auto'
        })
    }

    ngOnInit() {
        super.ngOnInit();
        this.registerForm = this.formBuilder.group(
            {
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                PIN: ['', Validators.required],
                PIN_issue_location: ['', Validators.required],
                PIN_issue_date: ['', Validators.required],
                email: ['', [Validators.required, Validators.email]],
                address: ['', Validators.required],
                mobile: ['', Validators.required],
                // bank_branch: ['', Validators.required],
                // bankaccount_number: ['', Validators.required],
                // _bank_id: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(6)]],
                confirmPassword: ['', Validators.required]
            },
            { validator: ValidationService.MustMatch('password', 'confirmPassword') }
        )
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls
    }

    onSubmit() {
        this.submitted = true

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return
        }
        this.loading = true
        let registerInfo = {
            first_name: this.f.firstName.value,
            last_name: this.f.lastName.value,
            gender: 0,
            email: this.f.email.value,
            mobile: this.f.mobile.value,
            password: this.f.password.value,
            address: this.f.address.value,
            _bank_id: 0,
            PIN: this.f.PIN.value,
            PIN_issue_date: this.f.PIN_issue_date.value.getTime(),
            PIN_issue_location: this.f.PIN_issue_location.value,
            bankaccount_number: '000',
            bank_branch: 'NO Branch',
        }
        this.netUserService
            .registerTrading(registerInfo)
            .then((oRs) => {
                let messConfig = {
                    type: MessageType.INFORMATION,
                    title: 'User registered successfully',
                    message: 'Please check your email and click on the confirmation link',
                    cancelText: 'Cancel',
                    confirmText: 'OK',
                    OKAction: () => {
                        this.router.navigate(['af/trading/login'])
                    },
                    CANCELAction: () => {
                        this.router.navigate(['af/trading/login'])
                    }
                }
                this.messDialogService.openDialog(messConfig)
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.loading = false
            })
    }
    onClickCancel(): void {
        this.router.navigate(['af/trading/login'])
    }
}
