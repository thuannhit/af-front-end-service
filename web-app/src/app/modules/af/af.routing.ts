import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { AFComponent } from '@/modules/af/af.component';
import { AFHomeComponent } from '@/modules/af/af-home';

const routes: Routes = [
    {
        path: '',
        component: AFComponent,
        children: [
            { path: '', component: AFHomeComponent },
            { path: 'admin', loadChildren: () => import('@/modules/af/af-admin/af-admin.module').then(m => m.AFAdminModule) },
            { path: 'trading', loadChildren: () => import('@/modules/af/af-trading/af-trading.module').then(m => m.AFTradingModule) },
        ]
    },

    // otherwise redirect to home
    // { path: '**', redirectTo: 'af' }
];

export const afRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);