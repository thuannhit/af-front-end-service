import {
    Component,
    OnInit,
    AfterViewInit,
    ViewChild,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    Input,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { MatPaginator, MatTableModule } from '@angular/material'
import { PageEvent } from '@angular/material/paginator'
import {
    FormBuilder,
    FormGroup,
    Validators,
    AbstractControl
} from '@angular/forms'
import {
    UserLocalService,
    NetHandlerService,
    SpinnerService
} from '@/shared/_services/_local-services'
import { FinanceService } from '@/shared/_services/_net-services'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

export interface AFinancicalStatement {
    date?: number
    created_at?: number
    amount: number
    balance: number
    type: number
    _user_id: number
}
export interface ReturnedFiancialStatement {
    total: number
    limit: number
    skip: number
    data: AFinancicalStatement[]
}
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'financial-statements',
    templateUrl: 'financial-statements.component.html',
    styleUrls: ['financial-statements.component.scss']
})
export class FinancialStatementsComponent extends I18nComponent
    implements OnInit, AfterViewInit {
    @Input() targetUser: number
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatTableModule, { static: true }) myTable: MatTableModule
    financialStatementsForm: FormGroup
    loading: boolean = false
    returnUrl: string
    currentFinanceInfo: object = {
        cashInAccount: 0,
        validCashForBuying: 0
    }
    balanceNumber: number = 0
    financialStatements: AFinancicalStatement[]
    displayedColumns: string[] = [
        'RecordDay',
        'StatementDay',
        'Topup',
        'Withdraw',
        'Note',
        'Balance'
    ]
    startDate: Date
    minPublishedDate: Date
    maxPublishedDate: Date
    invalidPublishedDate = (d: Date): boolean => {
        const day = d.getDay()
        // Prevent Saturday and Sunday from being selected.
        return day !== 0
    }
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private formBuilder: FormBuilder,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private financeSerive: FinanceService,
        private changeDetectorRefs: ChangeDetectorRef,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
        //TODO: Check translate is herritate from the parrent (cash-management), not from financial-statements itself
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        super.ngOnInit()
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/'
        this.financialStatementsForm = this.formBuilder.group({
            fromDate: ['', Validators.required],
            toDate: [
                '',
                [
                    Validators.required,
                    (control: AbstractControl) => {
                        // const formGroup = control.parent.controls;
                        // const fromDate = formGroup.fromDate;
                        if (control.value) {
                            return null
                            // if (control.value && control.value >= control.parent.controls.fromDate.value) {
                            //     return null
                            // }
                        }
                        return { validCashAllowed: true }
                    }
                ]
            ]
        })
        const today = new Date()
        this.minPublishedDate = new Date(today)
        this.maxPublishedDate = new Date(today)
        this.minPublishedDate.setDate(this.minPublishedDate.getDate() - 1000)
        this.maxPublishedDate.setDate(this.maxPublishedDate.getDate() + 1)
    }
    get f() {
        return this.financialStatementsForm.controls
    }
    ngAfterViewInit() {
        this.getBackgroundData()
    }
    ngOnDestroy() { }

    getBackgroundData() { }

    onSubmit(): void {
        // stop here if form is invalid
        if (this.financialStatementsForm.invalid) {
            return
        }
        this.skip = 0
        this.getFinancialStatements(
            this.targetUser,
            this.f.fromDate.value,
            this.f.toDate.value
        )
        this.getBalance(this.targetUser, this.f.toDate.value)
    }

    getFinancialStatements(_user_id: number, fromDate: Date, toDate: Date) {
        if (!!_user_id) {
            let spinnerRef = this.spinnerService.start()
            this.financeSerive
                .getFinancialStatements({
                    $limit: this.pageSize,
                    $skip: this.skip,
                    '$select[]': [
                        'date',
                        'created_at',
                        'amount',
                        'balance',
                        'type',
                        '_user_id',
                        'note'
                    ],
                    'created_at[$gte]': `${fromDate.getFullYear()}-${fromDate.getMonth() +
                        1}-${fromDate.getDate()} 00:00:00`,
                    'created_at[$lte]': `${toDate.getFullYear()}-${toDate.getMonth() +
                        1}-${toDate.getDate()} 23:59:59`,
                    _user_id: _user_id,
                    '$sort[created_at]': 1
                })
                .then((oRs: ReturnedFiancialStatement) => {
                    this.totalItems = oRs.total ? oRs.total : 0
                    this.financialStatements = oRs.data
                    // Note: This is important to detect the datasource was change.
                    // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                    this.changeDetectorRefs.detectChanges()
                })
                .catch((error) => {
                    this.netHandlerService.handleError(error)
                })
                .finally(() => {
                    this.spinnerService.stop(spinnerRef)
                })
        }
    }

    getBalance = (_user_id: number, toDate: Date) => {
        let spinnerRef = this.spinnerService.start()
        this.financeSerive
            .getFinancialStatements({
                $limit: 1,
                $skip: 0,
                '$select[]': ['date', 'created_at', 'balance', '_user_id'],
                'created_at[$lte]': `${toDate.getFullYear()}-${toDate.getMonth() +
                    1}-${toDate.getDate()} 23:59:59`,
                _user_id: _user_id,
                '$sort[_id]': -1
            })
            .then((oRs: ReturnedFiancialStatement) => {
                this.totalItems = oRs.total ? oRs.total : 0
                this.balanceNumber = this.totalItems !== 0 ? oRs.data[0].balance : 0
                // Note: This is important to detect the datasource was change.
                // Please see more information from this article: https://stackoverflow.com/questions/46746598/angular-material-how-to-refresh-a-data-source-mat-table
                this.changeDetectorRefs.detectChanges()
            })
            .catch((error) => {
                this.netHandlerService.handleError(error)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
    onChange: Function = (oParams) => {
        this.pageSize = oParams.pageSize
        this.skip = oParams.pageIndex * this.pageSize
        this.getFinancialStatements(
            this.targetUser,
            this.f.fromDate.value,
            this.f.toDate.value
        )
    }
}
