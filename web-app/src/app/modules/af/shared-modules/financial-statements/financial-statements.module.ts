import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FinancialStatementsComponent } from '@/modules/af/shared-modules/financial-statements'
import {
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule
} from '@angular/material'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function createTranslateLoader(http: HttpClient) {
    alert('*** Runggni')
    return new TranslateHttpLoader(http, './assets/i18n/financial-statements/', '.json');
}
@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        MatDatepickerModule,
        MatNativeDateModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatTableModule,
        MatToolbarModule,
        MatPaginatorModule,
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: false
        })
    ],
    exports: [
        CommonModule,
        FormsModule,
        FinancialStatementsComponent,
        MatDatepickerModule,
        HttpClientModule
    ],
    declarations: [FinancialStatementsComponent],
    entryComponents: [],
    providers: [],
    bootstrap: [FinancialStatementsComponent]
})
export class FinancialStatementsModule { }
