import {
    Component,
    OnInit,
    AfterViewInit,
    ViewChild,
    ViewEncapsulation,
    ChangeDetectionStrategy,
    Input,
    ChangeDetectorRef
} from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { MatPaginator, MatTableModule } from '@angular/material'
import { PageEvent } from '@angular/material/paginator'
import {
    UserLocalService,
    NetHandlerService,
    SpinnerService
} from '@/shared/_services/_local-services'
import { StockOrderDetailService } from '@/shared/_services/_net-services'
import { I18nComponent } from '@/i18n/container/i18n.component'
import { TranslateService } from '@ngx-translate/core'
import { Store } from '@ngrx/store'
import * as fromI18n from '@/i18n/reducers'

export interface ReturnedAMatched {
    _id?: number
    match_date_time: number
    match_price: number
    match_amount: number
    property_return_date: number
}
export interface ReturnedMatchedList {
    total: number
    limit: number
    skip: number
    data: ReturnedAMatched[]
}
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
    selector: 'match-result-history',
    templateUrl: 'match-result-history.component.html',
    styleUrls: ['match-result-history.component.scss']
})
export class MatchResutHistoryComponent extends I18nComponent
    implements OnInit, AfterViewInit {
    @Input() targetStockOrder: number
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    @ViewChild(MatTableModule, { static: true }) myTable: MatTableModule
    returnUrl: string
    currentFinanceInfo: object = {
        cashInAccount: 0,
        validCashForBuying: 0
    }
    matchedRSList: ReturnedAMatched[]
    displayedColumns:string[] = [
        'matchedTime',
        'matchedPrice',
        'matchedAmount',
        'propertyReturnDate'
    ]
    totalItems: number = 0
    pageSize: number = 10
    skip: number = 0
    // MatPaginator Output
    pageEvent: PageEvent
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userLocalService: UserLocalService,
        private spinnerService: SpinnerService,
        private netHandlerService: NetHandlerService,
        private stockOrderDetailService: StockOrderDetailService,
        private changeDetectorRefs: ChangeDetectorRef,
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate)
        // redirect to home if already logged in
        if (!this.userLocalService.currentUser) {
            this.router.navigate(['/af/admin/login'])
        }
    }

    ngOnInit() {
        super.ngOnInit()
        if (!Number.isNaN(this.targetStockOrder)){
            this.getMatchedHistory()
        }
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() { }

    getMatchedHistory() {
        let spinnerRef = this.spinnerService.start()
        this.stockOrderDetailService
            .getMatchedResults({
                '$select[]': [
                    '_id',
                    'match_date_time',
                    'property_return_date',
                    'match_price',
                    'match_amount',
                ],
                $limit: 1000,
                $skip: 0,
                _stock_order_id: this.targetStockOrder,
                '$sort[match_date_time]': -1
            })
            .then((oRs: ReturnedMatchedList) => {
                this.matchedRSList = oRs.data
                this.changeDetectorRefs.detectChanges()
            })
            .catch((oError) => {
                this.netHandlerService.handleError(oError)
            })
            .finally(() => {
                this.spinnerService.stop(spinnerRef)
            })
    }
}
