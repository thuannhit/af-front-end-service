import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatchResutHistoryComponent } from '@/modules/af/shared-modules/match-result-history/match-result-history.component'
import {
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule
} from '@angular/material'
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/match-result-history/', '.json');
}
@NgModule({
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        MatDatepickerModule,
        MatNativeDateModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        MatTableModule,
        MatToolbarModule,
        MatPaginatorModule,
        HttpClientModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            },
            isolate: false
        })
    ],
    exports: [
        CommonModule,
        FormsModule,
        MatchResutHistoryComponent,
        MatDatepickerModule,
        HttpClientModule
    ],
    declarations: [MatchResutHistoryComponent],
    entryComponents: [],
    providers: [],
    bootstrap: [MatchResutHistoryComponent]
})
export class MatchResutHistoryModule { }
