import { NgModule } from '@angular/core'
import { MatInputCommifiedDirective } from './_directive/mat-input-commified.directive'

@NgModule({
    declarations: [MatInputCommifiedDirective],
    imports: [],
    exports: [MatInputCommifiedDirective]
})
export class ngTHMatInputCommifiedModule { }
