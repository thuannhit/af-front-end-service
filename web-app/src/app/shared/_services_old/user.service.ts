﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AdminUser } from '@/shared/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<AdminUser[]>(`${config.apiUrl}/users`);
    }

    register(user: AdminUser) {
        return this.http.post(`${config.apiUrl}/users/register`, user);
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/users/${id}`);
    }
}