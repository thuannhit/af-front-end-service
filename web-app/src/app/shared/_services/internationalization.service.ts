import { Injectable, PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class InternationalizationService {
    //------set the default to english--------
    public language = new BehaviorSubject('en');
    translateObject: any = {};
    
    constructor(private http: HttpClient) { }
    
    use(lang: string = 'en', translationsUrl: string): Promise<{}> {
        return new Promise<{}>((resolve, reject) => {
            const langPath = `${translationsUrl}/${lang || 'en'}.json`;
            this.http.get<{}>(langPath).subscribe(
                translation => {
                    this.translateObject = Object.assign({}, translation || {});
                    resolve(this.translateObject);
                },
                error => {
                    // this.translateObject = {};
                    resolve(this.translateObject);
                }
            );
        });
    }
}
