import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { DataProcessor } from '@/shared/_helpers';
import { User } from '@/shared/_models';
import { API_CONSTANT } from '@/shared/_common';
import { map } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class UserService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private dataProcessor = new DataProcessor();
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }
    adminLogin(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_LOGIN,
            data: oFormData
        },
            bAsync = true;
            // user: User;
        this.dataProcessor.UserLoginProcessing(oInputData, fnSuccess, fnError, bAsync).then(<User>(user) => {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
        }, (oErr) => {
        });
    }
    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    //     register(user: User) {
    //         return this.http.post(`${config.apiUrl}/users/register`, user);
    //     }

    //     delete(id: number) {
    //         return this.http.delete(`${config.apiUrl}/users/${id}`);
    //     }
}