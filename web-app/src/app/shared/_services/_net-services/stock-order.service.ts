import { Injectable } from '@angular/core';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({ providedIn: 'root' })
export class StockOrderService {
    private dataProcessor = new DataProcessor();
    constructor(private userLocalService: UserLocalService) {
    }
    addNewOrder(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCK_ORDER_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getStockOrders(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCK_ORDER_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    changeStockOrderStatus(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCK_ORDER__STATUS_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }


}