import { Injectable } from '@angular/core'

import { DataProcessor } from '@/shared/_helpers'
import { API_CONSTANT } from '@/shared/_common'
import { UserLocalService } from '@/shared/_services/_local-services'
@Injectable({ providedIn: 'root' })
export class DepositNetServices {
    private dataProcessor = new DataProcessor()
    constructor(private userLocalService: UserLocalService) { }

    getCurrentCustomers(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CUSTOMERS_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

    removeCustomer(_id: number) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CUSTOMERS_SVC.concat('/').concat(
                _id.toString()
            ),
            data: {},
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithDELETEMethod(
            oInputData,
            bAsync
        )
    }

    addNewCustomer(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CUSTOMERS_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    
    updateCustomer(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CUSTOMERS_SVC.concat('/').concat(
                _id.toString()
                ),
                data: oFormData,
                token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
            },
            bAsync = true
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    
    addNewDepositInterestConfig(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_INTEREST_CONFIG_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }

    updateCurrentInterestConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_INTEREST_CONFIG_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
        bAsync = true
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    
    getCurrentInterestConfig(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_INTEREST_CONFIG_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

    addNewDeposit(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CRUD_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }

    updateCurrentDeposit(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CRUD_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
        bAsync = true
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    
    getCurrentDeposits(oFormData) {
        let oInputData = {
            url: API_CONSTANT.DEPOSIT_CRUD_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
}
