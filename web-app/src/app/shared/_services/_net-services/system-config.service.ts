import { Injectable } from '@angular/core';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({providedIn:'root'})
export class SystemConfigService {
    private dataProcessor = new DataProcessor();
    constructor(private userLocalService: UserLocalService) {
    }
    addNewStock(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCKS_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }

    getEscrowConfig(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCKS_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    removeEscrowConfig(_id: number) {
        let oInputData = {
            url: API_CONSTANT.STOCKS_SVC.concat('/').concat(_id.toString()),
            data: {},
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithDELETEMethod(oInputData, bAsync)
    }
    updateEscrowConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.STOCKS_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    addNewInterest(oFormData) {
        let oInputData = {
            url: API_CONSTANT.INTEREST_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getInterests(oFormData) {
        let oInputData = {
            url: API_CONSTANT.INTEREST_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    updateInterestConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.INTEREST_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    addNewTax(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TAX_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getTaxs(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TAX_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    updateTaxConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.TAX_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
            return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }

    getTransCost(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TRANS_COST_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    addNewTransCost(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TRANS_COST_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    updateTransactionCostConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.TRANS_COST_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    
    getInfoFromTradingSVC(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TRADING_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

    getBanks(oFormData) {
        let oInputData = {
            url: API_CONSTANT.BANK_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    addNewBank(oFormData) {
        let oInputData = {
            url: API_CONSTANT.BANK_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    updateBankConfig(oFormData: any, _id: number) {
        let oInputData = {
            url: API_CONSTANT.BANK_SVC.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }
    deleteBankItem(_id: number) {
        let oInputData = {
            url: API_CONSTANT.BANK_SVC.concat('/').concat(_id.toString()),
            data: {},
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithDELETEMethod(oInputData, bAsync)
    }
}