import { Injectable } from '@angular/core';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({ providedIn: 'root' })
export class StockOrderDetailService {
    private dataProcessor = new DataProcessor();
    constructor(private userLocalService: UserLocalService) {
    }
    addNewMatched(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCK_ORDER_DETAIL_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getMatchedResults(oFormData) {
        let oInputData = {
            url: API_CONSTANT.STOCK_ORDER_DETAIL_SVC,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

}