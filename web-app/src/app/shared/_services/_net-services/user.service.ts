import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { DataProcessor } from '@/shared/_helpers';
import { AdminUser, TradingUser } from '@/shared/_models';
import { API_CONSTANT } from '@/shared/_common';
import { map } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class UserService {
    private currentAdminUserSubject: BehaviorSubject<AdminUser>;
    private currentTradingUserSubject: BehaviorSubject<TradingUser>;
    public currentAdminUser: Observable<AdminUser>;
    public currentTradingUser: Observable<TradingUser>;
    private dataProcessor = new DataProcessor();
    constructor(private http: HttpClient) {
        this.currentAdminUserSubject = new BehaviorSubject<AdminUser>(JSON.parse(localStorage.getItem('currentAdminUser')));
        this.currentTradingUserSubject = new BehaviorSubject<TradingUser>(JSON.parse(localStorage.getItem('currentTradingUser')));
        this.currentAdminUser = this.currentAdminUserSubject.asObservable();
        this.currentTradingUser = this.currentTradingUserSubject.asObservable();
    }
    public get currentAdminUserValue(): AdminUser {
        return this.currentAdminUserSubject.value;
    }
    public get currentTradingUserValue(): TradingUser {
        return this.currentTradingUserSubject.value;
    }
    adminLogin(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_LOGIN,
            data: oFormData
        },
            bAsync = true;
        // user: User;
        this.dataProcessor.UserLoginProcessing(oInputData, fnSuccess, fnError, bAsync).then(<AdminUser>(user) => {
            debugger;
            localStorage.setItem('currentAdminUser', JSON.stringify(user));
            this.currentAdminUserSubject.next(user);
        }, (oErr) => {
            console.log(oErr)
        });
    }
    tradingUserLogin(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_LOGIN,
            data: oFormData
        },
            bAsync = true;
        // user: User;
        this.dataProcessor.UserLoginProcessing(oInputData, fnSuccess, fnError, bAsync).then(<TradingUser>(user) => {
            localStorage.setItem('currentTradingUser', JSON.stringify(user));
            this.currentTradingUserSubject.next(user);
        }, (oErr) => {
            console.log(oErr)
        });
    }
    getAllValidUsers(oFormData, fnSuccess, fnError){
        let oInputData = {
            url: API_CONSTANT.USERS,
            data: oFormData,
            token: `Bearer ${this.currentAdminUserSubject.value.accessToken}` 
        },
            bAsync = true;
        // user: User;
        this.dataProcessor.processingWithGETMethod(oInputData, fnSuccess, fnError, bAsync).then(<TradingUser>(users) => {
        }, (oErr) => {
            console.log(oErr)
        });
    }
    getUser(oFormData, fnSuccess, fnError){

    }
    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentAdminUser');
        localStorage.removeItem('currentTradingUser');
        this.currentAdminUserSubject.next(null);
        this.currentTradingUserSubject.next(null);
    }
}