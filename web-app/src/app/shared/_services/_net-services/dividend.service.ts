import { Injectable } from '@angular/core';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({ providedIn: 'root' })
export class DividendService {
    private dataProcessor = new DataProcessor();
    constructor(private userLocalService: UserLocalService) {
    }
    addNewSchedule(oFormData) {
        let oInputData = {
            url: API_CONSTANT.ADD_DIVIDEND_SCHEDULE,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getDividendSchedules(oFormData) {
        let oInputData = {
            url: API_CONSTANT.GET_DIVIDEND_SCHEDULES,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

    applyASchedule(oFormData: any, _id: number) {
        console.log(oFormData)
        let oInputData = {
            url: API_CONSTANT.APPLY_DIVIDEND_SCHEDULE.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }

    doneASchedule(oFormData: any, _id: number) {
        console.log(oFormData)
        let oInputData = {
            url: API_CONSTANT.DONE_DIVIDEND_SCHEDULE.concat('/').concat(_id.toString()),
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPUTMethod(oInputData, bAsync)
    }

}