import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({ providedIn: 'root' })
export class FinanceService {
    private dataProcessor = new DataProcessor();
    constructor(private http: HttpClient, private userLocalService: UserLocalService) {
    }
    topup(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TOPUP,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}` 
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    withdraw(oFormData) {
        let oInputData = {
            url: API_CONSTANT.WITHDRAW,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}` 
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getFinancialStatements(oFormData) {
        let oInputData = {
            url: API_CONSTANT.GET_FINANCIAL_STATEMENTS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}` 
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    getBalance(oFormData) {
        let oInputData = {
            url: API_CONSTANT.GET_BALANCE,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}` 
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
}