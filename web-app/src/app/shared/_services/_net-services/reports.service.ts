import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
@Injectable({ providedIn: 'root' })
export class ReportServices {
    private dataProcessor = new DataProcessor();
    constructor(private http: HttpClient, private userLocalService: UserLocalService) {
    }
    getLoansOfUser(oFormData) {
        let oInputData = {
            url: API_CONSTANT.REPORT_LOANS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    getStockPriceFromBackend(oFormData) {
        let oInputData = {
            url: API_CONSTANT.REPORT_LOANS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    analyzeEscrowsForAllCustomer(oFormData) {
        let oInputData = {
            url: API_CONSTANT.REPORT_ESCROW,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    interestDeductionFromCash(oFormData) {
        let oInputData = {
            url: API_CONSTANT.INTEREST_DEDUCTION_FROM_CASH,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    interestDeductionFromBalance(oFormData) {
        let oInputData = {
            url: API_CONSTANT.INTEREST_DEDUCTION_FROM_BALANCE,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    getAlerts(oFormData) {
        let oInputData = {
            url: API_CONSTANT.REPORT_ESCROW,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

    getStocksLoan(oFormData) {
        let oInputData = {
            url: API_CONSTANT.REPORT_LOANS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }

}