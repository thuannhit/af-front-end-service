import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services'
@Injectable({ providedIn: 'root' })
export class UserNetService {

    private dataProcessor = new DataProcessor();
    constructor(private userLocalService: UserLocalService) {
    }
    adminLogin(oFormData) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_LOGIN,
            data: oFormData
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    tradingLogin(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TRADING_USER_LOGIN,
            data: oFormData
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    registerAdmin(oFormData) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_REGISTER,
            data: oFormData
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    registerTrading(oFormData) {
        let oInputData = {
            url: API_CONSTANT.TRADING_USER_REGISTER,
            data: oFormData
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    adminCreateUser(oFormData) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_CREATE_USER,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithPOSTMethod(oInputData, bAsync)
    }
    tradingUserLogin(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ADMIN_LOGIN,
            data: oFormData
        },
            bAsync = true;
        // user: User;
        this.dataProcessor.UserLoginProcessing(oInputData, fnSuccess, fnError, bAsync).then(<GeneralUser>(user) => {
            this.userLocalService.storeCurentUser(user)
        }, (oErr) => {
            console.log(oErr)
        });
    }
    getAllValidUsers(oFormData) {
        let oInputData = {
            url: API_CONSTANT.USERS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        // user: User;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    getUserUnderManagementBy(oFormData) {
        let oInputData = {
            url: API_CONSTANT.USER_GROUP_MANAGEMENT,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
    getUserInformation(oFormData) {
        let oInputData = {
            url: API_CONSTANT.USERS,
            data: oFormData,
            token: `Bearer ${this.userLocalService.currentUserValue.accessToken}`
        },
            bAsync = true;
        // user: User;
        return this.dataProcessor.modernProcessingWithGETMethod(oInputData, bAsync)
    }
}