import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { DataProcessor } from '@/shared/_helpers';
import { IC_Product } from '@/shared/_models';
import { API_CONSTANT } from '@/shared/_common';
import {UserService} from '@/shared/_services';

@Injectable({ providedIn: 'root' })
export class ICProductService {
    // private currentUserSubject: BehaviorSubject<IC_Product>;
    // public currentUser: Observable<IC_Product>;
    private dataProcessor = new DataProcessor();
    constructor(private http: HttpClient, private userService: UserService) {
    }
    createICProduct(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.CREATE_IC_PRODUCT,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.ICCreationProcessing(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    getICProductList(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.GET_ALL_IC_PRODUCT,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.getICProductList(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    activateICProducts(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ACTIVATE_IC_PRODUCT,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    deactivateICProducts(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.DEACTIVATE_IC_PRODUCT,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    addICInterestOption(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.ADD_IC_INTEREST_OPTION,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    getICInterestList(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.GET_IC_INTEREST_OPTIONS,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithGETMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    removeICInterestOptions(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.DELETE_IC_INTEREST_OPTIONS,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    updateICInterestOptions(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.UPDATE_IC_INTEREST_OPTIONS,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    publishIC(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.PUBLISH_IC,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithPOSTMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
    getPublishedICsList(oFormData, fnSuccess, fnError) {
        let oInputData = {
            url: API_CONSTANT.GET_PUBLISHED_ICS_LIST,
            data: oFormData,
            token: `Bearer ${this.userService.currentUserValue.token}` 
        },
            bAsync = true;
        this.dataProcessor.processingWithGETMethod(oInputData, fnSuccess, fnError, bAsync).then(oRs=>{
        }, oError=>{
        });
    }
}