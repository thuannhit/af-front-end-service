import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { MessageDialogComponent } from '@/core/message-dialog/message-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import {DialogOption} from '@/shared/_models'
@Injectable({providedIn:'root'})
export class MessageDialogService {
    constructor(private dialog: MatDialog, private snackBar: MatSnackBar) { }
    private dialogRef: MatDialogRef<MessageDialogComponent>;

    public openDialog(options: DialogOption) {
        let receivedData: DialogOption = options
        this.dialogRef = this.dialog.open(MessageDialogComponent, {
            data: receivedData,
            minWidth: "450px"
        });
        return this.dialogRef
    }
    public confirmed(): Observable<any> {

        return this.dialogRef.afterClosed().pipe(take(1), map(res => {
            return res;
        }
        ));
    }
    public openMessageToast(message, action) {
        this.snackBar.open(message, action, {
            duration: 10000,
        });
    }

    public closeDialog(){
        this.dialogRef.close()
    }
}