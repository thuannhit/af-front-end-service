import { Injectable } from '@angular/core';
import { MessageDialogService } from '@/shared/_services/_local-services';
import { MessageType } from '@/shared/_common/'
import { DialogOption } from '@/shared/_models/'
import { Router, ActivatedRoute,  } from '@angular/router'
@Injectable({
    providedIn: 'root'
})
export class NetHandlerService {
    constructor(
        private messageService: MessageDialogService,
        private route: ActivatedRoute,
        private router: Router,
    ) {

    }

    public handleError(oError) {
        console.log(oError)
        // To see more on how to use this dialog:
        // https://itnext.io/building-a-reusable-dialog-module-with-angular-material-4ce406117918
        if (oError && oError.name == 'NotAuthenticated') {
            this.notAuthorizedError(oError)
        } else {
            this.commonError(oError)
        }

    }
    public handleSuccess(message, action) {
        this.messageService.openMessageToast(message, action)
    }

    private notAuthorizedError(oError) {
        let returnUrl = this.router.url || '/'
        let oOption: DialogOption = {
            type: MessageType.ERROR,
            title: 'Session timeout',
            message: 'Please try to sign-in again!',
            confirmText: 'Login',
            OKAction: () => {
                this.router.navigate(['/af/admin/login'], { queryParams: { returnUrl: returnUrl } })
            }
        }
        this.messageService.openDialog(
            oOption
        )
    }

    private commonError(oError) {
        this.messageService.openDialog({
            type: MessageType.ERROR,
            title: oError.name,
            message: oError.message,
            // cancelText: 'Cancel',
            confirmText: 'OK'
        })
    }
}