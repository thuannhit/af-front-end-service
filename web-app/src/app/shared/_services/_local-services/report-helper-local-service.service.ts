import { Injectable } from '@angular/core'
import {
    DayTimeHelperSerivice
} from '@/shared/_services/_local-services'
import { SystemConfigService } from '@/shared/_services/_net-services/'
import { HttpClient } from '@angular/common/http';
import axios from 'axios';
import { RequestPromise } from 'request-promise';
import { DataProcessor } from '@/shared/_helpers';
import { API_CONSTANT, FinancialServiceCMD } from '@/shared/_common';
import { UserLocalService } from '@/shared/_services/_local-services';
import moment from 'moment'
export interface InputLoan {
    buyPrice: number
    quantity: number
    sellPrice: number
    transaction_value: number
    buy_transaction_cost: number
    start_date: number
    interest: number
}

@Injectable({
    providedIn: 'root'
})
export class ReportHelperLocalService {
    constructor(
        // TODO: Check why add this line, it failed to load page
        // private stockPriceLocalService: StockPriceLocalService,
        private dateTimeHelperService: DayTimeHelperSerivice,
        // private dataProcessor = new DataProcessor(),
        private userLocalService: UserLocalService,
        private systemConfigService: SystemConfigService
    ) { }

    public estimateLoansProfit(loan: InputLoan) {
        let buy_cost = loan.transaction_value * 0.15 / 100
        let loans_amount = loan.transaction_value + buy_cost

        let quantity = loan.quantity
        let currentStockPrice = loan.sellPrice

        let estimatedPropertyReturnDate = this.dateTimeHelperService.getExpectedPropertyReturnDayForSell(
            new Date().getTime()
        )
        let advanceCostDay = moment(estimatedPropertyReturnDate).diff(
            moment(new Date().getTime()),
            'days'
        )
        let buyAmount = loans_amount
        let sellAmount = currentStockPrice * quantity
        let sellCost = sellAmount * 0.15 / 100
        let tax = sellAmount * 0.1 / 100
        let advanceCost = (sellAmount * 0.045 / 100) * advanceCostDay
        let interestAmount = loans_amount * 16.2 / 100 / 360 * moment(new Date().getTime()).diff(
            moment(loan.start_date),
            'days'
        )

        // await this.systemConfigService.getInfoFromTradingSVC({
        //     cmd: FinancialServiceCMD.GET_CURRENT_INTEREST_IN_TIMERANGE,
        //     fromDate: loan.start_date,
        //     toDate: new Date().getTime()
        // }).then(oRs => {
        //     debugger;
        // }).catch(error => {
        //     debugger;

        // })
        let estimatedSellValue =
            sellAmount - buyAmount - sellCost - advanceCost - tax - interestAmount

        return +estimatedSellValue.toFixed(2)
    }


    public getEstimatedNETSellValue(loan: InputLoan) : number{
        let buy_cost = loan.transaction_value * 0.15 / 100
        let loans_amount = loan.transaction_value + buy_cost

        let quantity = loan.quantity
        let currentStockPrice = loan.sellPrice

        let estimatedPropertyReturnDate = this.dateTimeHelperService.getExpectedPropertyReturnDayForSell(
            new Date().getTime()
        )
        let advanceCostDay = moment(estimatedPropertyReturnDate).diff(
            moment(new Date().getTime()),
            'days'
        )
        let sellAmount = currentStockPrice * quantity
        let sellCost = sellAmount * 0.15 / 100
        let tax = sellAmount * 0.1 / 100
        let advanceCost = (sellAmount * 0.045 / 100) * advanceCostDay
        let interestAmount = loans_amount * 16.2 / 100 / 360 * moment(new Date().getTime()).diff(
            moment(loan.start_date),
            'days'
        )

        let estimatedSellValue =
            sellAmount - sellCost - advanceCost - tax - interestAmount

        return estimatedSellValue
    }
    private async getNowPriceOfStock(stock: string): Promise<number> {
        var config = {
            // 'Access-Control-Allow-Origin': '*',
            mode: "no-cors"
        };
        let dataRs
        // fetch('https://www.fireant.vn/api/Data/Markets/Quotes?symbols=' + stock, { mode: 'no-cors' })
        // fetch('https://www.fireant.vn/Home/StockDetail/' + stock, {mode: 'no-cors'})
        //     .then(oRs=>{
        //         debugger;
        //         console.log(oRs)
        //     })

        const response = await fetch('https://www.fireant.vn/api/Data/Markets/Quotes?symbols=' + stock, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'no-cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
        });
        debugger
        return dataRs
        // return addBusinessDays(inputDay, 3).getTime()
    }

}
