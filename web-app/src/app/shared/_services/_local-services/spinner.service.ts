import { EventEmitter, Injectable } from '@angular/core'
import { Event, NavigationEnd, Router } from '@angular/router'
import { MatDialog, MatDialogRef } from '@angular/material'
import { BusyDialogComponent } from '@/core/busy-dialog'

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    constructor(private router: Router, private dialog: MatDialog) { }

    start(message?): MatDialogRef<BusyDialogComponent> {
        const dialogRef = this.dialog.open(BusyDialogComponent, {
            disableClose: true,
            panelClass: 'transparent',
            data: message == '' || message == undefined ? 'Loading...' : message
        })
        return dialogRef
    }

    stop(ref: MatDialogRef<BusyDialogComponent>) {
        ref.close()
    }
}
