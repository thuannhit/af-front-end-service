import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { GeneralUser } from '@/shared/_models';
@Injectable({ providedIn: 'root' })
export class UserLocalService {
    private currentUserSubject: BehaviorSubject<GeneralUser>;
    public currentUser: Observable<GeneralUser>;
    constructor() {
        this.currentUserSubject = new BehaviorSubject<GeneralUser>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    public get currentUserValue(): GeneralUser {
        return this.currentUserSubject.value;
    }
    storeCurentUser(user: GeneralUser) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }
    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}