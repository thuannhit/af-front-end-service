import { Injectable } from '@angular/core';
import { MessageDialogService } from '@/shared/_services/_local-services';
import { MessageType } from '@/shared/_common/'
import moment from 'moment'
const addWorkdays = function (days) {
    var increment = days / Math.abs(days);
    var date = this.clone().add(Math.floor(Math.abs(days) / 5) * 7 * increment, 'days');
    var remaining = days % 5;
    while (remaining != 0) {
        date.add(increment, 'days');
        if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7)
            remaining -= increment;
    }
    return date;
};
const addBusinessDays = function (d, n) {
    d = new Date(d);
    var day = d.getDay();
    d.setDate(d.getDate() + n + (day === 6 ? 2 : +!day) + (Math.floor((n - 1 + (day % 6 || 1)) / 5) * 2));
    return d;
}
@Injectable({
    providedIn: 'root'
})
export class DayTimeHelperSerivice {
    constructor() { }

    public getExpectedPropertyReturnDayForBuy(inputDay: number): number {
        return addBusinessDays(inputDay, 3).getTime()
    }
    public getExpectedPropertyReturnDayForSell(inputDay: number): number {
        return addBusinessDays(inputDay, 2).getTime()
    }
    public addBudinessDays(inputDay: number, numberOfBusinessDay: number): number {
        return addBusinessDays(inputDay, numberOfBusinessDay).getTime()
    }
    public howManyWorkingDays(startDate: number, endDate: number): number {
        let start = moment(startDate).utc().add(moment(startDate).utcOffset(), 'm'); // Ignore timezones
        let end = moment(endDate).utc().add(moment(startDate).utcOffset(), 'm'); // Ignore timezones

        var first = start.clone().endOf('week'); // end of first week
        var last = end.clone().startOf('week'); // start of last week

        // Fixing Summer Time problems
        let firstCorrection = moment(first).utc().add(60, 'm').toDate(); //
        var days = last.diff(firstCorrection, 'days') * 5 / 7; // this will always multiply of 7

        var wfirst = first.day() - start.day(); // check first week
        if (start.day() == 0) --wfirst; // -1 if start with sunday
        var wlast = end.day() - last.day(); // check last week
        if (end.day() == 6) --wlast; // -1 if end with saturday
        return wfirst + days + wlast - 1; // get the total (subtract holidays if needed)
    }
    public nextMonday(inputDay: number): number {
        let inputDate = new Date(inputDay)
        return inputDate.setDate(inputDate.getDate() + (1 + 7 - inputDate.getDay()) % 7)
    }
    public isSunday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 6) ? true : false
    }
    public isSaturday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 0) ? true : false
    }
    public isMonday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 1) ? true : false
    }
    public isTuesday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 2) ? true : false
    }
    public isWednesday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 3) ? true : false
    }
    public isThursday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 4) ? true : false
    }
    public isFriday(inputDay: number): boolean {
        let inputDate = new Date(inputDay)
        return (inputDate.getDay() == 6) ? true : false
    }
}
