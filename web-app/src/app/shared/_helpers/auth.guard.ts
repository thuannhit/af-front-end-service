﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UserLocalService } from '@/shared/_services/_local-services';
import { UserRole } from '@/shared/_common/';

@Injectable({ providedIn: 'root' })
export class AFAdminAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userLocalService: UserLocalService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.userLocalService.currentUserValue;
        if (currentUser?.user?._role_id === UserRole.ADMIN || currentUser?.user?._role_id === UserRole.SUPER_USER || currentUser?.user?._role_id === UserRole.ADVANCED_ADMIN) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/af/admin/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
@Injectable({ providedIn: 'root' })
export class AFTradingAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userLocalService: UserLocalService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.userLocalService.currentUserValue;
        if (currentUser?.user?._role_id === UserRole.TRADING_USER || currentUser?.user?._role_id === UserRole.MG_USER) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/af/trading/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
@Injectable({ providedIn: 'root' })
export class AFAuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userLocalService: UserLocalService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.userLocalService.currentUser;
        if (currentUser) {
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/af/trading/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private userLocalService: UserLocalService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.userLocalService.currentUser;
        if (currentUser) {
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/af/trading/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}