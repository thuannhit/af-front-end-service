import { AJAX } from '@/shared/_helpers'
export class DataProcessor {
    private ajax = new AJAX()
    constructor() { }
    private baseURI = '/'
    public UserLoginProcessing(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        inputData.data.strategy = 'local'
        let oRequest = {
            url: serviceUrl,
            type: 'POST',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }
    public ICCreationProcessing(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'POST',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }
    public getICProductList(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'GET',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }
    public activateICProduct(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'POST',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }

    public processingWithGETMethod(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'GET',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }
    public modernProcessingWithGETMethod(inputData, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'GET',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.modernCall(oRequest, bAsync)
    }
    public processingWithPOSTMethod(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'POST',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync)
    }
    public modernProcessingWithPOSTMethod(inputData, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'POST',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.modernCall(oRequest, bAsync)
    }
    public modernProcessingWithDELETEMethod(inputData, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'DELETE',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.modernCall(oRequest, bAsync)
    }
    public modernProcessingWithPUTMethod(inputData, bAsync) {
        let serviceUrl = this.baseURI + inputData.url
        let oRequest = {
            url: serviceUrl,
            type: 'PUT',
            data: inputData.data,
            headers: {
                'content-type': 'application/json',
                Authorization: inputData.token,
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'X-Requested-With'
            }
        }
        return this.ajax.modernCall(oRequest, bAsync)
    }
}
