export interface Actions {
    actionText: string
    actionFn: Function
}
export interface DialogOption {
    title: string
    type: number
    message: string
    cancelText?: string
    confirmText?: string
    OKAction?: Function
    CANCELAction?: Function
    Actions?: Actions[]
}

