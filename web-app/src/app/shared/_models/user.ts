﻿export interface AuthenStrategy {
    strategy: string
}
export interface AdminInfo {
    _id: number,
    email: string,
    _role_id: number
}
export class TradingUser {
    id: number;
    username: string;
    firstName: string;
    lastName: string;
    token: string;
}
export class AdminUser {
    accessToken: string;
    authentication: AuthenStrategy;
    user: AdminInfo;
}
export class GeneralUser {
    accessToken: string;
    authentication: AuthenStrategy;
    user: AdminInfo;
}