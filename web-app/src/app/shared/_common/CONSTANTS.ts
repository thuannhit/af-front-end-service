export const API_CONSTANT = {
    ADMIN_LOGIN: 'sign-in',
    TRADING_USER_LOGIN: 'sign-in',
    ADMIN_REGISTER: 'users',
    TRADING_USER_REGISTER: 'users',
    USERS: 'users',
    ADMIN_CREATE_USER: 'admin/users',
    TRADING_REGISTER: 'users',
    TOPUP: 'trading/financial-statements',
    WITHDRAW: 'trading/financial-statements',
    GET_FINANCIAL_STATEMENTS: 'trading/financial-statements',
    STOCK_ORDER_SVC: 'trading/stock-orders',
    STOCK_ORDER__STATUS_SVC: 'trading/stock-orders/status',
    UPDATE_STOCK_ORDER_STATUS: 'trading/stock-orders/status',
    STOCK_ORDER_DETAIL_SVC: 'trading/stock-order-details',
    STOCKS_SVC: 'system/stocks',
    BANK_SVC: 'system/banks',
    INTEREST_SVC: 'system/preferences',
    TAX_SVC: 'system/preferences',
    TRANS_COST_SVC: 'system/preferences',
    TRADING_SVC: 'trading/services',
    GET_BALANCE: 'trading/services',
    ADD_DIVIDEND_SCHEDULE: 'trading/dividends/schedule',
    GET_DIVIDEND_SCHEDULES: 'trading/dividends/schedule',
    APPLY_DIVIDEND_SCHEDULE: 'trading/dividends/status',
    DONE_DIVIDEND_SCHEDULE: 'trading/dividends/status',
    REPORT_LOANS: 'reports/loans',
    REPORT_ESCROW: 'reports/escrows-all',
    INTEREST_DEDUCTION_FROM_BALANCE: 'reports/loans',
    INTEREST_DEDUCTION_FROM_CASH: 'reports/loans',
    USER_GROUP_MANAGEMENT: 'reports/mg_management',
    DEPOSIT_CUSTOMERS_SVC: 'deposit/customers',
    DEPOSIT_INTEREST_CONFIG_SVC: 'deposit/interest_configs',
    // DEPOSIT_REPORTS_SVC: 'deposit/reports',
    DEPOSIT_CRUD_SVC: 'deposit/deposits_crud',
    DEPOSIT_INTEREST_SETTLEMENT_SVC: 'deposit/settlement',
    DEPOSIT_SETTLEMENT_SVC: 'deposit/settlement',


    PUBLISH_IC: 'admin/ic/publish_ic',
    GET_PUBLISHED_ICS_LIST: 'admin/ic/get_published_ics_list',
    CREATE_IC_PRODUCT: 'admin/ic/create_ic_product',
    GET_ALL_IC_PRODUCT: 'admin/ic/get_all_ic_product',
    ACTIVATE_IC_PRODUCT: 'admin/ic/activate_ic_product',
    DEACTIVATE_IC_PRODUCT: 'admin/ic/deactivate_ic_product',
    ADD_IC_INTEREST_OPTION: 'admin/ic/add_ic_interest_option',
    GET_IC_INTEREST_OPTIONS: 'admin/ic/get_ic_interest_options',
    DELETE_IC_INTEREST_OPTIONS: 'admin/ic/delete_ic_interest_options',
    UPDATE_IC_INTEREST_OPTIONS: 'admin/ic/update_ic_interest_options'
}

export const GeneralStatus = Object.freeze({
    INACTIVE: 0,
    ACTIVE: 1
})


export const I18N_PATH_CONSTANT = {}

export const UserRole = Object.freeze({
    TRADING_USER: 1,
    MG_USER: 2,
    ADMIN: 3,
    ADVANCED_ADMIN: 4,
    SUPER_USER: 5,
})

export const FinancialStatementType = Object.freeze({
    UNKNOWN: 0,
    UP: 1,
    DOWN: 2
})
export const MessageType = Object.freeze({
    WARNING: 0,
    ERROR: 1,
    SUCCESS: 2,
    INFORMATION: 3,
    ACTIONS: 4
})
export const PreferenceType = Object.freeze({
    BASE_INTEREST: 1,
    TRANSACTION_COST: 2,
    TRANSACTION_TAX: 3,
})
export const StockStatus = Object.freeze({
    ACTIVE: GeneralStatus.ACTIVE,
    INACTIVE: GeneralStatus.INACTIVE,
})

export const FinancialServiceCMD = Object.freeze({
    GET_USER_BALANCE: 'get_balance',
    GET_CURRENT_INTEREST: 'get_current_interest_rate',
    GET_CURRENT_TRANSACTION_COST_RATE: 'get_current_transaction_cost_rate',
    GET_CURRENT_TRANSACTION_TAX_RATE: 'get_current_transaction_tax_rate',
    GET_VALID_STOCK_FOR_SELLING: 'get_available_stock',
    GET_STOCK_LIST_OF_USER: 'get_stock_list_of_user',
    GET_AFFORDABLE_WITHDRAW: 'get_affordable_withdraw',
    GET_CURRENT_INTEREST_VALUE_FOR_ONGOING_LOANS: 'get_current_interest_value_of_user_for_ongoing_loans',
    GET_CURRENT_INTEREST_VALUE_FOR_DONE_LOANS: 'get_current_interest_value_of_user_for_done_loans',
    GET_CURRENT_ALL_USERS_INTEREST_VALUE_FOR_ONGOING_LOANS: 'get_current_interest_value_of_all_users_for_ongoing_loans',
    GET_CURRENT_ALL_USERS_INTEREST_VALUE_FOR_DONE_LOANS: 'get_current_interest_value_of_all_users_for_done_loans',
    GET_CURRENT_INTEREST_IN_TIMERANGE: 'get_current_interest_rate_in_timerange',
})

export const ReportServiceCMD = Object.freeze({
    GET_LOANS_OF_USER: 'get_loans_of_user',
    GET_ENDED_LOANS_NOT_SETTLEMENT_INTEREST_OF_USER: 'get_ended_loans_not_settlement_interest_of_user',
    ANALYZE_ESCROWS_FOR_ALL: 'analyze_escrows_for_all',
    ANALYZE_ESCROWS_FOR_ONE: 'analyze_escrows_for_one',
    GET_STOCKS_LOANS_REPORT: 'get_stocks_loan_report'
})


export const UserStatus = Object.freeze({
    INACTIVE: 0,
    ACTIVE: 1
})

/** Stock Orders */
export const StockOrderType = Object.freeze({
    BUY: 1,
    SELL: 2
})

export const StockOrderTradingType = Object.freeze({
    UNKNOW: 0,
    LO: 1,
    ATC: 2,
    ATO: 3
})

export const StockOrderStatus = Object.freeze({
    INACTIVE: GeneralStatus.INACTIVE,
    NEW: 1,
    APPROVED: 2,
    IN_PROGRESS: 3,
    DONE: 4,
    REJECTED: 5
})

export const StockOrderDetailStatus = Object.freeze({
    COMPLETE: 1,
    PART: 2
})

export const INTEREST_TYPE = Object.freeze({
    UNKNOW: 0,
    FLOATING_INTEREST: 1,
    FIXED_INTEREST: 2
})

export const COST_AND_TAX_FREE = Object.freeze({
    TAX_FREE: 0,
    TAX_NOT_FREE: 1,
    TRANS_COST_FREE: 0,
    TRANS_COST_NOT_FREE: 1
})

/** Dividend Status */
export const DividendStatus = Object.freeze({
    NEW: 1,
    APPLY: 2,
    DONE: 3
})

/** Interest settlement method */
export const InterestSettlementMethod = Object.freeze({
    UN_DEFINED: 0,
    ON_SELLING: 1,
    AT_THE_END_OF_MONTH: 2
})

/** Deposit customers type */
export const DepositCustomerStatus = Object.freeze({
    INACTIVE: 0,
    ACTIVE: 1
})

/** Interest config status for Deposit */
export const DepositInterestConfigStatus = Object.freeze({
    INACTIVE: 0,
    ACTIVE: 1
})

/** Deposit Interest payment method  */
export const DepositInterestPaymentMethod = Object.freeze({
    UNDEFINED: 0,
    IN_THE_EARLY_OF_QUATER: 1,
    IN_THE_EARLY_OF_MONTH: 2,
    EVERY_3_MONTHS: 3,
    EVERY_MONTH: 4,
    COMPOUND_INTEREST: 5
})
