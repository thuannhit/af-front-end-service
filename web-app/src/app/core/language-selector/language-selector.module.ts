import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Observable } from "rxjs";
// used to create fake backend
import { fakeBackendProvider } from '@/shared/_helpers';
import { JwtInterceptor, ErrorInterceptor } from '@/shared/_helpers';
import { LanguageSelectorComponent } from './language-selector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { TranslateService } from '@ngx-translate/core';
import { InternationalizationService } from '@/shared/_services';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatListModule,
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { metaReducers, ROOT_REDUCERS } from './reducers';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';

@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        HttpClientModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        TranslateModule.forRoot(),
        MatGridListModule,
        MatCardModule,
        MatTooltipModule,
        // NbChatModule,
        // NbDatepickerModule,
        // NbDialogModule,
        // NbMenuModule,
        // NbSidebarModule,
        // NbToastrModule,
        // NbWindowModule,
        // NbSelectModule,
        // NbCardModule,
        MatMenuModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatListModule,
        // LanguageSelectorComponent
    ],
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatSidenavModule,
        MatListModule,
        // Observable
    ],
    declarations: [
    ],
    entryComponents: [],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        InternationalizationService,
        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [LanguageSelectorComponent]
})
export class LanguageSelectorModule { };