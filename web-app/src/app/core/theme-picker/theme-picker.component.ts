import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StyleManagerService } from '@/shared/_services/_local-services/style-manager.service';
// import { indigo, deeppurple, pink, purple} from "@angular/material/prebuilt-themes";
// import "~@angular/material/prebuilt-themes/indigo-pink.css";
// import "~@angular/material/prebuilt-themes/deeppurple-amber.css";
// import "~@angular/material/prebuilt-themes/pink-bluegrey.css";
// import "~@angular/material/prebuilt-themes/purple-green.css";
@Component({
    selector: 'app-theme-picker',
    templateUrl: './theme-picker.component.html',
    styleUrls: ['./theme-picker.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ThemePickerComponent implements OnInit {

    themes: CustomTheme[] = [
        {
            primary: '#673AB7',
            accent: '#FFC107',
            name: 'deeppurple-amber',
            isDark: false,
        },
        {
            primary: '#3F51B5',
            accent: '#E91E63',
            name: 'indigo-pink',
            isDark: false,
            isDefault: false,
            
        },
        {
            primary: '#E91E63',
            accent: '#607D8B',
            name: 'pink-bluegrey',
            isDark: true,
            isDefault: false,
        },
        {
            primary: '#9C27B0',
            accent: '#4CAF50',
            name: 'purple-green',
            isDark: true,
        },
    ];

    constructor(
        public styleManager: StyleManagerService,
    ) { }

    ngOnInit() {
        this.installTheme('indigo-pink');
    }

    installTheme(themeName: string) {
        const theme = this.themes.find(currentTheme => currentTheme.name === themeName);
        if (!theme) {
            return;
        }
        if (theme.isDefault) {
            this.styleManager.removeStyle('theme');
        } else {
            this.styleManager.setStyle('theme', theme.name);
            // this.styleManager.setStyle('theme', `/assets/pink-bluegrey.css`);
        }

    }

}



export interface CustomTheme {
    name: string;
    accent: string;
    primary: string;
    isDark?: boolean;
    isDefault?: boolean;
}