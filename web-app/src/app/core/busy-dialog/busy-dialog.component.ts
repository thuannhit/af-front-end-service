import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';  
@Component({
    templateUrl: 'busy-dialog.component.html',
    styleUrls: ['busy-dialog.component.scss']
})
export class BusyDialogComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<BusyDialogComponent>
        , @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
    }

}