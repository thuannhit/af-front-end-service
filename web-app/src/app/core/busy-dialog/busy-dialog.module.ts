import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BusyDialogComponent } from '@/core/busy-dialog';
import {
    MatFormFieldModule,
    MatProgressSpinnerModule
} from '@angular/material';

@NgModule({

    imports: [
        CommonModule,
        MatFormFieldModule,
        MatProgressSpinnerModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    ],
    exports: [
        CommonModule,
        FormsModule,
        BusyDialogComponent
    ],
    declarations: [
        BusyDialogComponent
    ],
    entryComponents: [],
    providers: [
    ],
})
export class BusyDialogModule { };