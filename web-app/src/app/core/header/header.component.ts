import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { InternationalizationService } from '../../shared/_services';
// import { NbMediaBreakpointsService, NbThemeService } from '@nebular/theme';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from '@/shared/_services';
import { ICCreationComponent, ICCreationDialogComponent } from '@/modules/ic/components/ic-creation'
import { userInfo } from 'os';
@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {
    private destroy$: Subject<void> = new Subject<void>();
    userPictureOnly: boolean = false;
    translationsUrl = '/assets/i18n/header';
    language: any = {};
    isDisable: boolean;
    currentUser: {

    }
    themes = [
        {
            value: 'default',
            name: 'Light',
        },
        {
            value: 'dark',
            name: 'Dark',
        },
        {
            value: 'cosmic',
            name: 'Cosmic',
        },
        {
            value: 'corporate',
            name: 'Corporate',
        },
    ];
    currentTheme = 'default';
    creationICForm = {
        // icInterest:'',
    }
    @Output() public sidenavToggle = new EventEmitter();
    constructor(
        private inznservice: InternationalizationService,
        private http: HttpClient,
        // private themeService: NbThemeService,
        // private breakpointService: NbMediaBreakpointsService,
        private router: Router,
        public dialog: MatDialog,
        private userService: UserService,
    ) {
        this.userService.currentUser.subscribe(x => {
            this.currentUser = x;
            if (!!x && !!x.username) {
                this.isDisable = false;

            } else {
                this.isDisable = true;
            }
        });
    }
    ngOnInit() {

        this.inznservice.language.subscribe((val) => {
            if (val == "en") {
                this.inznservice.use(val, this.translationsUrl).then((data) => {
                    this.language = data;
                });
            }
            else if (val == "vn") {
                this.inznservice.use(val, this.translationsUrl).then((data) => {
                    this.language = data;
                });
            }
        });

        // this.currentTheme = this.themeService.currentTheme;

        // const { xl } = this.breakpointService.getBreakpointsMap();
        // this.themeService.onMediaQueryChange()
        //     .pipe(
        //         map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        //         takeUntil(this.destroy$),
        //     )
        //     .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

        // this.themeService.onThemeChange()
        //     .pipe(
        //         map(({ name }) => name),
        //         takeUntil(this.destroy$),
        //     )
        //     .subscribe(themeName => this.currentTheme = themeName);
    }

    public onToggleSidenav = () => {
        this.sidenavToggle.emit();
    }

    // changeTheme(themeName: string) {
    //     debugger;
    //     this.themeService.changeTheme(themeName);
    // }
    onOpenICManagementPage() {
        this.router.navigate(['/ic/admin/ic-management']);
        // debugger;
    }
    onOpenInterestManagementPage() {
        this.router.navigate(['/ic/admin/ic-interest-management']);
        // debugger;
    }
    onOpenICCreationPage() {
        if (this.router.url === "/ic/admin/ic-management") {
            this.openDialog();
        } else {
            this.router.navigate(['/ic/admin/ic-creation']);
        }
    }
    onOpenICTradingPage() {
        this.router.navigate(['/ic/admin/ic-trading-management']);
    }
    openDialog(): void {
        const dialogRef = this.dialog.open(ICCreationDialogComponent, {
            width: '70%',
            // data: { name: this.name, animal: this.animal }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            // this.animal = result;
        });
    }

    logout() {
        this.userService.logout();
        if (this.router.url.includes('admin')) {
            this.router.navigate(['/ic/admin/login']);
            return;
        };

        this.router.navigate(['/login']);
    }
}


