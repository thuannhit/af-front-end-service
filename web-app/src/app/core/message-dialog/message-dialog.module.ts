import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MessageDialogService } from '@/shared/_services/_local-services/';
import { MatDialogModule, MatButtonModule, MatIconModule } from '@angular/material';
import { MessageDialogComponent } from './message-dialog.component';
@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        MatButtonModule,
        MatIconModule
    ],
    declarations: [
        MessageDialogComponent
    ],
    exports: [MessageDialogComponent],
    entryComponents: [],
    providers: []
})
export class MessageDialogModule {
}