import { ChangeDetectionStrategy, Component, HostListener, Inject, Output } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { MessageType } from "@/shared/_common";
import { DialogOption } from "@/shared/_models";
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './message-dialog.component.html',
    styles: ['message-dialog.component.scss']
})
export class MessageDialogComponent {
    messageType = MessageType;
    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogOption, private mdDialogRef: MatDialogRef<MessageDialogComponent>) {
    }
    public cancel() {
        if (this.data.CANCELAction) {
            this.data.CANCELAction()
        }
        this.close(false);
    }
    private close(value) {
        this.mdDialogRef.close(value);
    }
    public confirm() {
        if (this.data.OKAction) {
            this.data.OKAction()
        }
        this.close(true);
    }
    // public doAction(i: number){
    //     if (this.data.Actions[i].actionFn){
    //         this.data.Actions[i].actionFn()
    //     }
    //     this.close(true);
    // }
    @HostListener("keydown.esc")
    public onEsc() {
        this.close(false);
    }
}