import { Router } from '@angular/router';

// import { UserService } from './shared/_services';
import { TradingUser, AdminUser } from './shared/_models';
import { Component, OnDestroy, OnInit, PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import './shared/_content/app.less';

import { TranslateService } from '@ngx-translate/core';
import * as fromI18n from './i18n/reducers';
import { Store } from '@ngrx/store'
import { I18nComponent } from './i18n/container/i18n.component';

@Component({ selector: 'app', templateUrl: 'app.component.html', styleUrls: ['./app.component.scss'] })
export class AppComponent extends I18nComponent implements OnDestroy {
    currentTradingUser: TradingUser;
    currentAdminUser: AdminUser;
    constructor(
        readonly store: Store<fromI18n.State>,
        readonly translate: TranslateService
    ) {
        super(store, translate);
    }

    ngOnDestroy(){}
}