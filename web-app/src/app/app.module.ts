import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import {
    HttpClient,
    HttpClientModule,
    HTTP_INTERCEPTORS
} from '@angular/common/http'
import { CurrencyPipe } from '@angular/common';
import { appRoutingModule } from './app.routing'
import { JwtInterceptor, ErrorInterceptor } from './shared/_helpers'
import { AppComponent } from './app.component'
import { AppHomeComponent } from './modules/home'
import { BusyDialogComponent } from '@/core/busy-dialog/'
import { MessageDialogComponent } from '@/core/message-dialog'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { I18nModule } from '@/i18n/i18n.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker'
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatSnackBarModule
} from '@angular/material'
import { FlexLayoutModule } from '@angular/flex-layout'
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import 'hammerjs'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap' // For tooltip
import { MatMenuModule } from '@angular/material/menu' // For Menu icon
import { RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { metaReducers, ROOT_REDUCERS } from './reducers';
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/app/', '.json');
}
@NgModule({
    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        I18nModule,
        appRoutingModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        HttpClientModule,
        StoreModule.forRoot(ROOT_REDUCERS, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictStateSerializability: true,
                strictActionSerializability: true
            }
        }),
        StoreRouterConnectingModule.forRoot({
            routerState: RouterState.Minimal
        }),
        StoreDevtoolsModule.instrument({
            name: 'NgRx Book Store App'
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            },
            isolate: true
        }),
        // TODO: what is forRoot in here without any input data
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatTabsModule,
        FlexLayoutModule,
        NgbModule,
        MatMenuModule,
        // LanguageSelectorModule
        NgxMaterialTimepickerModule,
        // BusyDialogModule,
        MatDialogModule, 
        MatProgressSpinnerModule,
        MatSnackBarModule
    ],
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatSelectModule,
        MatDatepickerModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatTabsModule,
        TranslateModule
    ],
    declarations: [AppComponent, AppHomeComponent, BusyDialogComponent, MessageDialogComponent],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        CurrencyPipe
    ],
    entryComponents: [BusyDialogComponent, MessageDialogComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
