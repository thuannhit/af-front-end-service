const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'source-map',
    entry: {
        main:path.resolve(__dirname, 'src/main.ts'),
        polyfills: path.resolve(__dirname, 'src/polyfills.ts'),
    },

    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            '@': path.resolve(__dirname, 'src/app/'),
            '$': path.resolve(__dirname, ''),
        }
    },
    output: {
        publicPath: '/',
        chunkFilename: '[name]-chunk.js',
        filename: '[name].[contenthash].bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },

    optimization: {
        minimize: true,
        minimizer: [new UglifyJSPlugin({
            // cache: true,
            parallel: true,
            sourceMap: true
        })],
        runtimeChunk: true,
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        // get the name. E.g. node_modules/packageName/not/this/part.js
                        // or node_modules/packageName
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        // npm package names are URL-safe, but some servers don't like @ symbols
                        return `npm.${packageName.replace('@', '')}`;
                    },
                },
            },
        },
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['ts-loader', 'angular2-template-loader', 'angular-router-loader',]
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader']
            },
            {
                test: /\.(css|scss)$/,
                include: [
                    path.join(__dirname, 'src/app/')
                ],
                loaders: ['to-string-loader', 'css-loader', 'sass-loader']
            },
            // workaround for warning: System.import() is deprecated and will be removed soon. Use import() instead.
            {
                test: /[\/\\]@angular[\/\\].+\.js$/,
                parser: { system: true }
            },
            {
                test: /\.(jpg|png)$/,
                use: {
                    loader: 'url-loader',
                },
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            }
        ]
    },
    plugins: [
        // new CopyWebpackPlugin([{ from: 'src/app/assets', to: 'assets' }]),
        new HtmlWebpackPlugin({ template: './src/index.html' }),
        new webpack.DefinePlugin({
            // global app config object
            config: JSON.stringify({
                apiUrl: 'http://localhost:5000'
            }),
            'process.env.NODE_ENV': JSON.stringify('production')
        }),

        // workaround for warning: Critical dependency: the request of a dependency is an expression
        new webpack.ContextReplacementPlugin(
            /\@angular(\\|\/)core(\\|\/)fesm5/,
            path.resolve(__dirname, 'src')
        ),
        // new UglifyJSPlugin({
        //     cache: true,
        //     parallel: true,
        //     sourceMap: true
        // })
        // new ExtractTextPlugin({ filename: '[name].scss', allChunks: true }),
    ],

    devServer: {
        historyApiFallback: true,
        inline: true,
        port: 4000,
        host: 'localhost',
    }
}